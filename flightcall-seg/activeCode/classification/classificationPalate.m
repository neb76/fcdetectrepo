 pnames={'MaxIntensity', 'MinIntensity', 'MedianIntensity', ...
       'MeanIntensity', 'Area','regVar', 'Perimeter', 'MajorAxis',...
       'MinorAxis', 'Orientation', 'CentroidPositionI' 'CentroidPositionJ' };
  
  
  
  [labelList, featureMat, pnames]=processData(cleanData, specMat);
  
  cTree=ClassificationTree.fit(featureMat,labelList, 'PredictorNames', pnames);
  
  
  cTree=ClassificationTree.fit(featureMat,labelList, 'crossval', 'on', 'kfold', 10);
  cTree.kfoldLoss  %.0898
  
  
  view(cTree,'mode','graph')
  
  
 B=TreeBagger(40,featureMat,labelList, 'OOBVarImp', 'on'); 
  
 err= error(B, featureMat,labelList);

 
nTrees=60; 
cvpart = cvpartition(size(labelList,1),'holdout', .3);
Xtrain = featureMat(training(cvpart),:);
Ytrain = labelList(training(cvpart),:);
Xtest = featureMat(test(cvpart),:);
Ytest = labelList(test(cvpart),:);

B=TreeBagger(nTrees,Xtrain,Ytrain, 'OOBVarImp', 'on'); 

figure;
plot(error(B,Xtest,Ytest,'mode','cumulative'));
xlabel('Number of trees');
ylabel('Test classification error');



error(B,Xtest,Ytest);