%###must change pFeatures value if you change the features value.  Also
%note that you must change both in this class, and the outer, calling class


%labels are binary lists, ones and twos
%invariant-- adjustments made inside
    %takes raw spec, and raw segmentation that has background as 0, signal
    %as 1
function[labelList, featureMat, pnames]= extractFeatures(seg, spec)
    pFeatures= 12; %####this is hard coded for preallocation issues
    
    %This trims high-pass filtered lower frequencies, the value .81 is from
    %eyeballing
    newH= size(spec, 1);
    newH= newH*.81; %from inspection
    trimmed_spec= spec(1:newH, :);
    %ensure all values are positive
    oldmin= min(min(trimmed_spec));%imshow(trimp, [], 'XData', [1 256], 'YData', [1 175])
    if oldmin<0
        trimmed_spec= trimmed_spec-oldmin;
    end
    
   
    seg= seg+1; %add one so background is classified. This is safer in case background is incorrectly segmented
    props= regionprops(seg,trimmed_spec, {'PixelValues', 'Area', 'Centroid',...
      'MaxIntensity', 'MinIntensity', 'MeanIntensity','Perimeter',...
      'MajorAxisLength', 'MinorAxisLength', 'Orientation' });
  
    cent= regionprops(seg, {'Centroid'});
    nobj= numel(props);
    
   labelList= zeros(nobj, 1);
   labelList(2,1)=1;
   featureMat= ones(nobj, pFeatures);
    
   for k= 1:nobj
       pix=props(k).PixelValues;
       
       featureMat(k,1)= props(k).MaxIntensity;
       featureMat(k,2)=props(k).MinIntensity;
       featureMat(k,3)=median(pix);
       featureMat(k,4)=props(k).MeanIntensity;
       featureMat(k,5)= props(k).Area;
       featureMat(k,6)= var(pix);
       featureMat(k,7)= props(k).Perimeter;  %this stat makes me nervous...    
       featureMat(k,8)=props(k).MajorAxisLength;
       featureMat(k,9)=props(k).MinorAxisLength;
       featureMat(k,10)=props(k).Orientation;
       featureMat(k,11:12)= cent(k).Centroid;    
   end        
   pnames={'MaxIntens', 'MinIntens', 'MedianIntens', ...
       'MeanIntens', 'Area','regVar', 'Perim', 'MjrAxis',...
       'MnAxis', 'Orientation', 'CentroidI' 'CentroidJ' };
end
    
    