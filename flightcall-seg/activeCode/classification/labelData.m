%label data

J192Out= job192.getAllOutputArguments;

j129_res= J192Out{27,2};


goods=find(j129_res> .7); %551


%sweep sigmas and K's
sched2=findResource('scheduler', 'configuration', 'atlasscheduler' );

labelAllData_newclustSize= createJob(sched2);
set(labelAllData_newclustSize,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})

for s= goods
    createTask(labelAllData_newclustSize, @computeDistrJob_labelData, 8, {'labeling alldata [8 2] dilation', .025, 6, specMat{s,1}, segMatRef(s,:)});
end

submit(labelAllData_newclustSize)
wait(labelAllData_newclustSize)

jobOut=labelAllData_newclustSize.getAllOutputArguments;

figure, imagesc(jobOut{1,1})



noCell=cleanData(~iscell(cleanData),:);
cleanData(~iscell(cleanData),:)=[];






cleanData= jobOut;
cleanData( all(cellfun(@isempty,cleanData),2), : ) = [];
for r= 1:size(cleanData,1)
    if cleanData{r,2}==.0130
        ind(end+1)= r;
    end
end


cleanData(ind,:)=[];

seeInd=randsample(size(cleanData,1), 10);

for i= 1:10
    ind= seeInd(i);
    figure
    subplot(1,2,1), imagesc(cleanData{ind, 1});
    specInd=cleanData{ind, 5};
    subplot(1,2,2), imagesc(processpic(specMat{specInd,1},1,0));
   
end




