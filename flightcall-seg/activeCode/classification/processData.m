
function[labelList,featureList]= processData(cleanData, specMat )
    labelList=[];
    featureList=[];
    
    for i= 1:size(cleanData)
        curSeg=cleanData{i,1};
        specInd=cleanData{i,5};
        curSpec=specMat{specInd, 1};
        [curLabL, curFeatMat]= extractFeatures(curSeg, curSpec);
        
        if isempty(labelList)
            labelList=curLabL;
            featureList=curFeatMat;            
        else
            labelList=vertcat(labelList,curLabL);
            featureList=vertcat(featureList,curFeatMat);
        end            
    end
end
  
  