function [segMat]=concat_chunks(raw_job_out)
raw_segMat={};
for c= 1:length(raw_job_out)
cur_segs=raw_job_out{c, 1};
cur_times=raw_job_out{c,2};
cur_tup=horzcat(cur_segs, cur_times);
raw_segMat=vertcat(raw_segMat, cur_tup);
end

inds=length(raw_segMat);
inds=num2cell(  1:inds)
inds=inds';
raw_segMat=horzcat(raw_segMat(:,1),inds, raw_segMat(:,2), raw_segMat(:,3), raw_segMat(:,4));

clean_rows=find(~cellfun(@isscalar,raw_segMat(:,1)));
segMat=raw_segMat(clean_rows,:);

end