function [spec_segMat, simple_segMat]=concat_chunks_two(raw_job_out)
spec_segMat={};
simple_segMat={};
for c= 1:length(raw_job_out)
    cur_spec=raw_job_out{c,1};
    cur_simp=raw_job_out{c,2};
    spec_segMat=vertcat(spec_segMat, cur_spec);
    simple_segMat=vertcat(simple_segMat, cur_simp);
end

% inds=length(raw_segMat);
% inds=num2cell(  1:inds)
% inds=inds';
% raw_segMat=horzcat(raw_segMat(:,1),inds, raw_segMat(:,2), raw_segMat(:,3), raw_segMat(:,4));
% 
% clean_rows=find(~cellfun(@isscalar,raw_segMat(:,1)));
% segMat=raw_segMat(clean_rows,:);

end