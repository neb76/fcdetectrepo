
close all;clc;
location= 'DANBY';
date='20090912';
time='194900';

%% Some variables
samRate=24000; % Sampling rate of recording
set(0,'DefaultFigureWindowStyle','docked')
txtpath= 'C:\Users\neb76\Desktop';
factor=10; %% This is to retain the analogy between number of windows, overlap based on length of audio segment
length=factor*3000; %30,000 samples is 1.248 secs

 %to start at dev spot, 1259.79, set I to number of windows
 i= ceil(1259.79/1.248);
        
%Create Indices
startWav=1+(i-1)*length;% - (i-1)*frameoverlap;%e.g 1-10001 10001-20001
endWav=startWav+length;
%A=wavread(strcat(txtpath,'/',location,'_',date,'_',time,'.wav'),[startWav endWav]);
full_stream=wavread(strcat(txtpath,'/',location,'_',date,'_',time,'.wav'), 'size');

A=highpassfilter(A,samRate,2500);        
[S,F,T,P]= spectrogram(A,factor*70,factor*10,512,samRate,'yaxis');

drawnow
pause 
close all





%% closer attempt to map from old, working example
startTime=1259.79;
endTime=1259.89;

startWav=floor(startTime/(1/samRate));
endWav=ceil(endTime/(1/samRate));
strcat(txtpath,'/',location,'_',date,'_',time,'.wav')
A=wavread(strcat(txtpath,'/',location,'_',date,'_',time,'.wav'),[startWav endWav]);
duration=endTime-startTime;
durSamples=duration/(1/samRate);
factor =durSamples/3000;
spectrogram(A,round(factor)*70,round(factor)*10,512,samRate,'yaxis')
