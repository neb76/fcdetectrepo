%num, 'r'
function []=find_thresh(csv_name, sound_file_name, varargin)
time_seg_length=2;
optarg=size(varargin,2);
[num,txt,raw]= xlsread(csv_name);
samrate=24000; %hardcoded sample rate
species=txt(2:end, 10);
time_freq=num(:,4:7);
data_frame=horzcat(species, num2cell(time_freq));   %#species 	#time(start then stop) 	#frequency(lwo then high)


clip_range=1:size(data_frame,1);

if optarg==2
		clip_range=randsample(size(data_frame,1),varargin{1});
		clip_range
end



for clip=clip_range' %'
	clip
	s=data_frame{clip,2};
	f= data_frame{clip,3};
	mid_time=(s + f)/2;
	start_i=round((mid_time- time_seg_length/2)*samrate);
	stop_i=round((mid_time+ time_seg_length/2)*samrate);
	cur_clip=wavread(sound_file_name, [start_i stop_i]);
	if strfind(data_frame{clip,1}, 'THSH') 
		strfind(data_frame{clip,1}, 'THSH')
 
	else
		low_freq=6000;
		high_freq=11000;
	end
	cur_clip_filtered=highpassfilter(cur_clip, samrate, low_freq);
	cur_clip_filtered=lowpassfilter(cur_clip_filtered, samrate, high_freq);	
	figure

	test_buf2(cur_clip_filtered, 1);
	title(sprintf('energy detection for a: %s  at time: %s',data_frame{clip,1}, data_frame{clip,2}) );
end

end
