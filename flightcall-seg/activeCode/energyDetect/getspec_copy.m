% e.g.   getspec('DANBY','20090912','194900',1259.79,1259.89)
%               change     spectrogram(A,round(factor)*70,round(factor)*10,512,samRate,'yaxis')
%                   to     spectrogram(A,round(factor)*70,round(factor)*10,1024,samRate,'yaxis')


function [p_new,f,t] = getspec_copy(fftSize, location,date,time,varargin)

% close all;clc;
%% Some variables
samRate=24000; % Sampling rate of recording
set(0,'DefaultFigureWindowStyle','docked')


%txtpath='/Users/damoulas/Dropbox/FOLDER FOR THEO TO USE';
txtpath = 'C:\Users\neb76\Desktop';

%% Read the 10 hour clip....step by step
optarg=size(varargin,2);


if optarg == 0 %% If you wanna cycle through the data
    factor=10; %% This is to retain the analogy between number of windows, overlap based on length of audio segment
    
    length=factor*3000; %30,000 samples is 1.248 secs
    %frameoverlap=2000;
    
    %% It seems that for 3K samples around 70-length window and 10 samples overlap is looking ok - need to tune.
    
    for i=1:10000 %Total samples is 10.5hrs*3600*samRate
        
        
        %Create Indices
        startWav=1+(i-1)*length;% - (i-1)*frameoverlap;%e.g 1-10001 10001-20001
        endWav=startWav+length;
        
        A=wavread(strcat(txtpath,'/',location,'_',date,'_',time,'.wav'),[startWav endWav]);
        
        A=highpassfilter(A,samRate,2500);
        
        spectrogram(A,factor*70,factor*10,512,samRate,'yaxis');
        drawnow
        
        
        pause
        
        
        close all
        
    end
    
elseif optarg == 2 %If you wanna pick a specific time interval to use
    
    startTime=varargin{1};
    endTime=varargin{2};
    
    startWav=floor(startTime/(1/samRate));
    endWav=ceil(endTime/(1/samRate));
    strcat(txtpath,'/',location,'_',date,'_',time,'.wav')
    A=wavread(strcat(txtpath,'/',location,'_',date,'_',time,'.wav'),[startWav endWav]);
    display(sprintf('total samples in our orignal calc: %d', endWav-startWav) );
    % A Butterworth filter
    A=highpassfilter(A,samRate,2500);
    
    % Calculating duration and factor levels
    duration=endTime-startTime;
    durSamples=duration/(1/samRate);
    factor =durSamples/3000;
    figure
%     spectrogram(A,round(factor)*70,round(factor)*10,512,samRate,'yaxis') % original
    spectrogram(A,round(factor)*70,round(factor)*10,fftSize,samRate,'yaxis'); % modded for better frequency resolution

    
    %% Normalizing the spectrogram
 
    
    [~,f,t,p]=spectrogram(A,round(factor)*70,round(factor)*10,fftSize,samRate);  %original
    p_new=10*log10(abs(p)); %e.g. 257x37
    
    %Normalizing
    %p_new=p_new./repmat(max(p_new,[],1),size(p_new,1),1);
    
    %%Median Filter
    %p_new = medfilt2(p_new, [3 3]);
    
    %% Gaussian Filter
    H=fspecial('gaussian',[3 3],0.2);
    p_new=imfilter(p_new,H,'replicate');
    
    
    %% Re-plotting
    % NOTE: This is the same as calling spectrogram with no outputs.
    
    surf(t,f,p_new,'EdgeColor','none');
    axis xy; axis tight; colormap(jet); view(0,90);
    xlabel('Time');
    ylabel('Frequency (Hz)');
    tit= strcat('time Interval,  start: ', num2str(startTime), '    finish: ', num2str(endTime) );
    tit_h= title(tit);
    set(tit_h, 'String', tit)
    
elseif optarg == 1 %If you only give a starting point 
    
    startTime=varargin{1};
    startWav=floor(startTime/(1/samRate));
    %endWav=startWav+511; %Total 512 frame size
    endTime=startTime+0.1;
    endWav=ceil(endTime/(1/samRate));
    
    A=wavread(strcat(txtpath,'/',location,'_',date,'_',time,'.wav'),[startWav endWav]);
    
    %% A Butterworth filter
    A=highpassfilter(A,samRate,2500);
    
    %% A=whiten(A,0.1);
    
    spectrogram(A,512,256,512,samRate,'yaxis')
    

    %% Play around with smooth filters
    [~,f,t,p]=spectrogram(A,512,256,512,samRate,'yaxis');
    
    p_new=10*log10(abs(p));
    
    %%Median Filter
    %p_new = medfilt2(p_new, [2 2]);
    
    %% Gaussian Filter
    H=fspecial('gaussian',[3 3],0.2);
    p_new=imfilter(p_new,H,'replicate');
    
    %%Mean filter
    %h = fspecial('average', [2 2]);
    %p_new=filter2(h, p_new);
    
    %%Convolution
    %[x,y]=size(p_new);
    %p_new = conv2(p_new, ones(2,2));
    %p_new=p_new(1:x,1:y);
    
    
    % Re-plotting
    % NOTE: This is the same as calling spectrogram with no outputs.
    figure
    surf(t,f,p_new,'EdgeColor','none');
    axis xy; axis tight; colormap(jet); view(0,90);
    xlabel('Time');
    ylabel('Frequency (Hz)');

end
p_new= flipud(p_new);
end