%(csv, sound)- listen to all
%(csv,sound, N)  -listen to the nth sample
%(csv,sound, type) -listen to all of certain type
%(csv,sound, start,stop) -listen to a specified range
%(csv,sound,N,'r')- listen to a random N sample
function []=listen_to_filters(csv_name, sound_file_name, varargin)

[num,txt,raw]= xlsread(csv_name);
optarg=size(varargin,2);
one_type=false
samrate=24000; %hardcoded sample rate
species=txt(2:end, 10);
time_freq=num(:,4:7);
data_frame=horzcat(species, num2cell(time_freq));   %#species 	#time(start then stop) 	#frequency(lwo then high)

%clip_start=1;
%clip_end=size(data_frame,1);
clip_range=1:size(data_frame,1);
%handle options
if optarg==1
	if ischar(varargin)
		one_type=true;
	else
		clip_range=varargin{1};
	end

end
if optarg==2
	if isnum(varargin{2})
		clip_range=varargin{1}:varargin{2};
	else
		clip_range=randsample(size(data_frame,1),varargin{2});
	end
end


for clip=clip_range
	if one_type
		if ~strcmp(data_frame{clip,1}, varargin{1})
			continue
		end
	end

	display(sprintf('processing a %s, at time %s, frequency of interest: %s to %s...', data_frame{clip,1}, num2str(data_frame{clip,2}), num2str(data_frame{clip,2}), num2str(data_frame{clip,2}) ))%add frequency range of interest
	start_i=round(data_frame{clip,2}*samrate);
	stop_i=round(data_frame{clip,3}*samrate);
	cur_sound=wavread(sound_file_name,[start_i stop_i]);
	low_freq_thresh=6000;
	high_freq_thresh=11000;
	if strcmp(data_frame{clip,1},'THSH')
		low_freq_thresh=2750;
		high_freq_thresh=3750;
	end
	cur_sound_filtered=highpassfilter(cur_sound, samrate, low_freq_thresh);
	cur_sound_filtered=lowpassfilter(cur_sound_filtered, samrate, high_freq_thresh);
	sound(cur_sound)
	sound(cur_sound_filtered)
end

end
