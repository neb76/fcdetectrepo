function [newsignal] = lowpassfilter(signal,samRate, freq)
%signal the data, samRate the usual sampling rate in Hz, freq the threshold freq in Hz


fNorm = freq / (samRate/2); 
[b, a] = butter(10, fNorm, 'low'); 
newsignal = filtfilt(b, a, signal);
