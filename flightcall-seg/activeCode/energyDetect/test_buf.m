%len will be time, size will be that len mapped to bytes by sample rate
%step referes to new data red per each next step in iteration, depends on
%overlap percentage

%set to 0 for whole file
function [run_time, sums_thrush, sums_sparrow]=test_buf(file_name,buf_len)

%%params
%these below give ideal resolution
fftnum=256;
window=128;
overlap_frac=.875;
overlap_win=round(overlap_frac*window);
samrate=24000; %##our data is all digitized at this rate

%% some mappings into file according to its size
full_size=wavread(file_name, 'size');
buf_size=buf_len*samrate;
step_size=buf_size*(1-overlap_frac);
step_len=buf_len*(1-overlap_frac);
total_steps=full_size/step_size
displaystats=sprintf('stats- buffer size: %d so # steps is: %d, size:  %d', buf_size, total_steps, step_size);


%%do first step to pre-set data structures 
cur_sound_clip=wavread(file_name, [1 (1+buf_size)]);    
[s,f,t,p]=spectrogram(cur_sound_clip,window,overlap_frac*window,fftnum,samrate, 'yaxis');

%use spec output f, listing of frequencty, to find range
thrush_cols=find(and(274<f,f<400));
sparrow_cols=find(and(600<f, f<1100));
col_N=size(p,2)
sums_thrush=zeros(1,col_N*total_steps);
sums_sparrow=zeros(1,col_N*total_steps);


sum_by_bird=@(spec,cols) sum(spec(cols,:),1); %simple anonmyous function for appending summations to our sum accumulation

p_log=10*log10(abs(p));
for bird= {'thrush' 'sparrow'}
    eval(sprintf('sum_%s(1,1:col_N)=sum_by_bird(p_log, %s_cols);', bird{1}, bird{1}));
end

tic
for step=[2:total_steps]
    start_i=step_size*(step-1)+1;    
    stop_i=start_i + buf_size;
    last=false;
    if stop_i>full_size
        stop_i=full_size(1)-1; 
        last=true;
    end
    cur_sound_clip=wavread(file_name, [start_i stop_i]);
    [s,f,t,p]=spectrogram(cur_sound_clip,window,overlap_win,fftnum,samrate, 'yaxis');
    p_log=10*log10(abs(p));
    col_ind_start=(step-1)*col_N +1;
    col_ind_end=step*col_N;
    if last
        col_ind_end=col_ind_start + size(p_log, 2)-1;
    end
    s1=sum(p_log,1)
    s2=sum_by_bird(p_log,sparrow_cols)
    for bird= {'thrush' 'sparrow'}
        eval(sprintf('sums_%s(1,col_ind_start:col_ind_end)=sum_by_bird(p_log, %s_cols);', bird{1}, bird{1}))
    end
    
end
 run_time=toc;


end