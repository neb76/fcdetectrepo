

%summary of params for function:
%    - winLen
%    - window function
    
%   **bufferlength, but more for performance, not for actual accuracy


%function itsel;f has two optional arguements,
%    -'is_file',  a string, if you are passing in a pre-read wav file instead of reading the file within the function instead of reading a file from a suplied filename
%   - winLen- defualt is 301, which seems to look good, but perhaps could be tuned

%recquires creating matrix size winLen*~buffer_len where buffer length will be time we chunk this in
%test_buf2(file_name)   use default window, 301, and read from file
%test_buf2(file_name, 'is_file')
%test_buf2(file_name, 'is_file', '401')
%test_buf2(file_name, '401')


%***Notes:
 %   we use only one filter, the high pass filter, cutting off everything below 2.5 kHz
 %   we apply a Hamming window, only because found online and looks good.
 %   Other window types, like flat top, could be useful?

 %ways to tune:-- try other windows lengths or both window types&lengths

function [energyST]=test_buf2(file_name, varargin)
optarg=size(varargin,2)
file_name_is_clip=false;
winLen=301;% default
for i=1:optarg
    if ischar(varargin{i})
        if strcmp(varargin{i}, 'is_file')
            file_name_is_clip=true;
        end
    elseif isnumeric(varargin{i})
        winLen=varargin{i};
    end

    winOverlap=winLen-1;
    wHamm=hamming(winLen);
    Fs=24000;
    
    if file_name_is_clip
        full_sound_clip=file_name;
    else
        full_sound_clip=wavread(file_name);
        full_sound_clip=highpassfilter(full_sound_clip,Fs,2500);
    end
    
    sigFramed = buffer(full_sound_clip, winLen, winOverlap, 'nodelay');  %creates matrix size: winLen x (signal_len -overlap mat)
    %first column, sample 1 to 301, second column,
    
    sigWindowed = diag(sparse(wHamm)) * sigFramed;
    
    % Short-Time Energy calculation
    energyST = sum(sigWindowed.^2,1);
    
    % Time in seconds, for the graphs
    t = [0:length(full_sound_clip)-1]/Fs;
    
    subplot(1,1,1);
    plot(t, full_sound_clip);
    title(sprintf('short time energy detection, with window len: %d   and overlap %d',  winLen, winOverlap));
    xlims = get(gca,'Xlim');
    
    hold on;
    
    % Short-Time energy is delayed due to lowpass filtering. This delay is
    % compensated for the graph.
    delay = (winLen - 1)/2;
    plot(t(delay+1:end - delay), energyST, 'r');
    xlim(xlims);
    xlabel('Time (sec)');
    legend({'signal','Short-Time Energy'});
    hold off;
    
    
    
end