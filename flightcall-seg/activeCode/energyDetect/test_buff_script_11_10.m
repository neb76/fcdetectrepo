%11/10, saturday testing
file_name='Z:\gitRepos\fcdetectrepo\flightcall-seg\dataFiles\shortened_recording.wav';
start_time=1259.79;
orig_clip='C:\Users\neb76\Desktop/DANBY_20090912_194900.wav';
file_name='Z:\gitRepos\fcdetectrepo\flightcall-seg\dataFiles\one_hit_clip_1_sec.wav';
two_sec_clip='Z:\gitRepos\fcdetectrepo\flightcall-seg\dataFiles\one_hit_clip_2_sec2.wav';
samrate=24000



energy=test_buf2(file_name);
[m d]=wavfinfo(orig_clip)

seconds=(10*60+27)*60
samplerate=902880234/seconds;


gap_seconds=2
start_i=1259*samrate
stop=start_i+(gap_seconds*samrate)
one_hit_clip_1_sec=wavread(orig_clip, [start_i stop]);

wavwrite(one_hit_clip_1_sec, samrate, 'Z:\gitRepos\fcdetectrepo\flightcall-seg\dataFiles\one_hit_clip_2_sec2.wav');

energy=test_buf2(two_sec_clip);
close all


filtered_one_hit_clip=highpassfilter(one_hit_clip_1_sec, samrate, 2500);
wavwrite(filtered_one_hit_clip, samrate, 'Z:\gitRepos\fcdetectrepo\flightcall-seg\dataFiles\filtered_2_sec.wav');



fvtool(D)
filter_again=filter(bandpass_test, one_hit_clip_1_sec)


double_filt=lowpassfilter(one_hit_clip_1_sec, samrate, 11000)
sound(double_filt)
sound(one_hit_clip_1_sec)

gap_seconds=1
start_i=2452*samrate
stop=start_i+(gap_seconds*samrate)
[thrush_hit, Fs]=wavread(orig_clip, [start_i stop]);
sound(thrush_hit)

low_pass_thrush=lowpassfilter(thrush_hit, samrate, 3750)
sound(low_pass_thrush)

low_then_high_thrush=highpassfilter(low_pass_thrush, samrate, 2750)
sound(low_then_high_thrush)

energy=test_buf2(low_then_high_thrush, 1);
energy=test_buf2(thrush_hit, 1);
figure
energy=test_buf2(low_pass_thrush, 1);
figure
energy=test_buf2(highpassfilter(thrush_hit, samrate,2750) , 1);


%build listen to all
csv_name= 'C:\Users\neb76\Desktop\nick_FCDetect_beforGit\DANBY_20090912_194900_NFC_637.csv'; 
orig_clip='C:\Users\neb76\Desktop/DANBY_20090912_194900.wav';

listen_to_filters(csv_name, orig_clip)


find_thresh(csv_name, orig_clip,4,'r')


high_thrush=highpassfilter(thrush_hit, samrate, 2500)
sound(high_thrush)

