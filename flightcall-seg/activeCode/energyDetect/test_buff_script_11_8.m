%testing buff script
%11/8, a thursday
%params??
% appear to be--
        %  overlap
        %fft size
        %win size
        %overlap


dev_clip='Z:\gitRepos\fcdetectrepo\flightcall-seg\dataFiles\med_10_minut_1250_start.wav';
orig_clip='C:\Users\neb76\Desktop/DANBY_20090912_194900.wav'

test_buf(dev_clip, 2, .5,0);

old_start=1259.79;
old_stop=old_start + 1;
[p,f,t]=getspec_copy(1024,'DANBY','20090912','194900', old_start, old_stop);



test_buf(orig_clip, .1, .5, old_start);

%first profiling loop!
[rt, sums]= test_buf(dev_clip, 60*10, .5, 0);
%46.3 secs for 20 mins



%named params for manual exexution of script
file_name='Z:\gitRepos\fcdetectrepo\flightcall-seg\dataFiles\med_10_minut_1250_start.wav';
buf_len=2
overlap_frac=.5;
special_start=0;

s1=sum(p,1);
s1_log=sum(10*log10(abs(p)),1);
%plot the two lines
X=1:size(s1,2)
figure
plot(X,s1,X,s1_log)




%try same with a detection
file_name='C:\Users\neb76\Desktop/DANBY_20090912_194900.wav'
buf_len=1
special_start=1259.79;
%do functions in adjecest script
figure,imagesc(p)
figure,imagesc(10*log(abs(p)))

s1=sum(p,1);
s1_log=sum(10*log10(abs(p)),1);
%plot the two lines
X=1:size(s1,2)
figure
plot(X,s1,X,s1_log)

for bl=[1 2 10]
for i=0:1
understand_spec_params(bl,i)
end
end

detection_start=1259.79;
[p,f,t]=getspec_copy(1024,'DANBY','20090912','194900', detection_start, samrate+detection_start);

old_stop=1259.89
[p,f,t]=getspec_copy(1024,'DANBY','20090912','194900', detection_start, old_stop);



for f =[1:3]
    for len=[1 2 5 10]
    understand_spec_params(f,len,0)
    end
end


%testing lab of o stats

    for len=[1 2 5 10]
    understand_spec_params(1,len,0)
    end
    
for bird= {'thrush' 'sparrow'}
s=bird{1}
end
    
% testing the function now, with summing
file_name='Z:\gitRepos\fcdetectrepo\flightcall-seg\dataFiles\shortened_recording.wav';
buf_len=1;

[run_time, sums_thrush, sums_sparrow]=test_buf(file_name,buf_len);

X=1:size(sums_thrush,2);
plot(X,sums_thrush,X, sums_sparrow);

figure
plot(sums_thrush(1:find(sums_thrush,1,'last')));

samrate=2400;
start_time=1259.79;
orig_clip='C:\Users\neb76\Desktop/DANBY_20090912_194900.wav';
start_i=start_time*samrate;
end_i=(start_time+1)*samrate;
one_hit_clip=wavread(orig_clip, [start_i end_i]);
wavwrite(one_hit_clip, 'one_hit_clip_1_sec.wav')

%now look at activations for one-hit-clip
file_name='Z:\gitRepos\fcdetectrepo\flightcall-seg\dataFiles\one_hit_clip_1_sec.wav';
buf_len=1;

[run_time, sums_thrush, sums_sparrow]=test_buf(file_name,buf_len);
trimed_thrush=sums_thrush(find(sums_thrush,1,'first'):find(sums_thrush,1,'last'));
trimed_sparrow=sums_sparrow(find(sums_sparrow,1,'first'):find(sums_sparrow,1,'last'));


X=1:size(trimed_thrush,2);
plot(X,trimed_thrush,X, trimed_sparrow);

figure
plot(sums_thrush(1:find(sums_thrush,1,'last')));



