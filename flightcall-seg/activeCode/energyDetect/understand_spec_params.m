function [] =understand_spec_params(fft_num,buf_len,use_theo_factor)
file_name='C:\Users\neb76\Desktop/DANBY_20090912_194900.wav';
start_time=1259.79;
if fft_num==1
    fftnum=256;
elseif fft_num==2
    fftnum=512
else 
    fftnum=1024;
end

if use_theo_factor
window=70;
overlap_win=10;
overlap_frac=.5
else
window= fftnum/2;
overlap_frac=.875;
end


samrate=2400; %##HARDCODE ASSUMPTION THAT ALL FILES DIGITIZED WITH THIS SAMPLE RATE


full_size=wavread(file_name, 'size');
buf_size=buf_len*samrate;
step_size=buf_size*(1-overlap_frac);
step_len=buf_len*(1-overlap_frac);
total_steps=full_size/step_size;


start_i=samrate*start_time;
stop_i=start_i + buf_size;

cur_sound_clip=wavread(file_name, [start_i stop_i]);
tit_string=sprintf('%d seconds      using fftnum of: %d',buf_len, fftnum);
if use_theo_factor
theo_factor=(stop_i-start_i)/3000;
[s,f,t,p]=spectrogram(cur_sound_clip,round(theo_factor)*window,round(theo_factor)*overlap_win,fftnum,samrate, 'yaxis');
tit_string=strcat(tit_string, 'using theo factor');
else
  [s,f,t,p]=spectrogram(cur_sound_clip,window,overlap_frac*window,fftnum,samrate, 'yaxis');
    tit_string=strcat(tit_string, 'no theo factor');
end
figure
imagesc(10*log10(abs(p)))
title(tit_string)
end