function [ tString ] = getTime( secs )
time= secs;
hrs= floor( time/(3600));
mins= floor((time-hrs*3600)/60);
secs= time-(hrs*3600+mins*60);
tString= strcat(num2str(   hrs), ':',num2str(  mins), ':', num2str( secs));
tString
end

