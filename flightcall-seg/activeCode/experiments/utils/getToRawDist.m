function [aff, dimF, dimT] = getToRawDist()
%returnsx raw distance affinity matrix
start= 4664.826;
stop=  4664.941;
filter= 3;
ampPower=2;


%get spec, then filter and trim
[p_new,f,t]= getspec('DANBY','20090912','194900',start,stop);
data= processpic( p_new, filter, 1 );

dimF= size(data, 1);
dimT= size(data, 2);
%rescale and specossibly transform
oldmin= min(min(data));
oldmax= max(max(data));
oldrange= oldmax-oldmin;
data= (data-oldmin)/oldrange;
if ampPower ~= 1
    data= data.^ampPower;
    %re-scale to 0-1
    oldmin= min(min(data));
    oldmax= max(max(data));
    oldrange= oldmax-oldmin;
    data= (data-oldmin)/oldrange;
end

dbvec= reshape(data, dimT*dimF, 1); %vec of all specoints
%pixel by pixel affinity calculation
distvec= pdist(dbvec, 'euclidean');
A= squareform(distvec); 




%make sparse by 8 neighbor connection
dimAff= size(A,1) 

edg_aDiagS=spdiags(A, -(dimF-1));
edg_aDiagM= spdiags(A, -dimF);
edg_aDiagL= spdiags(A, -(dimF+1));

edg_down=spdiags(A, -1);
edg_up= spdiags(A, 1);

edg_diagS= spdiags(A, (dimF-1));
edg_diagM= spdiags(A, dimF);
edg_diagL= spdiags(A, (dimF+1));

edg_adiagS(2)=0;
edg_adiagS(end)= 0;

edg_diagS(2)=0;
edg_diagS(end)= 0;


B= horzcat(edg_aDiagL, edg_aDiagM, edg_adiagS, edg_down, edg_up, edg_diagS, edg_diagM, edg_diagL);
aff= spdiags(B, [-(dimF+1), -dimF, -(dimF-1), -1, 1, (dimF-1), dimF, (dimF+1)], dimAff, dimAff);


end