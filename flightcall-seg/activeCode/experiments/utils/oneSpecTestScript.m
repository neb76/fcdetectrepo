type onespec_script

%one spec test script
clear all
close all
len= 1;  %if 0, use all data from csv
csv_name='C:\Users\neb76\Desktop\nick_FCDetect_beforGit\DANBY_20090912_194900_NFC_637.csv'
highSig= .1;
lowSig= .08;
step= .01;
dirName= datestr(clock, 'HH_MM_SS');
dirName= strcat(dirName, '_run');
dirName= regexprep(dirName,'\.','pnt');
mkdir(dirName);

segMat= parseCSV(csv_name, len);

jobN= 6* floor((highSig-lowSig)/step);
jobVec= repmat(segJob('template', .05, 0), 1, 1);
jobI= 1;
s= highSig;
while s>lowSig
    sigSt= strcat('sigma_ ', num2str(s))
    
    id= strcat('raw   ', sigSt);
    jobVec(jobI)= segJob(id, s, 0);
    jobI= jobI+1;
    
    id= strcat('R   ', sigSt);
    jobVec(jobI)= segJob(id, s, 1);
    jobI= jobI+1;
    
    id= strcat('S   ', sigSt);
    jobVec(jobI)= segJob(id, s, 2);
    jobI= jobI+1;
    
    id= strcat('SR   ', sigSt);
    jobVec(jobI)= segJob(id, s, 3);
    jobI= jobI+1;
    
    id= strcat('RS   ', sigSt);
    jobVec(jobI)= segJob(id, s, 4);
    jobI= jobI+1;
    
    id= strcat('RSR   ', sigSt);
    jobVec(jobI)= segJob(id, s, 5);    
    jobI= jobI+1;
    
    s= s-step;
end
    
    
specJob= jobVec(1);   
[spec, ~]= specJob.getJobSpecG(segMat{1, 2:3});




parfor j= 1:size(jobVec,2)  %make sure this is a row vector and not column vector
   curJob= jobVec(j);
   saveStr= regexprep(curJob.ID,' ','_');
   saveStr= regexprep(saveStr,':','_');
   saveStr= regexprep(saveStr,'\.','pnt');

   try
    
    segged= curJob.segmentThis(spec);   
    sf= figure;
    imagesc(segged)
    title(curJob.ID)
    
    saveStr= regexprep(curJob.ID,' ','_');
    saveStr= strcat(saveStr, 'Y');
    saveas(sf, [dirName saveStr '.jpg']); 
    
   catch exception
       ef= figure;
       errMssg= strcat(curJob.ID, ' does not converge at eigenvalue');
       title(errMssg)
      
       saveStr= strcat(saveStr, 'N');
       saveas(ef, [dirName saveStr '.jpg']);
       
   end       
       

   
end