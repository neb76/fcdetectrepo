function [ pixel_labels ] = kmeansseg( p, clustsnum, reps, casen)
%KMEANS Summary of this function goes here
%   Detailed explanation goes here
%   case1= non weighted kmeans-- 
        %which just divides into three even bars
%   case2= just amplitude, 
        % gives some interesting results actually, but not useful
        
%   case3= sphere the spaces, all weights the same 
%   case4= same as 3, only attempt to give specific weights
 

dimT= size(p, 2); %maps to 'ncols'
dimF= size(p, 1); %maps to 'nrows'
pr= 'putting into 3 columns form...'
dbvec= reshape(p, dimT*dimF, 1); %vec of all points
if (casen== 1) || (casen== 3) || (casen== 4)
    fvectemp=1:dimF;
    fvec= repmat(fvectemp',dimT,1);
    tvecttemp= 1:dimT;
    tmat= repmat(tvecttemp, dimF, 1);
    tvec= reshape(tmat, dimT*dimF, 1);
    if casen== 3||casen== 4
        fvmean= mean(fvec);
        tvmean= mean(tvec);
        dbmean= mean(dbvec);
        
        fsd= std(fvec);
        tsd= std(tvec);
        dbsd= std(dbvec);
        
        if casen== 4
            fvec= 1.1*(fvec-fvmean)/(fsd);
            tvec= (tvec-tvmean)/(tsd);
            dbvec= ((dbvec-dbmean)/dbsd).^3;
        else
        fvec=(fvec-fvmean)/(fsd);
        tvec= (tvec-tvmean)/(tsd);
        dbvec= (dbvec-dbmean)/dbsd;
        end
    end
    points= horzcat(dbvec, fvec, tvec);
    pr = 'in 3 column form'
    pr = 'k means now operating...'
    
    [cluster_idx, C] = kmeans(points, clustsnum, 'distance', 'sqEuclidean', 'Replicates',reps, 'start', 'uniform');
    
    pr = 'kmeans with unweighted spatial information completes'
    pr = 'showing segmented spectrogram'
end

if casen==2
    dbmean= mean(dbvec);
    dbsd= std(dbvec);
    dbvec= (dbvec-dbmean)/dbsd;
    [cluster_idx, C] = kmeans(dbvec, clustsnum, 'distance', 'sqEuclidean', 'Replicates',reps);
end




pixel_labels = reshape(cluster_idx,dimF,dimT);
imshow(pixel_labels,[], 'XData', [1 256], 'YData', [1 175]), title('image labeled by cluster index');
end

