%1/10  generating noise matrices
file_no=5; %#############this should be sufficient for all!

sound_root_address='/Users/nicholasbruns/Desktop/all_acoustic_data/';
segMatRec_root_address='/Users/nicholasbruns/Desktop/fd_full_segMats/';

names={'DANBY_20090912_194900',
'DANBY_20090913_194700',
'DANBY_20090914_194500',
'DANBY_20090915_194400',
'DANBY_20090916_194200'}

%
%nick_local_path='/Volumes/NICHOLAS/full_data_5_night/false_detection_lists/DANBY_20090912_194900_FP.csv';
%

sched=findResource('scheduler', 'configuration', 'atlasscheduler' );
test_big_seg_3=createJob(sched);
set(test_big_seg_3,'FileDependencies', {'/Users/nicholasbruns/repos/bioacoustics/fcdetectrepo/flightcall-seg/activeCode/runCode/notScript/'})


nodes=32;
% data_root_address='Z:\gitRepos\fc_data\full_data_5_night'
% label_file_location=strcat(data_root_address, '\false_detection_lists');


sound_file_name=[sound_root_address names{file_no} '.wav'];
samrate=24000;

fd_csv_name= [segMatRec_root_address 'segMatRef_night_' num2str(file_no) '.mat'];
load(fd_csv_name);
tot=length(segMatRef);
step=ceil(tot/nodes);
sound_size=wavread(sound_file_name,'size');
for i=1:nodes
 i
    start_chunk= step*(i-1)+1;
    if i==nodes
        end_chunk=size(segMatRef,1);
    else
        end_chunk=step*i;
    end
    cur_ref_chunk=segMatRef(start_chunk:end_chunk,:);
    cur_clip_chunk=cell(size(cur_ref_chunk,1), 1);    
    
    chunk_ind=1;
    for c= 1:size(cur_ref_chunk,1)     
        clip_center=mean([cur_ref_chunk{c,2} cur_ref_chunk{c,3}]);
        if clip_center<.3
            clip_center=.3;
        end
        
        interval=round([clip_center-.2 clip_center+.2]*samrate);
        if interval(2)>sound_size
            clip_end=sound_size-1;
            interval=[(clip_end-.4)*samrate clip_end]
        end
        interval=round(interval);
        
        try
        cur_clip=wavread(sound_file_name,interval);
        
        
   
        cur_clip_chunk{chunk_ind,1}=cur_clip;
        catch
        end
        
        chunk_ind=chunk_ind+1;
    
        end
        
    args={[file_no i], cur_clip_chunk, cur_ref_chunk};
    createTask(test_big_seg_3, @computeDistrJob_segment_false_detections_FULL, 0, args);

end
display('!!!!!!!!!!   if an appropriate /tmp directory does not yet exist, this is wrong')
submit(test_big_seg_3)

