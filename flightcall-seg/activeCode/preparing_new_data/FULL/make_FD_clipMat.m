function [segMatRef]=make_FD_clipMat(file_base, label_file_name)
label_location= 'Z:\gitRepos\fc_data\full_data_5_night\false_detection_lists';
file_cell=(regexp (file_base, '_', 'split'));

cur_seg_mat=parseCSV(sprintf('%s%s%s',label_location, filesep, label_file_name), total_specs);
segMatRef=cur_seg_mat;

specMat=cell(size(segMatRef, 1), 1);
for s= 1:size(segMatRef, 1)
   display(sprintf('making spec #%d...',s));
   start=segMatRef{s,2};
    stop= segMatRef{s,3};
    mid= mean([start stop]);
    
    start=mid-.2;
    stop= mid+.2;
    
    spec= getspecBatch(1024,file_cell{:},  start, stop);
    specMat{s,1}= spec;    
    close all;
end

end