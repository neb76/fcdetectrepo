%12/9  generating noise matrices
file_no=2 %#############this should be sufficient for all!

names={'DANBY_20090912_194900',
'DANBY_20090913_194700',
'DANBY_20090914_194500',
'DANBY_20090915_194400',
'DANBY_20090916_194200'}


sched=findResource('scheduler', 'configuration', 'atlasscheduler' );
segment_noise_file1=createJob(sched);
set(segment_noise_file1,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})


size_factor=4;
nodes=32;
data_root_address='Z:\gitRepos\fc_data\full_data_5_night'
label_file_location=strcat(data_root_address, '\false_detection_lists');
label_post_fix='_FP.csv';
spec_location=strcat(data_root_address, '\false_detection_spec_mats');
segment_location=strcat(data_root_address, '\false_detection_segment_mats');

label_file_name=strcat(names{file_no}, label_post_fix);
tot=633*size_factor;
[specMat, segMatRef]=make_FD_specmat(names{file_no}, label_file_name, tot); 
step=ceil(tot/nodes);

for i=1:nodes
start_chunk= step*(i-1)+1
if i==nodes
    end_chunk=length(specMat);
else
    end_chunk=step*i;
end
    cur_spec_chunk=specMat(start_chunk:end_chunk, :);
    cur_ref_chunk=segMatRef(start_chunk:end_chunk, :);
    
    args={sprintf('chunk %d of labeling file: %s ', i, names{file_no}),.025, 6, cur_spec_chunk, cur_ref_chunk}
    createTask(segment_noise_file1, @computeDistrJob_segment_false_detections, 4, args)

end

submit(segment_noise_file1)

raw_job_out=segment_noise_file1.getAllOutputArguments
segMat=concat_chunks(raw_job_out);

%####################################be very careful here, when you go into
%doing this in parralell
file_no=2 %be sure this is correct!!!
save(strcat(spec_location, filesep, names{file_no}, '_FD_rawspecs'), 'specMat')
save(strcat( segment_location, filesep, names{file_no}, '_FD_segMat'), 'segMat')

