sched=findResource('scheduler', 'configuration', 'atlasscheduler' );
load('Z:\gitRepos\fcdetectrepo\flightcall-seg\dataFiles\more_nights\new_data_names.mat')

seg_all_file1=createJob(sched);
set(seg_all_file1,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})
%name_file='Z:\gitRepos\fcdetectrepo\flightcall-seg\dataFiles\more_nights\new_data_names'
file_no=1

file_name=new_data_names{file_no}
file_locale='Z:\gitRepos\fcdetectrepo\flightcall-seg\dataFiles\more_nights';
load(strcat(file_locale, file_name, '_raw_spec.mat'))
segMatRef=parseCSV(strcat(file_locale, filesep, file_name, '.csv'), 0);
for i=1:length(specMat)
    label=sprintf('labels for file_%s',file_name);
    args={label, .025, 6, specMat{i,1}, segMatRef(i,:)};
    createTask(seg_all_file1, @computeDistrJob_labelNewData, 8, args)
end

submit(seg_all_file1)
job1_outputs=seg_all_file1.getAllOutputArguments


s='standard_seg_filtering';
m='two_seg_filters';


label_prefix='labeled_data_';
base_dir='Z:\gitRepos\fcdetectrepo\flightcall-seg\dataFiles\new_segmentations';

segMat=filter_batch_results(job1_outputs)
save_name=strcat(base_dir, filesep, s, filesep, label_prefix, file_name);
save(save_name, 'segMat');

segMat=filter_batch_results(job1_outputs, 'match')
save(strcat(base_dir, filesep, m, filesep, label_prefix, file_name), segMat);





%##########################file2
seg_all_file2=createJob(sched);
set(seg_all_file2,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})
%name_file='Z:\gitRepos\fcdetectrepo\flightcall-seg\dataFiles\more_nights\new_data_names'
file_no=2

file_name=new_data_names{file_no}
file_locale='Z:\gitRepos\fcdetectrepo\flightcall-seg\dataFiles\more_nights';
segMatRef=parseCSV(strcat(file_locale, filesep, file_name, '.csv'), 0);
for i=1:length(specMat)
    label=sprintf('labels for file_%s',file_name);
    args={label, .025, 6, specMat{i,1}, segMatRef(i,:)};
    createTask(seg_all_file2, @computeDistrJob_labelNewData, 8, args)
end
submit(seg_all_file2)

job2_outputs=seg_all_file2.getAllOutputArguments
save_seg_mat(s, file_name, filter_batch_results(job2_outputs))
save_seg_mat(m, file_name, filter_batch_results(job2_outputs, 'match'))

%##########################file3
seg_all_file3=createJob(sched);
set(seg_all_file3,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})
%name_file='Z:\gitRepos\fcdetectrepo\flightcall-seg\dataFiles\more_nights\new_data_names'
file_no=3

file_name=new_data_names{file_no}
file_locale='Z:\gitRepos\fcdetectrepo\flightcall-seg\dataFiles\more_nights';
load(strcat(file_locale, file_name, '.wav'))
segMatRef=parseCSV(strcat(file_locale, filesep, file_name, '.csv'), 0);
for i=1:length(specMat)
    label=sprintf('labels for file_%s',file_name);
    args={label, .025, 6, specMat{i,1}, segMatRef(i,:)};
    createTask(seg_all_file3, @computeDistrJob_labelNewData, 8, args)
end

submit(seg_all_file3)

job3_outputs=seg_all_file3.getAllOutputArguments
save_seg_mat(s, file_name, filter_batch_results(job3_outputs))
save_seg_mat(m, file_name, filter_batch_results(job3_outputs, 'match'))


%##########################file4
seg_all_file4=createJob(sched);
set(seg_all_file4,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})
%name_file='Z:\gitRepos\fcdetectrepo\flightcall-seg\dataFiles\more_nights\new_data_names'
file_no=4

file_name=new_data_names{file_no}
file_locale='Z:\gitRepos\fcdetectrepo\flightcall-seg\dataFiles\more_nights';
segMatRef=parseCSV(strcat(file_locale, filesep, file_name, '.csv'), 0);
for i=1:length(specMat)
    label=sprintf('labels for file_%s',file_name);
    args={label, .025, 6, specMat{i,1}, segMatRef(i,:)};
    createTask(seg_all_file4, @computeDistrJob_labelNewData, 8, args)
end

submit(seg_all_file4)

job4_outputs=seg_all_file4.getAllOutputArguments
save_seg_mat(s,file_name, filter_batch_results(job4_outputs))
save_seg_mat(m, file_name, filter_batch_results(job4_outputs, 'match'))


%#############################first as well
file_name='DANBY_20090912_194900_NFC_637'
save_seg_mat(s,file_name, filter_batch_results(jobOut))
jobOut_clean(~iscell(jobOut_clean),:)=[];
jobOut_clean( all(cellfun(@isempty,jobOut_clean),2), : ) = [];


save_seg_mat(m,file_name, filter_batch_results(jobOut_clean, 'match'))

