

%attempt to work into "distributed form"%image matrix script
%clusterTest
%type onespec_script
clear all
close all

%settings
onClust= true;
workerSave=false;
len= 1;  %if 0, use all data from csv
highSig= .08;
lowSig= .079;
step= .015;

%data and file locations
recInfo= {'DANBY', '20090912', '194900'}
csv_name='C:\Users\neb76\Desktop\nick_FCDetect_beforGit\DANBY_20090912_194900_NFC_637.csv'
%% make location for run
runDate= datestr(clock, 'mmmdd_');
outerDirN= datestr(clock, 'HH_MM_SS');
outerDirN= strcat(runDate, 'TestRunAt_', outerDirN );
outerDirN= regexprep(outerDirN,'\.','pnt');
mkdir(outerDirN);



%%
%move latest files into scheduler location
% copyfile(, 'C:\Users\neb76\Documents\MATLAB', 'f');
%create job objects

if onClust
    sched= findResource('scheduler', 'configuration', 'atlasscheduler' );
    clustJ= createJob(sched);
    set(clustJ,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})
else
    sched= findResource();
    copyfile('Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\*',  'C:\Users\neb76\Documents\MATLAB' );  %'C:\Users\neb76\AppData\Roaming\MathWorks\MATLAB\local_scheduler_data\R2011b'
    clustJ= createJob(sched);

end

    

segMat= parseCSV(csv_name, len);
jobN= 7* floor((highSig-lowSig)/step); %there is a hard coded first number for number of settings per sigma value

seedSet= cell(jobN, 4);
jobI= 1;
s= highSig;

while s>lowSig
    sigStBase= strcat('sigma_ ', num2str(s))
    
    %orig norm
        sigSt= strcat('OrigNorm_', sigStBase);
        
        id= strcat('raw   ', sigSt);
        seedSet(jobI, :)= {id, s, 0, 3};
        jobI= jobI+1;

        id= strcat('R   ', sigSt);
       seedSet(jobI, :)= {id, s, 1, 3};
        jobI= jobI+1;

        id= strcat('S   ', sigSt);
       seedSet(jobI, :)= {id, s, 2, 3};
        jobI= jobI+1;

        id= strcat('SR   ', sigSt);
       seedSet(jobI, :)= {id, s, 3, 3};
        jobI= jobI+1;

        id= strcat('RS   ', sigSt);
       seedSet(jobI, :)= {id, s, 4, 3};
        jobI= jobI+1;

        id= strcat('RSR   ', sigSt);
       seedSet(jobI, :)= {id, s, 5, 3};    
        jobI= jobI+1;
        
        id= strcat('RCub   ', sigSt);
       seedSet(jobI, :)= {id, s, 6, 3};    
        jobI= jobI+1;
        
    
%     SYM norm
        sigSt= strcat('SymNorm_', sigStBase);

    
        id= strcat('raw   ', sigSt);
       seedSet(jobI, :)= {id, s, 0, 2};        
        jobI= jobI+1;

        id= strcat('R   ', sigSt);
       seedSet(jobI, :)= {id, s, 1, 2};
        jobI= jobI+1;

        id= strcat('S   ', sigSt);
       seedSet(jobI, :)= {id, s, 2, 2};
        jobI= jobI+1;

        id= strcat('SR   ', sigSt);
       seedSet(jobI, :)= {id, s, 3,  2};
        jobI= jobI+1;

        id= strcat('RS   ', sigSt);
       seedSet(jobI, :)= {id, s, 4,  2};
        jobI= jobI+1;

        id= strcat('RSR   ', sigSt);
       seedSet(jobI, :)= {id, s, 5, 2};    
        jobI= jobI+1;
        
        id= strcat('RCub   ', sigSt);
       seedSet(jobI, :)= {id, s, 6,  2};    
        jobI= jobI+1;
        
%    RM norm
        sigSt= strcat('RmNorm_', sigStBase);

        id= strcat('raw   ', sigSt);
       seedSet(jobI, :)= {id, s, 0, 1};
        jobI= jobI+1;

        id= strcat('R   ', sigSt);
       seedSet(jobI, :)= {id, s, 1, 1};
        jobI= jobI+1;

        id= strcat('S   ', sigSt);
       seedSet(jobI, :)= {id, s, 2, 1};
        jobI= jobI+1;

        id= strcat('SR   ', sigSt);
       seedSet(jobI, :)= {id, s, 3, 1};
        jobI= jobI+1;

        id= strcat('RS   ', sigSt);
       seedSet(jobI, :)= {id, s, 4, 1};
        jobI= jobI+1;

        id= strcat('RSR   ', sigSt);
       seedSet(jobI, :)= {id, s, 5, 1};    
        jobI= jobI+1;
        
        id= strcat('RCub   ', sigSt);
       seedSet(jobI, :)= {id, s, 6, 1};    
        jobI= jobI+1;
        
  
end
%%
%create file system, all working
%ensure global scope for later saving

%load from filesystem,  
if workerSave
    %TODO: implement what i want to do to get the files in place...
else
    for s= 1:size(segMat,1);
        specDirN=  strcat( 'specsFor_ ', num2str(fix(segMat{s, 2})) )  %parse together a dir name for each time interval
        mkdir([outerDirN filesep specDirN])
        for n= 1:3
            normDirN= strcat('normCse_', num2str(n));
            mkdir([outerDirN filesep specDirN filesep normDirN]);
            for t= 0:6
                transDirName= strcat('transCse_', num2str(t));
                mkdir([outerDirN filesep specDirN filesep normDirN filesep transDirName])
            end
        end    
    end
    
end
%%   

%what is better, send each spec only only once, 
for specI= 1:size(segMat,1)
    spec= getspecBatch(1024, recInfo{:}, segMat{specI, 2:3});
    specDirN=  strcat( 'specsFor_ ', num2str(fix(segMat{specI, 2})) ) ; %parse together a dir name for each time interval
    for j= 1:size(seedSet,1)  %make sure this is a row vector and not column vector      
        curJob= seedSet(j,:);
        if workerSave
            createTask(clustJ, @computeDistrJob_workerSv, 1, { curJob{:}, spec, specDirN});  %TODO: remove debug piece of having returned segments for the client writing            
        else
            createTask(clustJ, @computeDistrJob_clientSv, 1, { curJob{:}, spec});
            
        end
    end
       
end
   
submit(clustJ);
wait(clustJ);
jobOut= clustJ.getAllOutputArguments;
%%
%save files into filesystem
if ~workerSave
    %use same loop, because it should be same sized list!
    %
    
    k = waitforbuttonpress ; %click on figure box
    
    segInd=1; %initialize result index to 1
    for specI= 1:size(segMat,1)
        specDirN=  strcat( 'specsFor_ ', num2str(fix(segMat{specI, 2})) );  %parse together a dir name for each time interval
        for j= 1:size(seedSet,1)  %make sure this is a row vector and not column vector
            jobStats= seedSet(j,:); %In the order of the constructor, in this case id, sig, trans, normcase
            curSeg= segList{segInd, 1};
            segInd= segInd+1;
            saveStr= [outerDirN filesep specDirN filesep 'normCse_' num2str(jobStats{4})...
                filesep 'transCse_' num2str(jobStats{3})...
                filesep 'sigma_' jobStats{2}];
            f= figure;
            if curSeg==1
                errMssg= strcat(jobStats{1}, ' does not converge at eigenvalue')
                title(errMssg)
                saveStr= strcat(saveStr, 'N');
                
            else
                imagesc(curSeg);
                title(jobStats{1});
                saveStr= strcat(saveStr, 'Y');
            end
            saveas(f, [saveStr '.jpg']);
            close(f)
        end
        
    end
end
    


