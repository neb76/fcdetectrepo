%jobSeed is cell of things needed to make a segJob
%issue with jobSeed, so arguements will be the expansion
function[segged, seed, report]= computeDistrJob_clientSv( id, sig, transF, normCase, spec)
    report= 'no Object';
    curJob= segJob(id, sig, transF, normCase); %construct object from passed in parameters
    seed= curJob.getSeedCell;
        try           
            segged= curJob.segmentThis(spec); %TODO: check if spec stays in scope once you enter the partfor loop?            
            report= 'succes';
        catch ME
            segged= 1;
            report= getReport(ME,'extended');
        end
end