
%for use in try to figure out swiggley
%sig, transF, normCase, filt, spec, segMatRef
%     job info                    spec infp

%all top outpus now rewritten as 1
function[segged, topPcnt, icc, seed, whichSpec, report, U, match]= computeDistrJob_labelNewData(id, sig, k, spec, segMatRef )
    match=true;
    report= 'no Object';
    curJob= segJob(id, sig, k); %construct object from passed in parameters
    whichSpec=segMatRef{1,1};
    seed=  curJob.getSeedCell;
        try           
            [segged, U]= curJob.segmentThis(spec); %TODO: check if spec stays in scope once you enter the partfor loop?            
            report= 'succes';
            if isnumeric(curJob.filtSet)
                if curJob.filtSet==100
                    icc= .99;
                end
            else
                icc= curJob.findICC_classFunc(spec, segged);
            end
            segged=curJob.postprocess(segged);
            [~, loudInd]=  curJob.getLoudSeg(spec, segged);
            [~, mRange]= curJob.getJobMask(spec, segMatRef{1,4:5});  %switched to cell, see if it works
            [topPcnt, topReg, boxInd]= curJob.boxROI(segged, mRange, segMatRef{1,2:3});
                        
            if loudInd~=boxInd
                match=false;
                %the reporting out strange becuase we swap box index with
                %whatever is 1
                if loudInd~=1
                    report=['no match, loud is: ' num2str(loudInd) ' box idx is: 1'];
                else
                    report=['no match, loud is: ' num2str(boxRoi) ' box idx is: 1'];
                end
            end
            
            %do a swap, like in postProcessing
            %swap box ind and current '1', so box index is 1, and 
            segged(segged==1)=boxInd;
            segged(topReg)= 1;
            
        catch ME
            segged= 1;
            U=[1 1 1];
            report= getReport(ME,'extended');
            icc=.013;
            topPcnt=.013;
            boxInd=9;
        end
end