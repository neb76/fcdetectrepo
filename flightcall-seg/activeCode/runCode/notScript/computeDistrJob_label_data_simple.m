
%use this for testing with new segmentation
%check if segmentation still works with processing

%all top outpus now rewritten as 1
%
%   'spectral'  -1
%   'simple'    -2
%   'both'      -3

% segmentations_cell
%    segmentation    toppcnt    match    report     ref_entry

function[segmentation_cell, job_specs]= computeDistrJob_label_data_simple(id, spec_chunk, ref_chunk, seg_type_string )
filt_num=1;  %no dsp filters, just lowpass triming on simple_seg
curJob= segJob(id);
job_specs= curJob.getSeedCell; %use this in case job results are lost
segmentation_cell=cell(size(spec_chunk,1),5);
case_id=1;
switch seg_type_string
    case 'spectral'
        case_id=1;
    case 'simple'
        case_id=2;
    case 'both'
        case_id=3;
    otherwise
        case_id=1;
end


for i=1:length(spec_chunk)
    match=true; %this set to false if "box" segment is not loudest
    did_not_fail=true;
    cur_spec=spec_chunk{i,1};
    %try spectral if indicated
    if case_id~=2
        try
            [segged, ~, sym_time]= curJob.segmentThis(cur_spec); %TODO: check if spec stays in scope once you enter the partfor loop?
            report= 'succes';
            
        catch ME
            if case_id==1
                %fail and report
                segged= 1;
                report= getReport(ME,'extended');
                topPcnt=.013;
                boxInd=9;
                match=false;
                did_not_fail=false;
            end
            if case_id==3
                %fail and revert to simple
                pocessed_spec=processpic_for_simpleseg(cur_spec, filt_num, 0); %
                segged=simpleSegment(pocessed_spec, thresh_val);
                report='reverted to simple';
            end
        end
    else
        pocessed_spec=processpic_for_simpleseg(cur_spec, filt_num, 0); %
        segged=simpleSegment(pocessed_spec, thresh_val);
        report='simple only';
    end
    
    
    segMatRef=ref_chunk(i,:);
    if did_not_fail
        [~, loudInd]=  curJob.getLoudSeg(cur_spec, segged);
        [~, mRange]= curJob.getJobMask(cur_spec, segMatRef{1,4:5});  %switched to cell, see if it works
        [topPcnt, topReg, boxInd]= curJob.boxROI(segged, mRange, segMatRef{1,2:3});

        if loudInd~=boxInd
            match=false;
        end   
        %do a swap, like in postProcessing
        %swap box ind and current '1', so box index is 1, and
         if topReg>0
            segged(segged==1)=boxInd;
            segged(topReg)= 1;
        end
    end
    
    new_entry={segged, topPcnt, match, report, segMatRef};
    segmentation_cell(i,:)=new_entry;
end

end