
%use this for testing with new segmentation
%check if segmentation still works with processing

%all top outpus now rewritten as 1
%
%   'spectral'  -1
%   'simple'    -2
%   'both'      -3

% segmentations_cell
%    segmentation    toppcnt    match    report     ref_entry

function[spectral_seg_cell, simple_seg_cell, job_specs]= computeDistrJob_label_data_simple_simultaneous(id, spec_chunk, ref_chunk )
curJob= segJob(id);
job_specs= curJob.getSeedCell; %use this in case job results are lost
spectral_seg_cell=cell(size(spec_chunk,1),5);
simple_seg_cell=cell(size(spec_chunk,1),5);

for i=1:length(spec_chunk)       
    cur_spec=spec_chunk{i,1};
    segMatRef=ref_chunk(i,:);
    [~, mRange]= curJob.getJobMask(cur_spec, segMatRef{1,4:5});
    
  %first, simple
    segged=simpleSegment(cur_spec);
    report='simple_only';
    
    [~, loudInd]=  curJob.getLoudSeg(cur_spec, segged);
    [topPcnt, topReg, boxInd]= curJob.boxROI(segged, mRange, segMatRef{1,2:3});
    match=(loudInd~=boxInd)
    
    %do a swap, like in postProcessing
    %swap box ind and current '1', so box index is 1, and
    if topReg>0
        segged(segged==1)=boxInd;
        segged(topReg)= 1;
    end    
    simple_seg_cell_entry={segged, topPcnt, match, report, segMatRef};
    simple_seg_cell(i,:)=simple_seg_cell_entry;
    
 %second, spectral   
    try
        [segged, ~, sym_time]= curJob.segmentThis(cur_spec); %TODO: check if spec stays in scope once you enter the partfor loop?
        report= sprintf('spec_succes: %f seconds', sym_time);
        segged=curJob.postprocess(segged);
        [~, loudInd]=  curJob.getLoudSeg(cur_spec, segged);
        [topPcnt, topReg, boxInd]= curJob.boxROI(segged, mRange, segMatRef{1,2:3});
        match=(loudInd~=boxInd);        
        %do a swap, like in postProcessing
        %swap box ind and current '1', so box index is 1, and
        if topReg>0
            segged(segged==1)=boxInd;
            segged(topReg)= 1;
        end            
    catch ME
        %fail and report
        segged= 1;
        report= getReport(ME,'extended');
        topPcnt=.013;
        match=false;
    end   
    spec_seg_cell_entry={segged, topPcnt, match, report, segMatRef};
    spectral_seg_cell(i,:)=spec_seg_cell_entry;

end


end