
%for use in try to figure out swiggley
%sig, transF, normCase, filt, spec, segMatRef
%     job info                    spec infp
function[segged, boxInd, topPcnt, icc, seed, report, U]= computeDistrJob_render(id, sig, k, spec, segMatRef )
    
    report= 'no Object';
    curJob= segJob(id, sig, k); %construct object from passed in parameters
    seed= [segMatRef{1,1}, curJob.getSeedCell];
        try           
            [segged, U]= curJob.segmentThis(spec); %TODO: check if spec stays in scope once you enter the partfor loop?            
            report= 'succes';
            if isnumeric(curJob.filtSet)
                if curJob.filtSet==100
                    icc= .99;
                end
            else
                icc= curJob.findICC_classFunc(spec, segged);
            end
            [~, loudInd]=  curJob.getLoudSeg(spec, segged);
            [~, mRange]= curJob.getJobMask(spec, segMatRef{1,4:5});  %switched to cell, see if it works
            [topPcnt, ~, boxInd]= curJob.boxROI(segged, mRange, segMatRef{1,2:3});
            if loudInd~=boxInd
                report=['no match, loud is: ' num2str(loudInd) ' box idx is: ' num2str(boxInd) 'score is: ' num2str(topPcnt)];
            end
            
        catch ME
            segged= 1;
            U=[1 1 1];
            report= getReport(ME,'extended');
            icc=.013;
            topPcnt=.013;
            boxInd=9;
        end
end