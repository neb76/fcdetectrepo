
%for use in try to figure out swiggley
%sig, transF, normCase, filt, spec, segMatRef
%     job info                    spec infp

%all top outpus now rewritten as 1
function[seg_cell, times_cell, seed, report]= computeDistrJob_segment_false_detections(id, sig, k, spec_chunk, chunk_times )
    if ~iscell(chunk_times)
        times_cell=num2cell(chunk_times);
    else
        times_cell=chunk_times;
    end
    
    report= cell(length(spec_chunk),1);
    seg_cell=cell(length(spec_chunk), 1);
    curJob= segJob(id, sig, k); %construct object from passed in parameters
    seed=  curJob.getSeedCell;
    for i=1:length(spec_chunk)
        cur_spec=spec_chunk{i,1};

        try           
            [segged, U]= curJob.segmentThis(cur_spec); %TODO: check if spec stays in scope once you enter the partfor loop?            
            report{i,1}= 'succes';

            segged=curJob.postprocess(segged);
            max_ind=max(max(segged))+1;
            segged(segged==1)= max_ind;
            
        catch ME
            segged= 1;
            report{i,1}= getReport(ME,'extended');
        end
        seg_cell{i,1}=segged;
    end
end