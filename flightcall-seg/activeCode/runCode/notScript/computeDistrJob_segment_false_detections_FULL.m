%figure out the ID for each
    %current thinking:   bring in from client in segMatRef


function[combined_seg_chunk, simple_seg_chunk, specMat_chunk, seed]= computeDistrJob_segment_false_detections_FULL(night_then_chunk_id, clip_chunk, ref_chunk )

   
    curJob= segJob(sprintf('chunk: %f  for night: %f   ,  full fd segmentation', night_then_chunk_id(1), night_then_chunk_id(2) )); %construct object from passed in parameters
    seed=  curJob.getSeedCell;
    combined_seg_chunk=cell(size(ref_chunk,1), 4);
    simple_seg_chunk=cell(size(ref_chunk,1), 4);
    specMat_chunk=cell(size(ref_chunk,1), 2);
    
    for i=1:size(clip_chunk,1)        
        cur_id=ref_chunk{i,1};
        cur_spec=make_specific_spec(clip_chunk{i});
        specMat_entry={cur_spec, cur_id};
        specMat_chunk(i,:)=specMat_entry;
        %   make simple
           
        simple_segged=simpleSegment(cur_spec)
        simple_entry={simple_segged, cur_id, 'simple', ref_chunk{i}};
        simple_seg_chunk(i,:)=simple_entry;
        try           
            [spec_segged, U]= curJob.segmentThis(cur_spec); %TODO: check if spec stays in scope once you enter the partfor loop?                        
            spec_segged=curJob.postprocess(spec_segged);
            max_ind=max(max(spec_segged))+1;
            spec_segged(spec_segged==1)= max_ind;
            combo_tech='spec';
        catch ME
            spec_segged= simple_segged;
            combo_tech='simple';
        end
        combo_entry={spec_segged, cur_id, combo_tech, ref_chunk{i}};
        combined_seg_chunk(i,:)=combo_entry;
        
    end
    
    write_base='/home/fs01/neb76/FULL_false_detection_segs/';
    this_chunk_seg=['night_' num2str(night_then_chunk_id(1)) filesep  'segMat_chunk_' num2str(night_then_chunk_id(2)) '.mat'];
    this_chunk_spec=['night_' num2str(night_then_chunk_id(1)) filesep  'specMat_chunk_' num2str(night_then_chunk_id(2)) '.mat'];
    
    segMat_simple=simple_seg_chunk;
    save([write_base 'simple' filesep this_chunk_seg], 'segMat_simple');
    segMat_combined=combined_seg_chunk;
    save([write_base 'combined' filesep this_chunk_seg], 'segMat_combined');
    specMat=specMat_chunk;
    save([write_base 'spec_mats' filesep this_chunk_spec], 'specMat');
    %write to /tmp folder!
        %consider a temp case
end