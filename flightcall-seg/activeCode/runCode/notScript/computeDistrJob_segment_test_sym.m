%made on 12/18 to ensure the true symmetry
function[seg_cell, av_time, seg_times, seed, report]= computeDistrJob_segment_test_sym(id, sym_check_arg, spec_chunk)

    
    report= cell(length(spec_chunk),1);
    seg_cell=cell(length(spec_chunk), 1);
    curJob= segJob(id, sym_check_arg); %construct object from passed in parameters
    seed=  curJob.getSeedCell;
    seg_times={};
    for i=1:length(spec_chunk)
        cur_spec=spec_chunk{i,1};
  cur_seg_time=99;
        try           
         
            [segged, U, cur_seg_time]= curJob.segmentThis(cur_spec); %TODO: check if spec stays in scope once you enter the partfor loop?            
            report{i,1}= 'succes';

            segged=curJob.postprocess(segged);
            max_ind=max(max(segged))+1;
            segged(segged==1)= max_ind;
        catch ME
            segged= 1;
            report{i,1}= getReport(ME,'extended');
        end
                    seg_times{end+1}=cur_seg_time;

        seg_cell{i,1}=segged;
       
    end
    seg_times=cell2mat(seg_times);
    av_time=mean(seg_times~=99);
end