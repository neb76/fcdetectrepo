
%for use in try to figure out swiggley
function[segged, seed, report]= computeDistrJob_tryFilt( id, sig, transF, normCase, spec, k)
    report= 'no Object';
    curJob= segJob(id, sig, transF, normCase); %construct object from passed in parameters
    seed= curJob.getSeedCell;
    if sig== .04
        curJob.filtSet= 2;
    else
        curJob.filtSet= 3;
    end
    curJob= curJob.adjustK(k);
        try           
            segged= curJob.segmentThis(spec); %TODO: check if spec stays in scope once you enter the partfor loop?            
            report= 'succes';
        catch ME
            segged= 1;
            report= getReport(ME,'extended');
        end
end