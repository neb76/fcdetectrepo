%settings for segmentation job set by curSeg class
%modify constructor of acording varying parameters for experiment\
%set id for the job inside this function, gives record for later job retrieval


%### notes,
%dumies for failures, reported out task result vector
%   .0133- non-convergence of the eigen decomposition
%   .0123 used if

%normcase, describes how to normalize laplacian
    %1 is random walk, 2 is symetric

%jobSeed is cell of things needed to make a segJob
%issue with jobSeed, so arguements will be the expansion

%arg logic?  1-4- make segJob,  spec,   clusters, row-ofSegmat
%     ID     t-start     t-finish     l-freq     h-freq


%report gives error if no convergence, 1 if box seg is loudest, and 0 if
%they don't match
% function[segged, topPcnt, seed, report]= computeDistrJob_ioVal( id, sig, transF, normCase, k, spec, segMatRow)
function[seed, resultVec, avScore, iccVec, avICC, nonMatchTot, nonConvergeTot, errLog]= computeDistrJob_valComboFilt( sig, k)
specNum= 633;  %for the number of specs you want to read.  For now, this is a debug condition

%specify which of available groups of pre-generated specsbelow which you would like, choices:
%       allspecs_sheetLngths.mat  ||  allspecs_p4_rand_p2.mat  ||  allspecs_p4_centered2.mat
specFileN= 'allspecs_p4_centered2.mat';
specFileOrg= ['/home/fs01/neb76/Documents/MATLAB/atlasscheduler/R2011b/' specFileN];

loadName= ['/tmp/' specFileN];
if exist(loadName, 'file')==0  %this check prevents race conditions-- before ATLAS revision midsummer, the nodes would fail when multple cores on one node copied/overwrote the matrix of spectrograms
    copyfile(specFileOrg, '/tmp');
end
if exist('/tmp/segMatRef.mat', 'file')==0
    copyfile('/home/fs01/neb76/Documents/MATLAB/atlasscheduler/R2011b/segMatRef.mat', '/tmp');
end
% specMat= load(loadName, 'specMat');
% segMatRef= load('/tmp/segMatRef.mat', 'segMatRef');

load(loadName, 'specMat');
load('/tmp/segMatRef.mat', 'segMatRef');


% ** segMatRef and specMat now in workspace
%       segMatRef-- 633x5 cell array from spreadsheet
%            segNum    start    stop    lowfrew   highFreq
%        specMat-- 633x1 cell array of spectrograms
%        non specs, become 512 x 13, all ones matrix.


%set up job
id= 'new combination filter job';  %for this job, deleted arguements ID and K
curJob= segJob(id, sig, k);
seed= curJob.getSeedCell ;

nonMatchTot= 0;
nonConvergeTot= 0;
errLog= cell(1,3);   %for writing out errors --specNumber, type, report

%segment for all specs
resultVec= zeros(1,specNum);  %this comes from loaded variable file
iccVec= zeros(1,specNum);
for s= 1:specNum    %change to size(specMat,1) to do all
    spec= specMat{s,1};
    if size(spec, 2)==13
        if spec(1,1)==1
            errLog{end+1, 1} = s;
            errLog{end, 2}= 'noSpec';
            errReport= ['geSpec failed'];
            errLog{end, 3}= errReport;
            resultVec(s)= 0.0123;
            iccVec(s)= 0.0123;
            continue
        end
    end
    topPcnt= 0;
    %construct object from passed in parameters
    try
        segged= curJob.segmentThis(spec);
        icc= findICC_eval(spec, segged, curJob.clustK);
        [~, loudInd]=  curJob.getLoudSeg(spec, segged);
        [~, mRange]= curJob.getJobMask(spec, segMatRef{s,4:5});  %switched to cell, see if it works
        [topPcnt, ~, boxInd]= curJob.boxROI(segged, mRange, segMatRef{s,2:3});
        %check if loudest segment is "boxRoi segment", log error report if not
        if loudInd~=boxInd
            nonMatchTot= nonMatchTot + 1;
            errLog{end+1, 1} = s;
            errLog{end, 2}= 'nonMatch';
            errReport= ['no match, loud is: ' num2str(loudInd) ' box idx is: ' num2str(boxInd) 'score is: ' num2str(topPcnt)];
            errLog{end, 3}= errReport;
        end
        resultVec(s)= topPcnt;
        iccVec(s)= icc;
        
    catch ME
        nonConvergeTot= nonConvergeTot + 1;
        errLog{end+1, 1} = s;
        errLog{end, 2}= 'nonCon';
        errReport= getReport(ME, 'extended');
        errLog{end, 3}= errReport;
        
        resultVec(s)= 0.0133; %dummy value, cleared out later by finding average without, and replacing
        iccVec(s)= 0.0133; %dummy value, cleared out later by finding average without, and replacing
    end
    
end

goodNums= resultVec(resultVec~=0.0133);
goodNums= goodNums(goodNums~=0.0123);
avScore= mean(goodNums);

goodICC=iccVec(iccVec~=.0133);
goodICC=goodICC(goodICC~=.0123);
avICC= mean(goodICC);

end