%function for cluster segmentations
function[segged]= computeDistrJob_workerSave( id, sig, transF, normCase, spec, specDir, outerDirN)
    homeDir = pwd();
    copyfile([homeDir '*.m'], '$TMPDIR']);
    cd('$TMPDIR')
    curJob= segJob(id, sig, transF, normCase); %construct object from passed in parameters
    curJob.filtSet= 2;
        try           
            segged= curJob.segmentThis(spec); %TODO: check if spec stays in scope once you enter the partfor loop?            
        catch ME
            segged= getReport(ME,'extended');
        end
end

