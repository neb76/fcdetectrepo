function [dif]= findDifH(spec, x)
    dimF= size(spec, 1);
    dimT= size(spec, 2);
    dimAff= dimF*dimT;
    [i, j]= ind2sub([dimAff dimAff], x);
    dif= abs(spec(i)-spec(j));
end