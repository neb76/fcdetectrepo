%operating on the processed picture

function [ icc ] = findICC_eval(orig, seg, k)
%used to evaluate clustering.  Takes in the segmentation and the original
%specgrogram, returns the inter vs. intracluster variance


pspec= processpic(orig, 1, 0); %unfiltered

dimF= size(pspec, 1);
dimT= size(pspec, 2);
allpoints= reshape(pspec, dimF*dimT, 1);
btv= var(allpoints); %variacle for all pic

varVec= 1:k;
for i= 1:k
    clustIs= seg==i;
    clustPoints= pspec(clustIs);
    varVec(i)= var(clustPoints);
end

icc= btv^2/((sum(varVec))^2 + btv^2);
end

