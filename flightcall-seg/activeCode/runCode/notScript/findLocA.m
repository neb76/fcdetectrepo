%this is entirely for local scaling
%creates annomyous function for appyling to all elements in index cell
%

function [bothSigs]= findLocA(sigMat, x)
    dimF= size(sigMat, 1);
    dimT= size(sigMat, 2);
    dimAff= dimF*dimT;
    [i, j]= ind2sub([dimAff dimAff], x);
    sigI=sigMat(i);
    sigJ=sigMat(j);
    bothSigs=sigI.*sigJ;
end