function [lap] = getLap( A, normCase)
%hardcode param, mainly for debugging
% normCase= 3;

%check to ensure sparsity
if ~issparse(A)
    pr= 'not sparse'
    return
end

sumvec = sum(A,2);
pr = 'getting laplacian...'

pr= 'zeros in laplacian'
size(sumvec(sumvec == 0))
% compute the normalized laplacian
sumvec(sumvec == 0) = eps;
pr= 'zeros are gone'

%debug case, recovered old laplacian after inability to reporduce wonky,
%nice results
if normCase== 3
    lap= -A;
    sumvec = sum(A,2);
    lap= spdiags(-sumvec, 0, lap); 
    sqsumvec = sqrt(sumvec);
    lap= bsxfun(@rdivide,lap, sqsumvec);
    lap= bsxfun(@rdivide,lap, sqsumvec');
end



%######   this is the L-sym normalization
if normCase== 2
    sqsumvec = sqrt(sumvec);
    lap= bsxfun(@rdivide,A, sqsumvec);
    lap= bsxfun(@rdivide,lap, sqsumvec');
end


%######  this the L-rw normalization
if normCase== 1
    lap= bsxfun(@rdivide,A, sumvec');
end



pr = 'normalized..'

lap= eye(size(lap)) - lap;
lap= sparse(lap);
if ~issparse(lap)
    pr= 'outpus is not sparse'
    return
end
% TODO- 
pr= 'sparse laplacian sucess, now decompossing...'
end


