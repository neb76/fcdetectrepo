% e.g.   getspec('DANBY','20090912','194900',1259.79,1259.89)
%               change     spectrogram(A,round(factor)*70,round(factor)*10,512,samRate,'yaxis')
%                   to     spectrogram(A,round(factor)*70,round(factor)*10,1024,samRate,'yaxis')


function [p_new,f,t] = make_specific_spec(sound_clip)
DURATION=.4;
fftSize=1024;
% close all;clc;
%% Some variables
samRate=24000; % Sampling rate of recording
set(0,'DefaultFigureWindowStyle','docked')
 
    A=sound_clip;
    
    % A Butterworth filter
    A=highpassfilter(A,samRate,2500);
    
    % Calculating duration and factor levels
    duration=DURATION;
    durSamples=duration/(1/samRate);
    factor =durSamples/3000;
    
    %% Normalizing the spectrogram
    
    [~,f,t,p]=spectrogram(A,round(factor)*70,round(factor)*10,fftSize,samRate);  %original
    p_new=10*log10(abs(p)); %e.g. 257x37
    
    %Normalizing
    %p_new=p_new./repmat(max(p_new,[],1),size(p_new,1),1);
    
    %%Median Filter
    %p_new = medfilt2(p_new, [3 3]);
    
    %% Gaussian Filter
    H=fspecial('gaussian',[3 3],0.2);
    p_new=imfilter(p_new,H,'replicate');

    p_new= flipud(p_new);
end