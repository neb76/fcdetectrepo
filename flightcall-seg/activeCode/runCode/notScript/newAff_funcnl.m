function [ outAff ] = newAff_funcnl( data, distProcCase, sigma, neigh )
%newAff_funcnl new caluclation of affinity matrix
%   Uses sparse-apply and anonymous function, achieves tremendous speed up!
%  6/28/2012  nicholas bruns

%for now, all are using the gausian kernel
%afftypes, according to which part of Range, Square, Range
    %case 0: raw
    %case 1: R
    %case 2: S
    %case 3: SR
    %case 4: RS
    %case 5: RSR
    %case6: R-cubic (which before produced top results)

%neigh:
%    -100- local scaling, uses 8 connected neighbors     here, sigma param
%       is used to say WHICH neighbor is best
%       
%         sigma range, is thus 1-8

%    -99- original, with original bug
%    -8- original, 8-connected neigbors
%    -12- 8 connected with extra dimensions on main axes
%leading range to [0,1]
% for local scaling, 7 is paper's default
if distProcCase==1 || distProcCase==4 || distProcCase==5 ||distProcCase==6 
    oldmin= min(min(data));
    oldmax= max(max(data));
    oldrange= oldmax-oldmin;
    data= (data-oldmin)/oldrange;
end

%square transform
if distProcCase==2 || distProcCase==3 || distProcCase==5 
        data= data.^2;
end


%cubic transform
if distProcCase==6 
        data= data.^3;
end


%post transform range to [0,1]
if distProcCase==3 || distProcCase==5
    oldmin= min(min(data));
    oldmax= max(max(data));
    oldrange= oldmax-oldmin;
    data= (data-oldmin)/oldrange;
end
            
dimF= size(data, 1);
dimT= size(data, 2);
dimAff= dimF*dimT;

novar= exist('neigh', 'var');

%original setting, but now with edges fixed
if novar== 0 || neigh==8 || neigh>=100 
    B= ones(dimAff, 4); %4 new bands
    %allighment is adjusted and non-intuitive due internal adjustments from spdiags
    ne=repmat([ones(dimF-1,1);0],dimT,1);  %skip NW diag if on top edge
    s_se=repmat([0;ones(dimF-1,1)],dimT,1); %skip going down if on bottom edge
    d= [ 1, (dimF-1), dimF, (dimF+1)]; %set dimmensions of SP diags
    B(:,1)= s_se;
    B(:,2)= ne;
    B(:,4)= s_se;    
    iMat= spdiags(B, d, dimAff, dimAff);
    iMat=iMat +iMat'; %reflect to get both directions 

elseif neigh==12
    B= ones(dimAff, 6); %6 new bands
    ne=repmat([ones(dimF-1,1);0],dimT,1);
    s_se=repmat([0;ones(dimF-1,1)],dimT,1); %pad with a zero to correct allighment
    s2=repmat([0;0;ones(dimF-2,1)],dimT,1);

    d= [ 1, 2, (dimF-1), dimF, (dimF+1), 2*dimF];
    B(:,1)= s_se;
    B(:,2)= s2;
    B(:,3)= ne;
    B(:,5)= s_se;
        
    iMat= spdiags(B, d, dimAff, dimAff);

    %new edge cases
    iMat=iMat +iMat';
    
%original procedure, which we will keep to replicate old errors
elseif neigh==99
    B= ones(dimAff, 8); 
    d= [-(dimF+1), -dimF, -(dimF-1), -1, 1, (dimF-1), dimF, (dimF+1)];

    iMat= spdiags(B, d, dimAff, dimAff);
    %edge cases
    iMat(1, dimF) = 0; %NW corner has no NE neighbor
    iMat(dimF, 1) = 0;%reflection
    iMat(end-(dimF-1), end) = 0; %SE corner has no SW neigh
    iMat( end, end-(dimF-1))  = 0;

else
    outAff= sparse(dimAff, dimAff);
    display('not a valid NeighborhoodSize')
    return
end

iMat(find(iMat))= find(iMat); %set all nonzeros, (1's), to their index position
difFunc= @(x) findDifH(data, x);
rawAff= spfun(difFunc, iMat);  %use matrix of indeces as a guide to compute differences on original matrix

if neigh==100
    %local scalling, using 'kth' neighbor
    kthMat=ordfilt2(data, sigma, ones(3,3) );
    sigMat= abs(bsxfun(@minus, data, kthMat));  %this is dif between point and 7th neighbor
    sigMat(sigMat==0)= eps;
    localAff= @(x) findLocA(sigMat, x);
    bothSigMat= spfun(localAff, iMat);   
    sigDiv= @(x) (-rawAff(x))./bothSigMat(x);
    outAff=spfun(sigDiv, iMat);    
    outAff= spfun(@exp, outAff);    
elseif neigh==101
    %use local standard deviation
    sigMat=stdfilt(data);
    sigMat(sigMat==0)= eps;
    localAff= @(x) findLocA(sigMat, x);
    bothSigMat= spfun(localAff, iMat);   
    sigDiv= @(x) (-rawAff(x))./bothSigMat(x);
    outAff=spfun(sigDiv, iMat);     
    outAff= spfun(@exp, outAff);    
else   
   sigTran= 2*sigma^2;
    gaussify= @(x) exp(-(x/sigTran));
    outAff= spfun(gaussify, rawAff);
end

end

