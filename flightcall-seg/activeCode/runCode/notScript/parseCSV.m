function [segMat]= parseCSV(csvFile, sampleS)
%parseCSV this generates the test segments from the pre-segmented data
%format of 5 columns:
%     ID     t-start     t-finish     l-freq     h-freq

% to use all data, 
%     sampleS=0  
%########
    %make sure that you call 0 if you dont want random perm
    
[num,txt,raw]= xlsread(csvFile);
%specified size of randomly sampled data
if sampleS ~=0
    smplI= randperm(size(num, 1));
    smplI=smplI(1:sampleS);
    segs= num(smplI, 4:7);
    segs = horzcat(smplI', segs);
else
    segs= num(:, 4:7);
    iVec=1:size(num,1);
    segs = horzcat(iVec', segs);
end


segMat= num2cell(segs);
end
