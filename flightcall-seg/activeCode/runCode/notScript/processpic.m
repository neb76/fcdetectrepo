%takes spec, trims off the part filtered by low pass filter, 
    %also makes min value 0, so all positive amplitude values
function [ trimp ] = processpic( p, filt, sharp )
%TRIMPIC 
%   the 208th row cuttoff is hardcoded in after trial and error
%filt==1
    %no filter
%filt==2
    %blurring filter
%filt==3
    %disk blurring filter (very smooth)    
%filt==4
    %large linear, 5x5 matrix
%filt==5
    %attempt at sharpen
    
% trimp=p(1:208, :); %for original, 512 DFT
newH= size(p, 1);
newH= newH*.81; %from inspection
trimp=p(1:newH, :);
if sharp==1
    HAve= fspecial('average', [5 5]);
    blur=imfilter(trimp, HAve, 'replicate');
    detail= bsxfun(@minus, trimp, blur);
    trimp= trimp - detail/2;
end

if filt==2
    HAve= fspecial('average');
    trimp=imfilter(trimp, HAve, 'replicate');
end

if filt==3
    HAve= fspecial('disk', 5);
    trimp=imfilter(trimp, HAve, 'replicate');
end

%med-ave
if filt==4
    trimp=medfilt2(trimp, [8 2], 'symmetric');
    HAve= fspecial('average', [8 2]);
    trimp=imfilter(trimp, HAve, 'replicate');
end

%ave-med
if filt==5
    HAve= fspecial('average', [8 2]);
    trimp=imfilter(trimp, HAve, 'replicate'); 
    trimp=medfilt2(trimp, [8 2], 'symmetric');
end

%med-gause
if filt==6
    trimp=medfilt2(trimp, [8 2], 'symmetric');
    HAve= fspecial('gaussian', [8 2], .5)
    trimp=imfilter(trimp, HAve, 'replicate');
end

%gause-med
if filt==7
    HAve= fspecial('gaussian', [8 2], .5);
    trimp=imfilter(trimp, HAve, 'replicate');
    trimp=medfilt2(trimp, [8 2], 'symmetric');
end

%both are very poor
% if filt==5
%     HAve= fspecial('prewitt')
%     trimp=imfilter(trimp, HAve, 'replicate')
% end
% 
% if filt==6
%     HAve= fspecial('sobel')
%     trimp=imfilter(trimp, HAve, 'replicate')
% end


oldmin= min(min(trimp));%imshow(trimp, [], 'XData', [1 256], 'YData', [1 175])
if oldmin<0
    trimp= trimp-oldmin;
end

% figure
% imagesc(trimp);
% title('process pic trimming of photo')
end

