%this is used bout outer, called with last three arguements. Used for easy
%combination

%for most-
%    processpic_ingredients(p, title, filt, h)
%for gaussian, need a sigma too
%   processpic_ingredients(p, title, filt, h, sig)
% 
% ingredient list:
%0 no filt
% 1   median     2-d neigh
% 2   weiner     2-d neigh
% 3   average    2-d neigh
% 4   gaussian   2-d neigh  sigma
% 5   closing    var neigh  shape       dilation then erosion,   seems better
% 6   opening    var neigh  shape
% 7   dilation   var neigh  shape
% 8   resize


function [ trimp, title ] = processpic_ingredients( trimp, title, filt, h, varargin)
%for now, forget about this earlier "reverse sharpening"
    % sharpBool= false;
    % 
    % if sharpBool
    %     HAve= fspecial('average', [5 5]);
    %     blur=imfilter(trimp, HAve, 'replicate');
    %     detail= bsxfun(@minus, trimp, blur);
    %     trimp= trimp - detail/2;
    % end

title= [title ' && '];

if nargin>4
    if ischar(varargin{1})
        se=strel(varargin{1},h);
    end
end

% if filt== 0
%     tit= 'no filt';
% end
%median
%-- recquires 2-d h
if filt==1
    trimp=medfilt2(trimp, h, 'symmetric');
    tit=['med-(hood: ' num2str(h) ')'];
end

%weiner, alpha
if filt==2
    trimp=wiener2(trimp, h);
    tit=['wien-(hood: ' num2str(h) ')'];
end

%average-- recquires 2-d h
if filt==3
    HAve= fspecial('average', h);
    trimp=imfilter(trimp, HAve, 'replicate');
    tit=['avg-(hood: ' num2str(h) ')'];
end

%gaussian, specify sigma and neigh
%-- recquires 2-d h
if filt==4
    HAve= fspecial('gaussian', h, sig)
    trimp=imfilter(trimp, HAve, 'replicate');
    tit=['gaus-(hood: ' num2str(h) ') (sig: ' num2str(sig) ')'];
end

%closing, var argin is the type
if filt==5
    trimp=imclose(trimp, se);
    tit= ['clse ' varargin{1} '-(hood: ' num2str(h) ')'];
end

%opening
if filt==6
    trimp=imopen(trimp, se);
    tit= ['opn ' varargin{1} '-(hood: ' num2str(h) ')'];
end

%dilation
if filt==7
    trimp= imdilate(trimp, se);
    tit= ['dilate ' varargin{1} '-(hood: ' num2str(h) ')'];
end

%resize
if filt==8
    %start with scale, then go to (A, [numrows numcols])
    trimp=imresize(trimp, h);
    tit= ['resized-(' num2str(h) ')']
end
%both are very poor
%     HAve= fspecial('prewitt')
%     HAve= fspecial('sobel')

title= [title tit];
end

