%applies filters corresponding to cell of specs to **RAW SPEC**
%the form is:
%     {filt, h, sig}
%so [tp, tit]= processpic_outer(spec, {10,3}, {10,3}, {})

%filter         neighborhood(either 1d or 2d)       extraspec(sigma or shape)

%specNum==0 turns off display, good for batch testing/validation
function[trimp, tit]=processpic_outer(raw_spec, specNum, varargin)
newH= size(raw_spec, 1);
%reduce to correct size
% trimp=p(1:208, :); %for original, 512 DFT
newH= newH*.81; %from inspection
trimp=raw_spec(1:newH, :);

tit='';

    for filt= 1:size(varargin,2)
        filtSpec=varargin{filt};
        [trimp, tit]= processpic_ingredients(trimp, tit, filtSpec{:});
    end

%trim off leading thre characters of ' + '
tit=tit(5:end);
tit=[num2str(specNum) '-' tit];

%make all positive, consider changing location
oldmin= min(min(trimp));%imshow(trimp, [], 'XData', [1 256], 'YData', [1 175])
if oldmin<0
    trimp= trimp-oldmin;
end

%      figure
if specNum~=0
    imagesc(trimp);
    title(tit)
end
end
