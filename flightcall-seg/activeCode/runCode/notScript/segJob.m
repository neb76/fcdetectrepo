%wrapper class designed for tuning segmentation parameters

%TODO: all calls to sj.filtset currently needs a colon expansion. Bad design, fix this
classdef segJob
    %UNTITLED4 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        ID; %string to describe settings
        recordingStr= {'DANBY', '20090912', '194900'};
        fftWinSize= 1024; %moreF or fewerF is 1024 and 512
        clustK=6;
        evNum=6;
        filtSet={{1,[8 2]}, {3, [8 2]}}; %this default is med- case 100 used for situation of passing in processed pic
        sig=.025;
        affinityType=3; %default is global sigma, as local not yet working right
        neighSize=8; %default is the no transorm
        normCase=1;  %1 is RW, the preferred
        evType= 'sm'; %if 'reg', its as if no arguement
        kmeansBuff=0;  % buffer for kmeans initialization, 0 is standard 
        kmeansReps=50;
        sharp=0;
        postDilation= [8 2];
        whenDilation=2;  %0 is off, 1 is beofre merge, 2 is after merge, 3 is both
        
        specTime=.4; %these bottom two setting must be modified according to the choice of spetrogram matrix
        specSize= 53; 
        sym_check=1;

    end
    
    
    %########################constructor####################
    %########################be sure this matches experiment
    methods       
        function sj=segJob(ID)
            sj.ID= ID;
        end
    end 
            
    methods       
        function obj= set.fftWinSize(obj, winSz)
            if ~(winSz~=512 ||...
                    winSz~=1024)
                error('fft window size must be power of 2' )
            end
            obj.fftWinSize = winSz;
        end 
        
        %not really used
        function [spec, fRange]= getJobSpecG(sj, start, fin)
            recInfo= sj.recordingStr;
            [spec,fRange,~]= getspecBatch(sj.fftWinSize, recInfo{:}, start, fin);
        end
        
        %main segmentation code re
        function [seg, U, sym_time]= segmentThis(sj, spec)
            if isnumeric(sj.filtSet)
                if sj.filtSet==100   %the 100 is a setting for debugging, if you have supplied a pre-segmented spec
                    pp=spec;
                end
            else
                [pp,~]= processpic_outer( spec, 0, sj.filtSet{:} ); %***perhaps move process pic up to get spec(depends on what I do for create mask)
            end
            % calculate affinity and use to build normalized Laplacian matrix
            affinity = newAff_funcnl(pp, sj.affinityType, sj.sig, sj.neighSize);
            if sj.sym_check
                affinity=triu(affinity);
                affinity=affinity'+affinity;
            end
            tic
            dimF= size(pp, 1);
            dimT= size(pp, 2);
            laplacian= getLap(affinity, sj.normCase);
            % perform kmeans clustering on the matrix U, norm-1 matrix of the k
            % eigenvectors corresponding to the k smallest eigenvalues
            if sj.normCase==3
                [U,~] = eigs(laplacian, sj.evNum);
            else
                [U,~] = eigs(laplacian, sj.evNum, 'sm');
            end            
            U= normr(U);          
            if sj.kmeansBuff==0
                [IDX,~] = kmeans(U,sj.clustK, 'replicates', sj.kmeansReps);
            
            %this is for trying clustering seeds close to the middle to try
            %and reduce strange edge issues
            else
                startMat= ones(sj.clustK, sj.evNum, sj.kmeansReps);                
                nU= size(U,1);
                cBuf= 3; %column buffer, how many in
                pool= nU-2*cBuf*dimF;                
                %make a 3-D matrix for starting seeds, as we want to keep
                %all starts away from edge              
                for r= 1:sj.kmeansReps                  
                    startI= randsample(pool, sj.clustK);  %get three numbers within range
                    startI= startI+cBuf*dimF; %scale so they match linear index on image
                    startMat(:,:,r)=U(startI, :);  %place into statmat
                end
                [IDX,~]= kmeans(U,sj.clustK,'start',startMat, 'replicates', sj.kmeansReps);
            end
            seg= reshape(IDX, dimF, dimT);
            sym_time=toc;
        end
        
        function[seed]= getSeedCell(sj)
%             seed= {sj.ID, sj.sig, sj.affinityType,  sj.normCase, sj.filtSet, sj.neighSize, sj.clustK};
              seed= struct(sj);
        end
        
        function [topReg, regId]= getLoudSeg(sj, spec, segged)
            % ideally, segments of interest are loudest in spec
            % this gives us basis to check for unusual situations
            if isnumeric(sj.filtSet)
                if sj.filtSet==100
                    trimmed_spec=spec;
                end
            else
                trimmed_spec= processpic_outer( spec, 0, sj.filtSet{:} );
            end
            negShift=min(min(trimmed_spec));
            trimmed_spec= trimmed_spec - negShift; % shift to all positive values            
            topReg=find(segged==1);
            topAveAmp=mean(trimmed_spec(topReg));
            regId=1;
            for i= 2:sj.clustK
                curReg= find(segged==i);
                aveAmp= mean(trimmed_spec(curReg));
                if (aveAmp>topAveAmp)
                    topReg= curReg;
                    topAveAmp= aveAmp;
                    regId= i;
                end
            end            
        end
        
        %This function scales frequency ranges to spectrogram bin sizes,
        %reporting out a range of frequencies
        %spectrograms and matrices are indexed from opp directions, so high frequency means low index...
        function [mask, mRange]= getJobMask(sj, spec, fLow, fHigh)
            blockSize= 12000/((sj.fftWinSize+1)/2);
            lowI= 512-round(fLow/blockSize);
            highI= 512-round(fHigh/blockSize);
            %case of an already processed picture getting passed in
            if isnumeric(sj.filtSet)
                if sj.filtSet==100
                    maxHeight=size(spec,1);
                end
            else
                maxHeight= size(spec,1)*.81;
            end            
            if (lowI>maxHeight);
                lowI=maxHeight;
            end            
            mRange=(highI):(lowI);
            mask= spec(mRange,:);
        end
                
        function [topPcnt, topReg, regId]= boxROI(sj, segged, mrange, start, stop)
            topReg= 0;
            regId= 0;
            topPcnt= 0;
            topTot=0; %this added to give ties to the bigger segment
            sizePic= size(segged);           
            %get indeces of time dimentions for box (left/right edges of box)
            segRange=stop-start;
            shift= (.5*segRange)*sj.specSize/sj.specTime; %amount of time 'blocks' in segment that equal the shift from center to start and stop
            shift= ceil(shift);
            
            midI= floor(sj.specSize/2);
            startI= midI-shift;
            finI= midI + shift;            
            for i= 1:sj.clustK
                curReg= find(segged==i);  %get region
                if isempty(curReg)
                    continue
                end
                
                [iFreq, iTime]= ind2sub(sizePic, curReg);
                segTot= size(iFreq, 1);
                %check if box length is longer than seg, failure to catch will causes index out of bounds array
                boxHigh= min(iFreq); %upper bound of frew, so lowest freq scene.
                boxLow= max(iFreq);
                if mrange(1)>boxHigh
                    boxHigh= mrange(1);
                end
                if mrange(end)<boxLow
                    boxLow= mrange(end);
                end                
                tupI= horzcat(iFreq, iTime); %now that appropriate box is set, get a tuple of indeces for filtering
                %reduce list of tuples to values below the specified box boundry
                inBox= tupI(tupI(:,1) >= boxHigh, :); %greater than low I, which is highFreq?
                inBox= inBox(inBox(:,1)<= boxLow, :); %lower than high I, which is low freq?
                
                inBox= inBox(inBox(:,2)>= startI, :);
                inBox= inBox(inBox(:,2)<= finI, :);
                
                nInBox= size(inBox,1);
                pctIn= nInBox/segTot;
                if ( ((pctIn==topPcnt)&&(segTot>topTot)) || (pctIn>topPcnt) )   %modified to give ties to bigger segment
                    topReg= curReg;
                    topPcnt= pctIn;
                    topTot= segTot;
                    regId= i;                    
                end
            end                      
        end
        
              %use dil==0 if you don't want any dilation of the regions
             function [relabeled]=postprocess(sj, segged_o)
                  relabeled= segged_o;
                  if sj.postDilation~= 0
                      se= strel('rectangle', sj.postDilation);
                      if sj.whenDilation== 1 || sj.whenDilation==3
                          relabeled=imdilate(relabeled, se);
                      end
                  end
                  
                  sizemat=ones(sj.clustK,1);
                  for i= 1:sj.clustK
                      eval(sprintf('reg%d=segged_o(segged_o==i) ;',i));  %want each as vectores of indeces of the points'
                      eval(sprintf('sizemat(i)= size(reg%d, 1) ;', i));  %check index, may not be one
                  end
                  
                  maxI= find(sizemat==max(sizemat));  %index of biggest   
  

                  relabeled= (relabeled==maxI); %make a binary pick using max I, then reverse it
                  relabeled= ~relabeled;
                  relabeled= bwlabel(relabeled);       
                  %grow the region... (the reason for dilating here, after
                  %joining regions, is to prevent the case where dilations
                  %join regions.  However, will this allow us to joine both
                        %bands in the double banded signla?
                  if sj.postDilation~= 0
                      if sj.whenDilation== 2 || sj.whenDilation==3
                          relabeled=imdilate(relabeled, se);
                      end  
                  end                  
              end
       
        %brought inside function because pre-filtered caused exception. not
        %relevent when you pass in a pre-processed image
        function [icc]=findICC_classFunc(sj, orig, seg)
            %used to evaluate clustering.  Takes in the segmentation and the original
            %specgrogram, returns the inter vs. intracluster variance            
            if isnumeric(sj.filtSet)
                if sj.filtSet==100
                    pspec=orig;
                end
            else
                pspec= processpic(orig, 1, 0); %unfiltered
            end            
            dimF= size(pspec, 1);
            dimT= size(pspec, 2);
            allpoints= reshape(pspec, dimF*dimT, 1);
            btv= var(allpoints); %variance for all pic           
            varVec= 1:sj.clustK;
            for i= 1:sj.clustK
                clustIs= seg==i;
                clustPoints= pspec(clustIs);
                varVec(i)= var(clustPoints);
            end           
            icc= btv^2/((sum(varVec))^2 + btv^2);
        end
        
        
        function sj= adjustK(sj, kNew)
            sj.evNum= kNew;
            sj.clustK=kNew;
        end
        
        %use case 1 for boxing raw specs
        function[boxedSpec]=boxTheSpec(sj, spec, segMatRef, raw)
            %hard coded values that need customization if settings shift:
           %right now hardCoded, due to size of specs generated           
            if raw== 1
                boxedSpec=processpic(spec,1,0);
                boxColor= max(max(boxedSpec));
                maxHeight= floor(size(spec,1)*.81);
            else
                boxedSpec=spec;
                boxColor=sj.clustK+1;
                maxHeight=size(spec,1);
            end
            
            %time dimensions
            start=segMatRef{1,2};
            stop=segMatRef{1,3};
            segRange=stop-start;
            shift= (.5*segRange)*sj.specSize/sj.specTime; %amount of time 'blocks' in segment that equal the shift from center to start and stop
            shift= ceil(shift);
            midI= floor(sj.specSize/2)
            startI= midI-shift
            finI= midI + shift
            
            %freqDimensions
            blockSize= 12000/((1024+1)/2);
            flowI= 512-round(segMatRef{1,4}/blockSize);
            fhighI= 512-round(segMatRef{1,5}/blockSize);
            
            if (flowI>maxHeight);
                flowI=maxHeight;
            end
                        
            %make stripes
            boxedSpec(fhighI:flowI, startI)=boxColor; %left
            boxedSpec(fhighI:flowI, finI)=boxColor; %right
            boxedSpec(fhighI, startI:finI)=boxColor; %top
            boxedSpec(flowI, startI:finI)=boxColor; %bottom                        
        end
        
        
        %2nd arg-- give number if you have it, which will be in dispaly
        function seeFilt(sj, spec, varargin)
            figure  %comment out if need be
            switch nargin
                case 3
                    processpic_outer(spec, varargin{1}, sj.filtSet{:});
                case 2
                    processpic_outer(spec, 1, sj.filtSet{:});
            end         
        end
              
    end    
end

