%clipping off high pass-filtered out junk inside, for symetry with spectral

function [ segImg ] = simpleSegment( img )
%simpleSegment Segmentation based on thresholding and conn. comp.
%

img=processpic_for_simpleseg(img, 1, 0); %


%Set the thresholding values


th_pixel_value = -75;
th_pixel_number = 30;  %Minimum number of pixels in a cluster

%Do the pixel value thresholding
th_img = img > -80;

%Do the pixel number thresholding
segImg = false(size(img)); %Create empty binary image
cc = bwconncomp(th_img);   %Find the connected components

for i = 1:cc.NumObjects
    if length(cc.PixelIdxList{i}) > th_pixel_number
        segImg(cc.PixelIdxList{i}) = true;
    end
end

segImg = bwlabel(segImg);


end

