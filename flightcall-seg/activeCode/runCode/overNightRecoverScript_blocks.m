%overnight script
%run validate
k=2;
%%
sched= findResource('scheduler', 'configuration', 'atlasscheduler' );
clustJ1= createJob(sched);
clustJ2= createJob(sched);
set(clustJ1,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})
set(clustJ2,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})

highSig= .05;
lowSig= .01;
step= .005;
sigInfo= {highSig, lowSig, step};
specN= 633;

jobI= 1;
for trans= [1 3 6]
    for norm= [1 2]
        argCell= makeArgCell(sigInfo{:}, trans, norm, k);
        if (jobI<4)
            createTask(clustJ1, @computeDistrJob_ioVal, 6, argCell )  ;
        else
            createTask(clustJ2, @computeDistrJob_ioVal, 6, argCell )  ;            
        end       
        jobI= jobI + 1;
    end
end
    
submit(clustJ1);
wait(clustJ1);
jobOut1= clustJ1.getAllOutputArguments;



submit(clustJ2); %submitted at 12:28
jobOut2= clustJ2.gea


%%
clear all
k=3;
%notes:
%    likely to bejobs 35 and 36, submitting the first at 3:30, 7/24
%%
sched= findResource('scheduler', 'configuration', 'atlasscheduler' );
clustJ1= createJob(sched);
clustJ2= createJob(sched);
set(clustJ1,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})
set(clustJ2,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})

highSig= .05;
lowSig= .01;
step= .005;
sigInfo= {highSig, lowSig, step};
specN= 633;

jobI= 1;
for trans= [1 3 6]
    for norm= [1 2]
        argCell= makeArgCell(sigInfo{:}, trans, norm, k);
        if (jobI<4)
            createTask(clustJ1, @computeDistrJob_ioVal, 6, argCell )  ;
        else
            createTask(clustJ2, @computeDistrJob_ioVal, 6, argCell )  ;            
        end       
        jobI= jobI + 1;
    end
end
    
submit(clustJ1);  %3:26 7/24
wait(clustJ1);
jobOut1= clustJ1.getAllOutputArguments;



submit(clustJ2); %submitted at 12:28
jobOut2= clustJ2.gea


%%
clear all

k=4;
%%
sched= findResource('scheduler', 'configuration', 'atlasscheduler' );
clustJ1= createJob(sched);
clustJ2= createJob(sched);
set(clustJ1,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})
set(clustJ2,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})

highSig= .05;
lowSig= .01;
step= .005;
sigInfo= {highSig, lowSig, step};
specN= 633;

jobI= 1;
for trans= [1 3 6]
    for norm= [1 2]
        argCell= makeArgCell(sigInfo{:}, trans, norm, k);
        if (jobI<4)
            createTask(clustJ1, @computeDistrJob_ioVal, 6, argCell )  ;
        else
            createTask(clustJ2, @computeDistrJob_ioVal, 6, argCell )  ;            
        end       
        jobI= jobI + 1;
    end
end
    
submit(clustJ1);
wait(clustJ1);
jobOut1= clustJ1.getAllOutputArguments;



submit(clustJ2); %submitted at 12:28
jobOut2= clustJ2.gea


%%
clear all

k=5;
%%
sched= findResource('scheduler', 'configuration', 'atlasscheduler' );
clustJ1= createJob(sched);
clustJ2= createJob(sched);
set(clustJ1,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})
set(clustJ2,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})

highSig= .05;
lowSig= .01;
step= .005;
sigInfo= {highSig, lowSig, step};
specN= 633;

jobI= 1;
for trans= [1 3 6]
    for norm= [1 2]
        argCell= makeArgCell(sigInfo{:}, trans, norm, k);
        if (jobI<4)
            createTask(clustJ1, @computeDistrJob_ioVal, 6, argCell )  ;
        else
            createTask(clustJ2, @computeDistrJob_ioVal, 6, argCell )  ;            
        end       
        jobI= jobI + 1;
    end
end
    
submit(clustJ1);
wait(clustJ1);
jobOut1= clustJ1.getAllOutputArguments;



submit(clustJ2); %submitted at 12:28
jobOut2= clustJ2.gea


dirTemp= ['clusterRuns' filesep];
saveN= [dirTemp num2str(k) 'clust'];
save(saveN, 'jobOut');
