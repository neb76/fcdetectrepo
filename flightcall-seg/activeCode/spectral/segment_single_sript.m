%4664.826, 4664.941;-- entry 49

% Ng, A., Jordan, M., and Weiss, Y. (2002). On spectral clustering: analysis and an algorithm. In T. Dietterich,
% S. Becker, and Z. Ghahramani (Eds.), Advances in Neural Information Processing Systems 14 
% (pp. 849 � 856). MIT Press.

% CONCEPT: Introduced the normalization process of affinity matrix(D-1/2 A D-1/2), 
% eigenvectors orthonormal conversion and clustering by kmeans 

%affTech:
%     -3, global sigma
%     -4, self tuned, local sigma.  Sigma param is now irrelevent
%     

% function [lab_pic]= segment(sigma, start, stop, filter, affTech)
clear all
close all
start= 16224.627;
stop=  16224.715;
sigma= .035 ;
affTech= 3;
filter= 4;
neighSize=99;
evNum = 3;
clustNum=3 ;
thresh= .00000001;

%get spec, then filter and trim
[p_new,f,t]= getspec('DANBY','20090912','194900',start,stop);

data= processpic( p_new, filter, 1 );
figure

% calculate affinity matrix
affinity = newAff_funcnl(data, 6, sigma, neighSize);
dimF= size(data, 1);
dimT= size(data, 2);
NL1= getLap(affinity, 1);

[U,eVals] = eigs(NL1, evNum, 'sm');  %'SM' is what we want, but 'SR' gives the surprise nice answers
% %riff:: trying thresholding
% u_lab= U(:,1);
% u_lab= abs(u_lab);
% u_lab(u_lab<=thresh)= 0;
% u_lab(u_lab>thresh)= 1;
% lab_pic= reshape(u_lab, dimF, dimT);
% figure
% imagesc(lab_pic)

% perform the eigen value decomposition on eigen vectors correspondin to k
% smallest eig-values
U_n= normr(U);
% U_n= abs(U_n);
[IDX,C] = kmeans(U_n,clustNum, 'replicates', 50); 
lab_pic= reshape(IDX, dimF, dimT);
figure
imagesc(data)

figure
imagesc(lab_pic)
% end

