function [ Q ] = getQ( aff, evNum, caseN )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
if caseN==1
sumvec = sum(aff,2);
sqsumvec = sqrt(sumvec);
lap= bsxfun(@rdivide,aff, sqsumvec);
lap= bsxfun(@rdivide,lap, sqsumvec');
% [V, ~]= eigs(aff);
% V= V(:, evNum);
end
if caseN==2
    lap= getLap(aff); 
end

pr = 'eig decomposing... is C sparse?'
issparse(aff)

[V, ~]= eigs(lap,evNum);
V= normr(V);
Q= mtimesx(V,V') ;
Q= sparse(bsxfun(@gt, Q, .9));

end

