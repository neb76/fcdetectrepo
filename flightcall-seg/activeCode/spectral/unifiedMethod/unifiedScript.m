 %unified script
%     get a normalized laplacian
%     create a new affinity using k eigen vectors
%     cluster on this mat
   
    
clear all;
close all;
start= 7020.449;
stop= 7020.580;
% generate the data
[p_new,f,t]= getspec('DANBY','20090912','194900',start,stop);
data= processpic( p_new, 3, 1 );

 
% calculate affinity matrix
affinity = newAff_vec(data, 3, .05);
dimF= size(data, 1)-2;
dimT= size(data, 2)-2;

%NL1= getLap(affinity);
% NL1= affinity;

% compute the normalized laplacian (method 2)  eye command is used to
% obtain the identity matrix of size m x n
% NL2 = eye(size(affinity,1),size(affinity,2)) - (D^(-1/2) .* affinity .* D^(-1/2));

% perform the eigen value decomposition on k largest eigen vectors


Q= getQ(affinity, 4, 2);

% Q(Q<.9)= 0;

%get all points above thresh
%for all points, get the connected points (safe_length*8)
