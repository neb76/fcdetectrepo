%first run at 11:16, randomly sampled 32 from top candidate,
%         sig= .025, cubic transform, sym normalization
%initial concern, is that is might be oversegmenting



sched= findResource('scheduler', 'configuration', 'atlasscheduler' );
rand32J= createJob(sched);
set(rand32J,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})


jobSpecs= {'',.025, 6, 2, 3};

num= 32;
smplI= randperm(size(specMat, 1));
smplI=smplI(1:num); %###important for indexing! be sure to keep around!


%randomly sampled 60 specs
smallSpec= specMat(smplI, :);

for i= 1:size(smallSpec,1)
    spec= smallSpec{i,1};
    specI=smplI(i);
    stats=segMatRef(specI,:);
    createTask(rand32J, @computeDistrJob_render, 6, {jobSpecs{:}, spec, stats} );
end


submit(rand32J)
wait(rand32J)

jobOut=rand32J.getAllOutputArguments;


%with outputs, plot a subplot with some nice stats
for s= 1:size(jobOut, 1)
    specI=smplI(s);
    curSeg= jobOut{s,1};
    segRow=segMatRef(specI,:);
    boxedSeg= boxTheSpec(curSeg,segRow,2);

    
    spec=specMat{specI,1};
    boxedSpec=boxTheSpec(spec, segRow,1);
    figure;

    a=subplot(1,2,1), imagesc(boxedSpec);  %subplot, with the left photo being non filtered trim
    colorbar;
    %box: # to # seconds, frequencies  
    boxStr= [segMatRef{specI,6} '  freq range: ' num2str(segMatRef{specI,4}) ' hz to ' num2str(segMatRef{specI,5}) ' hz'];
    text(1,450,boxStr);
    
    b=subplot(1,2,2), imagesc(boxedSeg);
    colorbar;
    %title- the %numth spec at %time and boxSeg is %boxInd
    time=floor(segMatRef{specI,2});
    tit=['the ' num2str(specI) 'th seg at time ' num2str(time) ' secs, box index is ' num2str(jobOut{s,2})];
    title(a,tit); 
    
    %score info
    scoreStr=['top %: ' num2str(jobOut{s,3}) '     ICC is: ' num2str(jobOut{s,4})];
    text(1,450,scoreStr);    
end