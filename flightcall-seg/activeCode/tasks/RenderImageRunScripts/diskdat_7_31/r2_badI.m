%second run at 11:33, randomly sampled 32 the persistently bad spectrograms I's,
%         sig= .025, cubic transform, sym normalization
%initial concern, is that i might be oversegmenting



sched= findResource('scheduler', 'configuration', 'atlasscheduler' );
badSpecJ= createJob(sched);
set(badSpecJ,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})


jobSpecs= {'',.025, 6, 2, 2};

num= 32;
smplI= randperm(size(badIs, 1));
smplI= badIs(smplI);
smplI=smplI(1:num); %###important for indexing! be sure to keep around!


%randomly sampled 60 specs
smallSpec= specMat(smplI, :);

for i= 1:size(smallSpec,1)
    spec= smallSpec{i,1};
    specI=smplI(i);
    stats=segMatRef(specI,:);
    createTask(badSpecJ, @computeDistrJob_render, 6, {jobSpecs{:}, spec, stats} );
end


submit(badSpecJ)
wait(badSpecJ)

jobOut=badSpecJ.getAllOutputArguments;
% 
% %     surf(t,f,spec,'EdgeColor','none');
% %     axis xy; axis tight; colormap(jet); view(0,90);
% %     xlabel('Time');
% %     ylabel('Frequency (Hz)');
% 
% [~,f,t] = getspecBatch(1024, 'DANBY','20090912','194900', 1259.4, 1259.8);
% 
% labels=f(99:end);
% 
% imagesc(processpic(spec,1,1));
% set(gca, 'YTick', [1:100:415]);
% set(gca, 'YTickLabels', labels);

%with outputs, plot a subplot with some nice stats
for s= 1:size(jobOut, 1)
    specI=smplI(s);
    curSeg= jobOut{s,1};
    segRow=segMatRef(specI,:);
    boxedSeg= boxTheSpec(curSeg,segRow,2);

    
    spec=specMat{specI,1};
    boxedSpec=boxTheSpec(spec, segRow,1);
    figure;

 %plot original
    a=subplot(1,2,1), imagesc(boxedSpec);  %subplot, with the left photo being non filtered trim
    colorbar;
    
    
    %box: # to # seconds, frequencies  
    boxStr= [segMatRef{specI,6} '  freq range: ' num2str(segMatRef{specI,4}) ' hz to ' num2str(segMatRef{specI,5}) ' hz'];
    text(1,450,boxStr);
    
    b=subplot(1,2,2), imagesc(boxedSeg);
    colorbar;
    %title- the %numth spec at %time and boxSeg is %boxInd
    time=floor(segMatRef{specI,2});
    tit=['the ' num2str(specI) 'th seg at time ' num2str(time) ' secs, box index is ' num2str(jobOut{s,2})];
    title(a,tit); 
    
    %score info
    scoreStr=['top %: ' num2str(jobOut{s,3}) '     ICC is: ' num2str(jobOut{s,4})];
    text(1,450,scoreStr);    
end


h = get(0,'children');
for i=1:length(h)
saveas(h(i), ['figure' num2str(i)], 'jpg');
end