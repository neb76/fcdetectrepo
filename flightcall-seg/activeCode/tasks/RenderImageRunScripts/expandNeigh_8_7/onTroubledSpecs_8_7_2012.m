%8.7.2012
%script to look at 3 different aff techniques-
%     -old with bug
%     -old with bug fixed
%     -new with expanded neighborhoods
% major q's
%     1) can i fix the edge bias?
%     2) can i grow the regions

    
    
%%import data!!!
sched2= findResource();
trynewAff= createJob(sched2);
set(trynewAff,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})

jobSpecs= {.035, 6, 1, 4}; %new med-ave filter

iSpecs= [2 13 299 474 600];
smallSpec= specMat(iSpecs, :);

for n= [8 12 99]
    for i=1:size(smallSpec, 1)
        spec= smallSpec{i,1};
        specI= iSpecs(i);
        stats= segMatRef(specI,:);
        createTask(trynewAff, @computeDistrJob_render,6, {jobSpecs{:}, n, spec, stats});
    end
end

submit(trynewAff)
wait(trynewAff)
jobOut=  trynewAff.getAllOutputArguments

outerName='troubleSpecs';
mkdir(outerName)
for s=iSpecs
    
end


% for 
    
for s= 1:size(jobOut, 1)
    seed= jobOut{s, 5};
    specI= seed{1,1};
    curSeg= jobOut{s,1};
    segRow=segMatRef(specI,:);
    boxedSeg= boxTheSpec(curSeg,segRow,2);

    
    spec=specMat{specI,1};
    boxedSpec=boxTheSpec(spec, segRow,1);
    figure; 

    a=subplot(1,3,1); imagesc(boxedSpec);  %subplot, with the left photo being non filtered trim
    
    %box: # to # seconds, frequencies  
    text(1,450,segMatRef{specI,6}); %add species
    belowStr= [num2str(segMatRef{specI,4}) ' to ' num2str(segMatRef{specI,5}) ' hz'];
    text(1, 462, belowStr);
    
    b=subplot(1,3,2); imagesc(boxedSeg);
    
    %title- the %numth spec at %time and boxSeg is %boxInd
    time=floor(segMatRef{specI,2});
    tit=['the ' num2str(specI) 'th seg at time ' num2str(time) ' secs, box index is ' num2str(jobOut{s,2})];
    title(a,tit); 
    
    %score info
    scoreStr=['top %: ' num2str(jobOut{s,3}) '     ICC is: ' num2str(jobOut{s,4})];
    text(1,450,scoreStr);
    
    
    c=subplot(1,3,3); imagesc(processpic(spec, jobSpecs{1,4}, 0));
    title('median[8 2] + average[8 2] filter')
    
    neigh= seed{1,7};
    
%     imwrite(gcf, [svStr filesep 'spec_' num2str(time)], 'GIF', 'ScreenSize', [1920 1080]);
%     close all;
end