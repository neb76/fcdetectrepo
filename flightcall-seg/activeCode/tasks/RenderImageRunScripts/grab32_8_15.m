%first run at 11:16, randomly sampled 32 from top candidate,
%         sig= .025, cubic transform, sym normalization
%initial concern, is that is might be oversegmenting


%*****import data!
sched= findResource('scheduler', 'configuration', 'atlasscheduler' );
getTheoSlides_dil= createJob(sched);
set(getTheoSlides_dil,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})


% jobSpecs= {.035, 6, 1, 4};

num= 32;
smplI= randperm(size(specMat, 1));
smplI=smplI(1:num); %###important for indexing! be sure to keep around!


%randomly sampled 60 specs
smallSpec= specMat(smplI, :);

for i= 1:size(smallSpec,1)
    spec= smallSpec{i,1};
    specI=smplI(i);
    stats=segMatRef(specI,:);
    createTask(getTheoSlides_dil, @computeDistrJob_render, 7, {'theoslides', 0, spec, stats} );
end


submit(getTheoSlides_dil)
wait(getTheoSlides_dil)

jobOut=getTheoSlides_dil.getAllOutputArguments;


%%
% a couple selects
noise_mises2= createJob(sched);
set(noise_mises2,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})

jobI=[3 393 609];
smallSpec= specMat(jobI, :);


for i= 1:size(smallSpec,1)
figure, imagesc(smallSpec{i,1});
end


for i= 1:size(smallSpec,1)
    specI=jobI(i);
    stats=segMatRef(specI,:);
    createTask(noise_mises2, @computeDistrJob_render, 7, {'theoslides', 0, smallSpec{i,1}, stats} );
end

submit(noise_mises2)
wait(noise_mises2)
jobOut=noise_mises2.getAllOutputArguments;
% % svStr='C:\Users\neb76\Desktop\figs8_3\rand32_2_widest';
% % mkdir(svStr)
%with outputs, plot a subplot with some nice stats
for s= 1:size(jobOut, 1)
    specI=smplI(s);
    curSeg= jobOut{s,1};
    segRow=segMatRef(specI,:);
    boxedSeg= boxTheSpec(curSeg,segRow,2);

    
    spec=specMat{specI,1};
    boxedSpec=boxTheSpec(spec, segRow,1);
    figure;
    set(gca, 'LooseInset', [0,0,0,0]);

    a=subplot(1,3,1); imagesc(boxedSpec);  %subplot, with the left photo being non filtered trim
    
    %box: # to # seconds, frequencies  
    text(1,450,segMatRef{specI,6}); %add species
    belowStr= [num2str(segMatRef{specI,4}) ' to ' num2str(segMatRef{specI,5}) ' hz'];  %add freq range
    text(1, 462, belowStr);
    
    b=subplot(1,3,2); imagesc(boxedSeg);
    
    %title- the %numth spec at %time and boxSeg is %boxInd
    time=floor(segMatRef{specI,2});
    tit=['the ' num2str(specI) 'th seg at time ' num2str(time) ' secs, box index is ' num2str(jobOut{s,2})];
    title(a,tit); 
    
    %score info
    scoreStr=['top %: ' num2str(jobOut{s,3}) '     ICC is: ' num2str(jobOut{s,4})];
    text(1,450,scoreStr);
    
    
    c=subplot(1,3,3); imagesc(processpic_outer(spec, specI, {1,[8 2]},{7, [8 2], 'rectangle'}, {3, [8 2]}));
    title('median[8 2] + average[8 2] filter')
    
%     imwrite(gcf, [svStr filesep 'spec_' num2str(time)], 'GIF', 'ScreenSize', [1920 1080]);
%     close all;
end




%for the select little guy


for s= 1:size(jobOut, 1)
    specI=jobI(s);
    curSeg= jobOut{s,1};
    segRow=segMatRef(specI,:);
    boxedSeg= boxTheSpec(curSeg,segRow,2);

    
    spec=specMat{specI,1};
    boxedSpec=boxTheSpec(spec, segRow,1);
    figure;
    set(gca, 'LooseInset', [0,0,0,0]);

    a=subplot(1,3,1); imagesc(boxedSpec);  %subplot, with the left photo being non filtered trim
    
    %box: # to # seconds, frequencies  
    text(1,450,segMatRef{specI,6}); %add species
    belowStr= [num2str(segMatRef{specI,4}) ' to ' num2str(segMatRef{specI,5}) ' hz'];  %add freq range
    text(1, 462, belowStr);
    
    b=subplot(1,3,2); imagesc(boxedSeg);
    
    %title- the %numth spec at %time and boxSeg is %boxInd
    time=floor(segMatRef{specI,2});
    tit=['the ' num2str(specI) 'th seg at time ' num2str(time) ' secs, box index is ' num2str(jobOut{s,2})];
    title(a,tit); 
    
    %score info
    scoreStr=['top %: ' num2str(jobOut{s,3}) '     ICC is: ' num2str(jobOut{s,4})];
    text(1,450,scoreStr);
    
    
    c=subplot(1,3,3); imagesc(processpic_outer(spec, specI, {1,[8 2]},{7, [8 2], 'rectangle'}, {3, [8 2]}));
    title('median[8 2] + average[8 2] filter')
    
%     imwrite(gcf, [svStr filesep 'spec_' num2str(time)], 'GIF', 'ScreenSize', [1920 1080]);
%     close all;
end