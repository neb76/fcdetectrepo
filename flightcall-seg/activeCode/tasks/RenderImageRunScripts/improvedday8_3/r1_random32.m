%first run at 11:16, randomly sampled 32 from top candidate,
%         sig= .025, cubic transform, sym normalization
%initial concern, is that is might be oversegmenting


%*****import data!
sched= findResource('scheduler', 'configuration', 'atlasscheduler' );
andrewRender2= createJob(sched);
set(andrewRender,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})


jobSpecs= {.035, 6, 1, 4};

num= 32;
smplI= randperm(size(specMat, 1));
smplI=smplI(1:num); %###important for indexing! be sure to keep around!


%randomly sampled 60 specs
smallSpec= specMat(smplI, :);

for i= 1:size(smallSpec,1)
    spec= smallSpec{i,1};
    specI=smplI(i);
    stats=segMatRef(specI,:);
    createTask(andrewRender, @computeDistrJob_render, 6, {jobSpecs{:}, 8, spec, stats} );
end


submit(andrewRender)
wait(andrewRender)

jobOut=andrewRender.getAllOutputArguments;

% % svStr='C:\Users\neb76\Desktop\figs8_3\rand32_2_widest';
% % mkdir(svStr)
%with outputs, plot a subplot with some nice stats
for s= 1:size(jobOut, 1)
    specI=smplI(s);
    curSeg= jobOut{s,1};
    segRow=segMatRef(specI,:);
    boxedSeg= boxTheSpec(curSeg,segRow,2);

    
    spec=specMat{specI,1};
    boxedSpec=boxTheSpec(spec, segRow,1);
    figure;
    set(gca, 'LooseInset', [0,0,0,0]);

    a=subplot(1,3,1); imagesc(boxedSpec);  %subplot, with the left photo being non filtered trim
    
    %box: # to # seconds, frequencies  
    text(1,450,segMatRef{specI,6}); %add species
    belowStr= [num2str(segMatRef{specI,4}) ' to ' num2str(segMatRef{specI,5}) ' hz'];
    text(1, 462, belowStr);
    
    b=subplot(1,3,2); imagesc(boxedSeg);
    
    %title- the %numth spec at %time and boxSeg is %boxInd
    time=floor(segMatRef{specI,2});
    tit=['the ' num2str(specI) 'th seg at time ' num2str(time) ' secs, box index is ' num2str(jobOut{s,2})];
    title(a,tit); 
    
    %score info
    scoreStr=['top %: ' num2str(jobOut{s,3}) '     ICC is: ' num2str(jobOut{s,4})];
    text(1,450,scoreStr);
    
    
    c=subplot(1,3,3); imagesc(processpic(spec, jobSpecs{1,4}, 0));
    title('median[8 2] + average[8 2] filter')
    
%     imwrite(gcf, [svStr filesep 'spec_' num2str(time)], 'GIF', 'ScreenSize', [1920 1080]);
%     close all;
end