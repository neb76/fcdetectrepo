%second run at 11:33, randomly sampled 32 the persistently bad spectrograms I's,
%         sig= .025, cubic transform, sym normalization
%initial concern, is that i might be oversegmenting



sched= findResource('scheduler', 'configuration', 'atlasscheduler' );
non1_32= createJob(sched);
set(non1_32,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'});
non1_end=createJob(sched);
set(non1_end,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'});
jobSpecs= {.035, 6, 1, 4};
jp= 4; %position of job during val Run

%get the badIs
%so few, just do all?

alljobs= J1Out{jp,2};
non1= find(alljobs<1);   %for job 4, this is only 

%randomly sampled 60 specs
smallSpec1= specMat(non1(1:32), :);
smallSpec2=specMat(non1(33:end),:);

for i= 1:size(smallSpec1,1)
    spec= smallSpec1{i,1};
    specI=non1(i);
    stats=segMatRef(specI,:);
    createTask(non1_32, @computeDistrJob_render, 6, {jobSpecs{:}, spec, stats} );
end


submit(non1_32)
wait(non1_32)

for i= 1:(size(smallSpec2,1))
    spec= smallSpec2{i,1};
    specI=non1(i+size(smallSpec1, 1));
    stats=segMatRef(specI,:);
    createTask(non1_end, @computeDistrJob_render, 6, {jobSpecs{:}, spec, stats} );
end

submit(non1_end)
wait(non1_end)

jobOut1=non1_32.getAllOutputArguments;
jobOut2= non1_end.getAllOutputArguments;
jobOut=vertcat(jobOut1, jobOut2);
% 
% %     surf(t,f,spec,'EdgeColor','none');
% %     axis xy; axis tight; colormap(jet); view(0,90);
% %     xlabel('Time');
% %     ylabel('Frequency (Hz)');
% 
% [~,f,t] = getspecBatch(1024, 'DANBY','20090912','194900', 1259.4, 1259.8);
% 
% labels=f(99:end);
% 
% imagesc(processpic(spec,1,1));
% set(gca, 'YTick', [1:100:415]);
% set(gca, 'YTickLabels', labels);

%with outputs, plot a subplot with some nice stats
%with outputs, plot a subplot with some nice stats
svStr='C:\Users\neb76\Desktop\figs8_3\non1_wide';
mkdir(svStr)
%with outputs, plot a subplot with some nice stats
for s= 1:size(jobOut, 1)
    specI=non1(s);
    curSeg= jobOut{s,1};
    segRow=segMatRef(specI,:);
    boxedSeg= boxTheSpec(curSeg,segRow,2);

    
    spec=specMat{specI,1};
    boxedSpec=boxTheSpec(spec, segRow,1);
    figure;

    a=subplot(1,3,1); imagesc(boxedSpec);  %subplot, with the left photo being non filtered trim

    %box: # to # seconds, frequencies  
    text(1,450,segMatRef{specI,6}); %add species
    belowStr= [num2str(segMatRef{specI,4}) ' to ' num2str(segMatRef{specI,5}) ' hz'];
    text(1, 462, belowStr);
    
    b=subplot(1,3,2); imagesc(boxedSeg);

    %title- the %numth spec at %time and boxSeg is %boxInd
    time=floor(segMatRef{specI,2});
    tit=['the ' num2str(specI) 'th seg at time ' num2str(time) ' secs, box index is ' num2str(jobOut{s,2})];
    title(a,tit); 
    
    %score info
    scoreStr=['top %: ' num2str(jobOut{s,3}) '     ICC is: ' num2str(jobOut{s,4})];
    text(1,450,scoreStr);
    
    
    c=subplot(1,3,3); imagesc(processpic(spec, jobSpecs{1,4}, 0));
    title('median[8 2] + average[8 2] filter')
    
    saveas(gcf, [svStr filesep 'spec_' num2str(time)], 'jpg');
    close all;
end