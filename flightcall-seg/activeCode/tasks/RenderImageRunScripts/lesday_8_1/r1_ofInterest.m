%grabbed from inspection of our look yesterday 
%         sig= .025, cubic transform, sym normalization



sched= findResource('scheduler', 'configuration', 'atlasscheduler' );
ofInterestJ3= createJob(sched);
set(ofInterestJ3,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})


jobSpecs= {'',.03, 6, 1, 3};


smplI=[108 268 458 562 70 608]; %###important for indexing! be sure to keep around!


%randomly sampled 60 specs
smallSpec= specMat(smplI, :);

for i= 1:size(smallSpec,1)
    spec= smallSpec{i,1};
    specI=smplI(i);
    stats=segMatRef(specI,:);
    createTask(ofInterestJ3, @computeDistrJob_render, 6, {jobSpecs{:}, spec, stats, 2} );
    createTask(ofInterestJ3, @computeDistrJob_render, 6, {jobSpecs{:}, spec, stats, 3} );
end


submit(ofInterestJ3) 
wait(ofInterestJ3)

jobOut=ofInterestJ3.getAllOutputArguments;


%with outputs, plot a subplot with some nice stats
jI=1
for s= 1:size(smallSpec, 1)
    specI=smplI(s);
    curSeg= jobOut{jI,1};
    jI=jI+1;
    segRow=segMatRef(specI,:);
    boxedSeg= boxTheSpec(curSeg,segRow,2);

    
    spec=specMat{specI,1};
    boxedSpec=boxTheSpec(spec, segRow,1);
    figure;

    a=subplot(1,3,1), imagesc(boxedSpec);  %subplot, with the left photo being non filtered trim
    colorbar;
    %box: # to # seconds, frequencies  
    boxStr= [segMatRef{specI,6} '  freq range: ' num2str(segMatRef{specI,4}) ' hz to ' num2str(segMatRef{specI,5}) ' hz'];
    text(1,450,boxStr);
    
    b=subplot(1,3,2), imagesc(boxedSeg);
    colorbar;
    %title- the %numth spec at %time and boxSeg is %boxInd
    time=floor(segMatRef{specI,2});
    tit=['the ' num2str(specI) 'th seg at time ' num2str(time) ' secs, box index is ' num2str(jobOut{s,2})];
    title(a,tit); 
    
    %score info
    scoreStr=['top %: ' num2str(jobOut{jI,3}) '     ICC is: ' num2str(jobOut{jI,4})];
    text(1,450,scoreStr);    
    
    overFilt= jobOut{jI,1};
    overBoxed= boxTheSpec(overFilt, segRow, 2);
    c=subplot(1,3,3), imagesc(overBoxed);
    colorbar
    rep= jobOut{jI,6};
    if strfind(rep, 'no match') ~=[]
        text(1,450,rep);
    end
    jI=jI+1;
    
end


get(14,'Title')

h = get(0,'children');
for i=1:length(h)
saveas(h(i), ['figure' num2str(i)], 'jpg');
end
