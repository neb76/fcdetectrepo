%debugging spectral
%settup cluster
sched= findResource('scheduler', 'configuration', 'atlasscheduler' );
clustJ= createJob(sched);
set(clustJ,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})
%%

start= 4664.826;
stop=  4664.941;
spec= getspecBatch(1024, 'DANBY', '20090912', '194900', start, stop);
figure, imagesc(processpic(spec, 3, 1));



for n= 1:3
    for t= 0:6
        createTask(clustJ, @computeDistrJob_clientSv, 3, {'', .05, t, n, spec});
    end
end

submit(clustJ);
wait(clustJ);
jobOut= clustJ.getAllOutputArguments;

clJob= jobOut(~misvec, :);

segI= 1;
for n= 1:3
    for t= 0:6
        curSeg= jobOut{segI, 1};
         if segI~= 17
            figure, imagesc(curSeg);
            seed= jobOut{segI, 2};
            figTit= ['transform_num: '   num2str(seed{3}) '  norm is: ' num2str(seed{4})];
            title(figTit);       
         end
         segI= segI+ 1;
    end
end

%0-6 for amp power
%1-3 norm case


