%last one had wrong abs value
%also, all 0 and 2 techniques failed, so i'm just skipping this time

start= 4664.826;
stop=  4664.941;
spec= getspecBatch(1024, 'DANBY', '20090912', '194900', start, stop);
% figure, imagesc(processpic(spec, 3, 1));



%%
%prep cluster
sched= findResource('scheduler', 'configuration', 'atlasscheduler' );
clustJ= createJob(sched);
set(clustJ,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})
%%
%load up jobs then execture
for n= 1:3
    for t= 1:6
        if t ~=2
            createTask(clustJ, @computeDistrJob_clientSv, 3, {'', .05, t, n, spec});
        end
    end
end

submit(clustJ);
wait(clustJ);
jobOut= clustJ.getAllOutputArguments;
%%
%%render in client
segI= 1;
for n= 1:3
    for t= 1:6
        curSeg= jobOut{segI, 1};
        if t~= 2
            figure, imagesc(curSeg);
            seed= jobOut{segI, 2};
            figTit= ['transform_num: '   num2str(seed{3}) '  norm is: ' num2str(seed{4})];
            title(figTit);
            
            segI= segI+ 1;
        end
    end
end
%%
%save pics, i like these
dNm= '7_14_pics'
rNm= '5_no_abs_correctEV_k5';
mkdir(dNm);
mkdir([dNm filesep rNm]);

for h= 1:15
    hand= h+2;
    seed= jobOut{h, 2};
    figTit= ['tran_'   num2str(seed{3}) '_norm_' num2str(seed{4})];
    saveas(hand, [dNm filesep rNm filesep figTit '.jpg'] );
end

hand= 10;
h= hand-3;
seed= jobOut{h, 2};
figTit= ['tran_'   num2str(seed{3}) '_norm_' num2str(seed{4})];
saveas(hand, [dNm filesep rNm filesep figTit '.jpg'] );


%0-6 for amp power
%1-3 norm case


