%last one had wrong abs value
%also, all 0 and 2 techniques failed, so i'm just skipping this time
len= 3
csv_name='C:\Users\neb76\Desktop\nick_FCDetect_beforGit\DANBY_20090912_194900_NFC_637.csv'
segMat= parseCSV(csv_name, len);

spec1= getspecBatch(1024, 'DANBY', '20090912', '194900', segMat{1, 2:3});
spec2= getspecBatch(1024, 'DANBY', '20090912', '194900', segMat{2, 2:3});
spec3= getspecBatch(1024, 'DANBY', '20090912', '194900', segMat{3, 2:3});




%%
%prep cluster
sched= findResource('scheduler', 'configuration', 'atlasscheduler' );
clustJ= createJob(sched);
set(clustJ,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})
%%
%load up jobs then execture
for n= [1 3 6]
    pr= n
      createTask(clustJ, @computeDistrJob_clientSv, 3, {'', .05, n, 3, spec1});
      createTask(clustJ, @computeDistrJob_clientSv, 3, {'', .05, n, 3, spec2});
      createTask(clustJ, @computeDistrJob_clientSv, 3, {'', .05, n, 3, spec3});    
end

submit(clustJ);
wait(clustJ);
jobOut= clustJ.getAllOutputArguments;
%%
%%render in client
segI= 1;
for n= 1:9
    curSeg= jobOut{n, 1};
    figure, imagesc(curSeg);
    seed= jobOut{n, 2};
    figTit= ['transform_num: '   num2str(seed{3}) '  norm is: ' num2str(seed{4})];
    title(figTit);
end

%some comparisons
figure, imagesc(processpic(spec1,1,1))
figure,imagesc(processpic(spec1,3,1))
figure,imagesc(processpic(spec2,1,1))
figure,imagesc(processpic(spec2,3,1))
figure,imagesc(processpic(spec3,1,1))
figure,imagesc(processpic(spec3,3,1))




%%
%save pics, i like these
dNm= '7_14_pics'
rNm= '5_no_abs_correctEV_k5';
mkdir(dNm);
mkdir([dNm filesep rNm]);

for h= 1:15
    hand= h+2;
    seed= jobOut{h, 2};
    figTit= ['tran_'   num2str(seed{3}) '_norm_' num2str(seed{4})];
    saveas(hand, [dNm filesep rNm filesep figTit '.jpg'] );
end

hand= 2;
h= hand-3;
seed= jobOut{h, 2};
figTit= ['tran_'   num2str(seed{3}) '_norm_' num2str(seed{4})];
saveas(hand, [dNm filesep rNm filesep 'loudtestWOrked.jpg'] );


%0-6 for amp power
%1-3 norm case


