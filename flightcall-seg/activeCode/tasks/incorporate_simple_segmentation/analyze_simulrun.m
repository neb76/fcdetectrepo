function [segment_number, raw_recall, adjusted_recall] = analyze_simulrun( out_data, varargin )
%ANALYZE_SIMULRUN Summary of this function goes here
%   Detailed explanation goes here
THRESH=.7;
n=length(out_data);
raw_recall=0;
segment_number=0;

scores=cell2mat(out_data(:,3));
n_inbox=length(find(scores>THRESH));

raw_recall=n_inbox/n;
adjusted_recall=raw_recall;

%do the adjusted part
check_str=@(x) strcmp(x,'spec');
if ~isempty(varargin)
if (strcmp( varargin{1},'combined'))
        conved_rows=find(cellfun(check_str,out_data(:,5)));
        clean_n=length(conved_rows);
        adjusted_recall=n_inbox/clean_n;
        n_tossed_out=n-clean_n;
        display(sprintf('spectral lost: %f', n_tossed_out))
end
end

%count segments
for i=1:n
    cur_seg_num=max(max(out_data{i,1}));
    segment_number=segment_number+cur_seg_num;
end

%adjust, because negative don't have 1
n_misses=n-n_inbox;
segment_number=segment_number- n_misses;

resultstr=sprintf(' # objects: %f \r raw recall: %f \r adjusted recall: %f \r', segment_number, raw_recall, adjusted_recall);
display(resultstr)
end

