thresh=.7;
%load results
full_spec_names={'DANBY_20090912_194900_rawspecs.mat',
'DANBY_20090913_194700_rawspecs.mat',
'DANBY_20090914_194500_rawspecs.mat',
'DANBY_20090915_194400_rawspecs.mat',
'DANBY_20090916_194200_rawspecs.mat'}
spec_home_base='/Volumes/NICHOLAS/full_data_5_night/full_spec_mats/';


for j=1:5
job_num=j;
l_string=strcat(spec_home_base, full_spec_names{job_num})
load(l_string)
eval(sprintf('raw_job=job%d_out;',job_num))

[spec_segMat, simple_segMat]= concat_chunks_two(raw_job);
%fix post_process
curJob=segJob('used to fix');    
    for i=1:length(spec_segMat)
        
        if ~isscalar( spec_segMat{i,1})
         
            spec=specMat{i,1};
            segged=curJob.postprocess(spec_segMat{i,1});
            segMatRef=spec_segMat{i,5};
            [~, loudInd]=  curJob.getLoudSeg(spec, segged);
            [~, mRange]= curJob.getJobMask(spec, segMatRef{1,4:5});  %switched to cell, see if it works
            [topPcnt, topReg, boxInd]= curJob.boxROI(segged, mRange, segMatRef{1,2:3});
            match=true;            
            if loudInd~=boxInd
                match=false;
            end
            
            %do a swap, like in postProcessing
            %swap box ind and current '1', so box index is 1, and 
            if topPcnt>thresh
                segged(segged==1)=boxInd;
                segged(topReg)= 1;     
            else
                %if we missed, no one has 1!
                max_ind=max(max(segged))+1;
                segged(segged==1)=max_ind;
            end
            
            
            fixed_entry={segged, topPcnt, match, 'spec', segMatRef};
            spec_segMat(i,:)=fixed_entry;
        end
    end

[ outdata_combined, outdata_simple ] = make_combined_set( spec_segMat, simple_segMat );

base=strcat('/Users/nicholasbruns/Desktop/processed_two_segs/');
night_s=strcat('night_', num2str(job_num));

segMat=outdata_combined;
save(strcat(base, 'combined/', night_s ), 'segMat');
segMat=outdata_simple;
save(strcat(base, 'simple/', night_s ), 'segMat');
end



            
% 
%             spec_segs=spec_segMat(:,1);
%             pp_func=@curJob.postprocess
%             spec_segs=cellfun(pp_func, spec_segs, 'UniformOutput', false);
%             spec_segMat(:,1)=spec_segs;