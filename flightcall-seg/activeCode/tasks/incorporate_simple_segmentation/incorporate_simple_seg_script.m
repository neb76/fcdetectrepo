load('C:\Users\neb76\Desktop\date_time_cell.mat')
night_num=5;
job_name_base= sprintf('DANBY_200909%s_rawspecs.mat', date_time_cell{night_num});%load reference cell

load(strcat('Z:\gitRepos\fc_data\full_data_5_night\full_spec_mats\', job_name_base));
ref_file_names={'DANBY_20090912_194900_NFC_637.csv',  
'DANBY_20090913_194700_NFC_367.csv',
'DANBY_20090914_194500_NFC_187.csv',
'DANBY_20090915_194400_NFC_126.csv',
'DANBY_20090916_194200_NFC_71.csv'};

ref_file_name=strcat('Z:\gitRepos\fc_data\full_data_5_night\label_files_csv\', ref_file_names{night_num});
segMatRef=parseCSV(ref_file_name,0);
nodes=20
sched=findResource('scheduler', 'configuration', 'atlasscheduler' );
both_tech_positive_segment=createJob(sched);
set(both_tech_positive_segment,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'});


n=length(specMat);
step_size=ceil(n/nodes)

for i=1:nodes
	start_chunk=step_size*(i-1)+1;
	if i==nodes
		end_chunk=n
	else
		end_chunk=step_size*i
        if end_chunk>n
            end_chunk=n;
        end
	end

	cur_spec_chunk=specMat(start_chunk:end_chunk, :);
	cur_ref_chunk=segMatRef(start_chunk:end_chunk,:);
	args={sprintf('simple and regular seg for night: %d',night_num), cur_spec_chunk, cur_ref_chunk}
	createTask(both_tech_positive_segment, @computeDistrJob_label_data_simple_simultaneous, 3, args)
    if end_chunk==n
        break
    end
    
end
    
submit(both_tech_positive_segment)