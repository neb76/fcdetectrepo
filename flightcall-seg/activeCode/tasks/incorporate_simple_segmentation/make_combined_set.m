%segmentation  %index   %recall_score  %match  %technique
function [ outdata_combined, outdata_simple ] = make_combined_set( spec_segMat, simple_segMat )
%create combined set that switched out failures for the simple
%segmentations
%   Detailed explanation goes here

outdata_spec=parse_into_outform(spec_segMat);
outdata_simple=parse_into_outform(simple_segMat);
spec_nocon_rows=find(cellfun(@isscalar,spec_segMat(:,1)));

outdata_combined=outdata_spec;
outdata_combined(spec_nocon_rows, :)=outdata_simple(spec_nocon_rows, :);


end

