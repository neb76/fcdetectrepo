seg_type_str='simple'
file_home='/Users/nicholasbruns/Desktop/processed_two_segs';
object_mat=ones(1,5);
recall_mat=ones(1,5);
a_recall_mat=ones(1,5);
for i=1:5
    %load result mat
  eval([' load ' file_home '/' seg_type_str '/night_' num2str(i) '.mat']);
  [object_mat(i), recall_mat(i), a_recall_mat(i)]=analyze_simulrun(segMat, seg_type_str);  
end


tot_objects=sum(object_mat);
av_r_recall=mean(recall_mat);
av_a_recall=mean(a_recall_mat);

eval(sprintf(' obj_array_%s= object_mat;', seg_type_str));
eval(sprintf(' tot_obj_%s= tot_objects;', seg_type_str));
eval(sprintf(' av_r_recall_%s= av_r_recall;', seg_type_str));
eval(sprintf(' av_a_recall_%s= av_a_recall;', seg_type_str));