function [ parsed_out ] = parse_into_outform( raw_out )

parsed_out=raw_out(:,1);
inds=num2cell(  1:length(parsed_out));
inds=inds';
parsed_out=horzcat(parsed_out, inds);
parsed_out=horzcat(parsed_out, raw_out(:,2:4));

end

