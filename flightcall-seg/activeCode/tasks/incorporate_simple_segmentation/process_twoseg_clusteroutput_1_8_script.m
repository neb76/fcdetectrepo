[spec_segMat, simple_segMat]= concat_chunks_two(job1_out);
thresh=.7;

%spec_fail number
% good=over .7
% 
% good/success
% good/total
%combined accuracy

%same numbers (precision and recall) but adjusted for number of segments

%total segments 


clean_rows=find(~cellfun(@isscalar,spec_segMat(:,1)));
spec_nocon_rows=find(cellfun(@isscalar,spec_segMat(:,1)));

spec_nocon=length(spec_segMat)-length(clean_rows);

simple_scores=cell2mat(   simple_segMat(:,2));
spec_scores=cell2mat( spec_segMat(:,2));

simple_inbox=length(find(simple_scores>.7));
spec_inbox=length(find(spec_scores>.7));

spec_prec=spec_inbox/(length(spec_segMat)-spec_nocon);
simple_prec=simple_inbox/length(simple_segMat);



simple_fails=find(simple_scores<.7);
spec_fails=find(spec_scores<.7);


inter_raw=intersect(simple_fails, spec_fails);

display('additional provided by spectral:')
length(simple_fails)- length(inter_raw)

intersect(spec_nocon_rows, 


[ outdata_combined, outdata_simple ] = make_combined_set( spec_segMat, simple_segMat )