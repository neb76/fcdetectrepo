%returns a matrix in form of the labeled data i've already passed to other
%group members, 
    %segmentation         index        icc
function [results_filt]=filter_batch_results(job_output, varargin)
thresh=.7;
scores=cell2mat(job_output(:,2));
filt_rows=find(scores>thresh);

if ~isempty(varargin) && strcmp(varargin{1},'match')

success_rows=find(ismember(job_output(:,6),'succes'));
filt_rows=intersect(filt_rows, success_rows);
end
filt_rows
results_filt=job_output(filt_rows,1);
icc_scores=job_output(filt_rows,3);
results_filt=horzcat(results_filt, num2cell(filt_rows),icc_scores);

end