%11/27
%outer script
%1st seg and thresh
%    correct segjob
%2nd seg and label
%    correct segjob
%sched=findResource('scheduler', 'configuration', 'atlasscheduler' );
%1

data_location='Z:\all_from_csug\';
file_base='DANBY_20090913_194700';
label_postfix='_NFC_367.csv'

file_cell=(regexp (file_base, '_', 'split'));
cur_seg_mat=parseCSV(sprintf('%s%s%s',data_location,file_base,label_postfix), 0);
segMatRef=cur_seg_mat;

specMat=cell(size(segMatRef, 1), 1);
for s= 1:size(segMatRef, 1)
    start=segMatRef{s,2};
    stop= segMatRef{s,3};
    mid= mean([start stop]);
    
    start=mid-.2;
    stop= mid+.2;
    
    spec= getspecBatch(1024,'DANBY','20090913','194700',  start, stop);
    specMat{s,1}= spec;    
    close all;
end


data_location2='Z:\all_from_csug\';
file_base2='DANBY_20090914_194500';
label_postfix2='_NFC_187.csv'
specMat2=make_specmat(data_location2,file_base2, label_postfix2);

specMat=make_specmat('Z:\all_from_csug\', 'DANBY_20090913_194700', '_NFC_367.csv')
specMat=make_specmat('Z:\all_from_csug\', 'DANBY_20090914_194500', '_NFC_187.csv')
specMat=make_specmat('Z:\all_from_csug\', 'DANBY_20090915_194400', '_NFC_126.csv')
specMat=make_specmat('Z:\all_from_csug\', 'DANBY_20090916_194200', '_NFC_71.csv')


