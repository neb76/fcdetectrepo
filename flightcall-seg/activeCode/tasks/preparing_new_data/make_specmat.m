function [specMat]=make_specmat(data_location, file_base, label_postfix )

file_cell=(regexp (file_base, '_', 'split'));
cur_seg_mat=parseCSV(sprintf('%s%s%s',data_location,file_base,label_postfix), 0);
segMatRef=cur_seg_mat;

specMat=cell(size(segMatRef, 1), 1);
for s= 1:size(segMatRef, 1)
    start=segMatRef{s,2};
    stop= segMatRef{s,3};
    mid= mean([start stop]);
    
    start=mid-.2;
    stop= mid+.2;
    
    spec= getspecBatch(1024,file_cell{:},  start, stop);
    specMat{s,1}= spec;    
    close all;
end

end