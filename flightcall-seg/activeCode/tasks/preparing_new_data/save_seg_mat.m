function []=save_seg_mat(dir_name, file_name, segMat)
label_prefix='labeled_data_';
base_dir='Z:\gitRepos\fcdetectrepo\flightcall-seg\dataFiles\new_segmentations';
save(strcat(base_dir, filesep, dir_name, filesep, label_prefix, file_name), 'segMat');
end