%get specs

csv_name='C:\Users\neb76\Desktop\nick_FCDetect_beforGit\DANBY_20090912_194900_NFC_637.csv'
[num,txt,raw]= xlsread(csv_name);

mkdir('allSpecs');




saveSpecs(raw, 'BZWA');
saveSpecs(raw, 'IRRG'); 
saveSpecs(raw, 'DBUP');
saveSpecs(raw, 'SBDN');
e=saveSpecs(raw, 'THSH');
e=saveSpecs(raw, 'THSH-aek');  
e=saveSpecs(raw, 'UNID');
e=saveSpecs(raw, 'UNID-aek');   
e=saveSpecs(raw, 'ZEEP');

e=saveSpecsSmall(raw, 'ZEEP', 5);


swig= segJob('trying to change filter type', .05, 6, 3);
swig.filtSet= 2;
spec1= getspecBatch(1024, 'DANBY', '20090912', '194900', 2692.609, 2692.726);

seg= swig.segmentThis(spec1);



%%

sched= findResource('scheduler', 'configuration', 'atlasscheduler' );
clustJ= createJob(sched);
set(clustJ,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})
%%



for n= [1 3 6]
createTask(clustJ, @computeDistrJob_tryFilt, 3, {'', .05, n, 3, spec1,3});    
createTask(clustJ, @computeDistrJob_tryFilt, 3, {'', .05, n, 3, spec1,4});    
createTask(clustJ, @computeDistrJob_tryFilt, 3, {'', .05, n, 3, spec1,5});    
createTask(clustJ, @computeDistrJob_tryFilt, 3, {'', .05, n, 3, spec1,6});    
end

submit(clustJ);
wait(clustJ);
jobOut= clustJ.getAllOutputArguments;


for s= 1:size(jobOut, 1)
    curSeg= jobOut{s,1};
    figure, imagesc(curSeg);
    seed= jobOut{s, 2};
    figTit= ['transform_num: '   num2str(seed{3}) '  norm is: ' num2str(seed{4})];
    title(figTit);
end