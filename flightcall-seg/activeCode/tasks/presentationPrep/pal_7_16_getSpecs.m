%get specs

csv_name='C:\Users\neb76\Desktop\nick_FCDetect_beforGit\DANBY_20090912_194900_NFC_637.csv'
[num,txt,raw]= xlsread(csv_name);

mkdir('allSpecs');




saveSpecs(raw, 'BZWA');
saveSpecs(raw, 'IRRG'); 
saveSpecs(raw, 'DBUP');
saveSpecs(raw, 'SBDN');
e=saveSpecs(raw, 'THSH');
e=saveSpecs(raw, 'THSH-aek');  
e=saveSpecs(raw, 'UNID');
e=saveSpecs(raw, 'UNID-aek');   
e=saveSpecs(raw, 'ZEEP');

e=saveSpecsSmall(raw, 'ZEEP', 5);


swig= segJob('trying to change filter type', .05, 6, 3);
swig.filtSet= 2;
spec1= getspecBatch(1024, 'DANBY', '20090912', '194900', 2692.609, 2692.726);

seg= swig.segmentThis(spec1);



%%

sched= findResource('scheduler', 'configuration', 'atlasscheduler' );
clustJ= createJob(sched);
set(clustJ,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})
%%



submit(clustJ);
wait(clustJ);
jobOut= clustJ.getAllOutputArguments;



for s= 1:size(jobOut, 1)
    curSeg= jobOut{s,1};
%     if curSeg ~=1
        figure, imagesc(curSeg);
        seed= jobOut{s, 2};
        figTit= ['sig: '   num2str(seed{2}) '  norm is: ' num2str(seed{4}) '  trans is: ' num2str(seed{3}) ];
        title(figTit);
%     end
end

%%

%try on several
len= 10;
segMat= parseCSV(csv_name, len);
specMat= cell(len, 1);

sched= findResource('scheduler', 'configuration', 'atlasscheduler' );
clustJ= createJob(sched);
set(clustJ,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})
%%

for s= 1:len 
    specMat{s,1}= getspecBatch(1024, 'DANBY', '20090912', '194900', segMat{s, 2:3});
end

for s= 1:len 
createTask(clustJ, @computeDistrJob_tryFilt, 3, {'', .025, 3, 1, specMat{s,1},2}); 
createTask(clustJ, @computeDistrJob_tryFilt, 3, {'', .025, 6, 1, specMat{s,1},2});
createTask(clustJ, @computeDistrJob_tryFilt, 3, {'', .025, 3, 2, specMat{s,1},2}); 
createTask(clustJ, @computeDistrJob_tryFilt, 3, {'', .025, 6, 2, specMat{s,1},2});

createTask(clustJ, @computeDistrJob_tryFilt, 3, {'', .04, 6, 1, specMat{s,1},2});
createTask(clustJ, @computeDistrJob_tryFilt, 3, {'', .04, 6, 2, specMat{s,1},2});
end


submit(clustJ);
wait(clustJ);
jobOut= clustJ.getAllOutputArguments;


s=1;
for figI= 1:len
    fig= num2str(figI);
    for js= 1:6
    curSeg= jobOut{s,1};
%     if curSeg ~=1
        figure, imagesc(curSeg);
        seed= jobOut{s, 2};
        figTit= ['fig num:' fig '  sig: '   num2str(seed{2}) '  norm is: ' num2str(seed{4}) '  trans is: ' num2str(seed{3}) ];
        title(figTit);
        s= s +1
    end
end
%     end


testTough= segJob('is it that good?', .025, 3, 1);
testTough= testTough.adjustK(2);
testTough.filtSet= 2;

seg= testTough.segmentThis(specMat{3,1});


spec1= getspecBatch(1024, 'DANBY', '20090912', '194900',21855.886, 21856.049);

spec1=  getspecBatch_nopass(1024, 'DANBY', '20090912', '194900',29175.70, 29176.096); %small dogs fighting
spec1=  getspecBatch_nopass(1024, 'DANBY', '20090912', '194900',29182.604, 29183.618); %bigger dogs fighting

spec1=  getspecBatch_nopass(1024, 'DANBY', '20090912', '194900',29182.604, 29183.618); %bigger dogs fighting
spec1=  getspecBatch_nopass(1024, 'DANBY', '20090912', '194900',32381, 32382.464); %bigger dogs fighting

