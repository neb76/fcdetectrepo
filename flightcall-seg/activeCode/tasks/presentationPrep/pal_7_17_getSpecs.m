spec1= getspecBatch(1024, 'DANBY', '20090912', '194900', 4664.826, 4664.941);

highSig= .04;
lowSig= .02;
step= .005;


sched= findResource('scheduler', 'configuration', 'atlasscheduler' );
clustJ= createJob(sched);
set(clustJ,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})
%%


sigma= highSig;
while sigma>=lowSig
createTask(clustJ, @computeDistrJob_tryFilt, 3, {'', sigma, 3, 1, spec1,2});    
createTask(clustJ, @computeDistrJob_tryFilt, 3, {'', sigma, 3, 1, spec1,3});    
createTask(clustJ, @computeDistrJob_tryFilt, 3, {'', sigma, 3, 1, spec1,4});    
createTask(clustJ, @computeDistrJob_tryFilt, 3, {'', sigma, 3, 1, spec1,5});    
sigma= sigma-step;
end


submit(clustJ);
wait(clustJ);
jobOut= clustJ.getAllOutputArguments;


for s= 1:size(jobOut, 1)
    curSeg= jobOut{s,1};
%     if curSeg ~=1
        figure, imagesc(curSeg);
        seed= jobOut{s, 2};
        figTit= ['sig: '   num2str(seed{2}) '  norm is: ' num2str(seed{4}) '  trans is: ' num2str(seed{3}) ];
        title(figTit);
%     end
end