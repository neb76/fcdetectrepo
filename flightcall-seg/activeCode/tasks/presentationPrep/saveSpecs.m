function [errVec] = saveSpecs(raw, call)
errVec=[1];
dirN= ['allSpecs' filesep call];
mkdir(dirN);
mkdir([dirN filesep 'noFilt']);
mkdir([dirN filesep 'Filt']);

vec= raw(:,10)
hits = cellfun(@(x)strcmp(x,call),vec);
callList= raw(hits,4:5);

for i = 1:size(callList,1)
    %todo: add a try catch block
    try
        spec= getpresSpecs(1024, 'DANBY', '20090912', '194900', callList{i,:});
        s= gcf;
        fName= num2str(fix(callList{i,1}));
        saveas(s, [dirN filesep 'noFilt' filesep fName '.jpg']);
        imagesc(processpic(spec, 3,1));
        saveas(s, [dirN filesep 'Filt' filesep fName '.jpg']);
        close( s);
    catch MException
        fName= fix(callList{i,1});
        eM= ['take note of ' num2str(fName)];
        display(eM);
        errVec= [errVec fix(callList{i,1})];
    end
    
end

end