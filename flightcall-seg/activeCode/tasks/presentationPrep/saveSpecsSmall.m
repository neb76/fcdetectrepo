function [errVec] = saveSpecsSmall(raw, call, len)
errVec=[1];
dirN= ['allSpecs' filesep call];
mkdir(dirN);
% mkdir([dirN filesep 'noFilt']);
% mkdir([dirN filesep 'Filt']);
mkdir([dirN filesep 'Filt2']);

vec= raw(:,10)
hits = cellfun(@(x)strcmp(x,call),vec);
callList= raw(hits,4:5);

for i = 1:len
    %todo: add a try catch block
    try
        spec= getpresSpecs(1024, 'DANBY', '20090912', '194900', callList{i,:});
        s= gcf;
        fName= num2str(fix(callList{i,1}));
%         saveas(s, [dirN filesep 'noFilt' filesep fName '.jpg']);
        figure, imagesc(processpic(spec, 3,1));
%         saveas(s, [dirN filesep 'Filt' filesep fName '.jpg']);
%         close( s);
        figure, imagesc(processpic(spec, 2,1));
        s= gcf;
        saveas(s, [dirN filesep 'Filt2' filesep fName '.jpg']);

    catch MException
        fName= fix(callList{i,1});
        eM= ['take note of ' num2str(fName)];
        display(eM);
        errVec= [errVec fix(callList{i,1})];
    end
    
end

end