%remake spec script 8/8, after fails with these test cases
spec299= specMat{299,1};
spec300=specMat{300,1};

figure, imagesc(spec299)
figure, imagesc(spec300)

spec296= specMat{293,1};
figure, imagesc(spec296)
%
blocksize= ceil(size(segMatRef,1)/32);


%first, test segMatRef
oldStart=0;
curStart=0;

for s=1:size(segMatRef,1)
    curStart= segMatRef{s, 2};
    if curStart<oldStart
        display(['strange start at' num2str(s)])
        curStart
    end
    oldStart=curStart;
end
%everything checks out, appears to be in order

%%
specMat=cell(size(segMatRef, 1), 1);
for s= 1:size(segMatRef, 1)
    start=segMatRef{s,2};
    stop= segMatRef{s,3};
    mid= mean([start stop]);
    
    start=mid-.2;
    stop= mid+.2;
    
    spec= getspecBatch(1024, 'DANBY', '20090912', '194900' , start, stop);
    specMat{s,1}= spec;    
    close all;
end