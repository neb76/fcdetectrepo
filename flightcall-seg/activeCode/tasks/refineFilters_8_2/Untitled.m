ind=399; spec= specMat{ind,1}; imagesc(processpic(spec,1,1)); %zeep
ind=85; specD= specMat{ind,1}; imagesc(processpic(specD,1,0));

figure; imagesc(processpic(spec,3,1));

newH= size(spec, 1);
newH= newH*.81; %from inspection
smallPic=spec(1:newH, :);%use this for fast things
oldmin= min(min(smallPic));%imshow(trimp, [], 'XData', [1 256], 'YData', [1 175])
if oldmin<0
    smallPic= smallPic-oldmin;
end
figure, imagesc(smallPic);

h=[3 3];
s= .5;

f=1;
imagesc(processpic_debug(spec, f, h, s));

h=3
f=6
figure, processpic_debug(spec, f, h, s);


%test med range
figure;
f=1;
sbI=1;
for h= 2:11
    hMat= [h h]; 
    subplot(3,4, sbI), processpic_debug(spec, f, hMat, sig);
    sbI= sbI+1;
end
subplot(3,4,12), imagesc(smallPic);

%gause binge , all after 9 is gaussian binge
figure
f=4;
sbI=1;
for h= 2:6
    for sig= .1:.2:1.4
        hMat= [h h];
        subplot(5,7, sbI), processpic_debug(spec, f, hMat, sig);
%         title()
        sbI= sbI+1;
    end
end

subplot(5,7,8), imagesc(smallPic);

%%this is for testing rations for neighboorhood setting
%concened about relative resolutions
figure
f= 6;
sig= .5;
sbI=1;
for x= [1 1.5 2 3 4]
    for r= [1 2 5 8 10]
        i=ceil(x*r);
        j=ceil(x);
        h= [i j]
        subplot(5, 5, sbI), processpic_debug(spec, f, h, sig);
        sbI=sbI+1;
    end
end
%

subplot(3,4,5), imagesc(smallPic);


%comparing the good looking [24 3] to disk
subplot(5,5,7), imagesc(processpic(spec,3,1));
processpic_debug(spec, f, h, sig)
