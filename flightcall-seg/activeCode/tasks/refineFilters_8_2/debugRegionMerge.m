%script to debug post process
for s= 1:size(jobOut, 1)
    seed=jobOut{s,5};
    specI= seed{1,1};
    curSeg= jobOut{s,1};
    segRow=segMatRef(specI,:);
    boxedSeg= boxTheSpec(curSeg,segRow,2);

    
    spec=specMat{specI,1};
    boxedSpec=boxTheSpec(spec, segRow,1);
    figure;
    set(gca, 'LooseInset', [0,0,0,0]);

    a=subplot(1,3,1); imagesc(boxedSpec);  %subplot, with the left photo being non filtered trim
    
    %box: # to # seconds, frequencies  
    text(1,450,segMatRef{specI,6}); %add species
    belowStr= [num2str(segMatRef{specI,4}) ' to ' num2str(segMatRef{specI,5}) ' hz'];  %add freq range
    text(1, 462, belowStr);
    
    b=subplot(1,3,2); imagesc(boxedSeg);
    
    %title- the %numth spec at %time and boxSeg is %boxInd
    time=floor(segMatRef{specI,2});
    tit=['the ' num2str(specI) 'th seg at time ' num2str(time) ' secs, box index is ' num2str(jobOut{s,2})];
    title(a,tit); 
    
    %score info
    scoreStr=['top %: ' num2str(jobOut{s,3}) '     ICC is: ' num2str(jobOut{s,4})];
    text(1,450,scoreStr);
    
    
    c=subplot(1,3,3); imagesc(processpic_outer(spec, specI, {1,[8 2]},{7, [8 2], 'rectangle'}, {3, [8 2]}));
    title('median[8 2] + average[8 2] filter')
    
%     imwrite(gcf, [svStr filesep 'spec_' num2str(time)], 'GIF', 'ScreenSize', [1920 1080]);
%     close all;
end


seedList= jobOut(:,5);
specList=cellfun(@(x) x{1,1}, seedList);
%these are JobOut indeces for the speces i want to check out
find(specList==413) %  #4
find(specList==417) %  #2

seg413= jobOut{4,1};
seg417= jobOut{2,1};


figure, imagesc(seg413)
figure, imagesc(seg417)

testJob2= segJob('',0);

rl413= testJob2.postprocess( seg413);
figure, imagesc(rl413)

rl417= testJob2.postprocess( seg417);
figure, imagesc(rl417)

testJob2.postDilation= [8 2];

%did not work, got all green, all one.
%now going to debug by setting function vars here

relabeled=seg413;
figure, imagesc(relabeled)
sj= testJob2;
segged_o= relabeled




