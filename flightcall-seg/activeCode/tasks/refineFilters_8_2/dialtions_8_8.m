%eval(sprintf(';'));

choicespecs= [2 66 327 399];

spec327= specMat{327, 1};
imagesc(spec327); title('raw 327');


%showimage
for specID=35:39;
eval(sprintf('spec%d=specMat{%d, 1};', specID, specID));
figure, eval(sprintf('imagesc(spec%d);', specID));
title(['raw ' num2str(specID)]);
end

for specID=35:39;  
    %get a little version for fun experiments
    eval(sprintf('tp%d=processpic(spec%d,1,0);', specID, specID));
    figure, eval(sprintf('imagesc(tp%d);', specID));
    title(['trimmed but raw ' num2str(specID)]);
end

specID= 545;
eval(sprintf('spec%d=specMat{%d, 1};', specID, specID));
figure, eval(sprintf('imagesc(spec%d);', specID));
title(['raw ' num2str(specID)]);
eval(sprintf('tp%d=processpic(spec%d,1,0);', specID, specID));
figure, eval(sprintf('imagesc(tp%d);', specID));
title(['trimmed but raw ' num2str(specID)]);


%do a suplot
specID=
filt=
spI= ;%subplot rows
spJ= ;%subplot columns
figure
spInd=0;
for h= 2:8
spInd=spInd+1;
subplot(spInd,spJ,spInd), eval(sprintf('processpic_debug(spec%d, %d, h, .05)', specID,filt));
end

specID=
spI= ;%subplot rows
spJ= ;%subplot columns

filtCell={{5,3},{3,[3 3]}
eval(sprintf('processpic_outer(spec%d, %d, filtCell })', specID, specID));


% %filts
% 1   median     2-d neigh
% 2   weiner     2-d neigh
% 3   average    2-d neigh
% 4   gaussian   sig & 2-d neigh
% 5   sq. closing  1-d neigh
% 6   sq. opening  1-d neigh
% 7   dilation


%look at what we have now, med ave, both with [8 2]
specID=399;
spI= 1;%subplot rows
spJ= 6;%subplot columns

figure, spInd=0; %intialize
filtCell={{1, [8 2]},{3, [8 2]}};  %med-ave
spInd=spInd+1; subplot(spI, spJ, spInd), eval(sprintf('processpic_outer(spec%d, %d, filtCell );', specID, specID));  %this line for computing 

filtCell={{1, [8 2]},{3, [8 2]}, {5, 3}};  %med-ave with close
spInd=spInd+1; subplot(spI, spJ, spInd), eval(sprintf('processpic_outer(spec%d, %d, filtCell );', specID, specID));  %this line for computing 

filtCell={{1, [8 2]},{3, [8 2]}, {6, 3}};  %med-ave with open
spInd=spInd+1;  subplot(spI, spJ, spInd), eval(sprintf('processpic_outer(spec%d, %d, filtCell );', specID, specID));  %this line for computing 

filtCell={{1, [8 2]},{3, [8 2]}, {5, 3}, {7,3}};  %med-ave with close then dilation
spInd=spInd+1; subplot(spI, spJ, spInd), eval(sprintf('processpic_outer(spec%d, %d, filtCell );', specID, specID));  %this line for computing 

filtCell={{1, [8 2]},{3, [8 2]}, {6, 3}, {7,3}};  %med-ave with close then dilation
spInd=spInd+1;  subplot(spI, spJ, spInd), eval(sprintf('processpic_outer(spec%d, %d, filtCell );', specID, specID));  %this line for computing 

filtCell={};  %med-ave with close then dilation
spInd=spInd+1;  subplot(spI, spJ, spInd), eval(sprintf('processpic_outer(spec%d, %d, filtCell );', specID, specID));  %this line for computing 

%%





%%
%
specID= 300;
jobSpecs= {.035, 6, 1, 100};

filtCell= {}
filtCell={{1, [8 2]},{3, [8 2]}, {6, 3, 'square'}, {7,3, 'square'}};  %med-ave with close then dilation
filtCell={{1, [8 2]},{3, [8 2]}, {6, [8 2], 'rectangle'}, {7,[8 2], 'rectangle'}};  %med-ave with close then dilation
filtCell={{1, [8 2]},{3, [8 2]}, {6, [8 2], 'rectangle'}, {7,[8 2], 'rectangle'} {6, [8 2], 'rectangle'}};  %try addinga  closing on top of above to terrace out hills
filtCell={{1, [8 2]},{3, [8 2]}, {6, [8 2], 'rectangle'}, {7,[8 2], 'rectangle'} {6, [24 3], 'rectangle'}};  %not much change, try more opening

filtCell={{1, [8 2]},{3, [8 2]}, {7,[8 2], 'rectangle'}};  %med-ave dilation
filtCell={{1, [8 2]},{3, [8 2]}, {7,[8 2], 'rectangle'},{5, [24 3], 'rectangle'}};  %same, now close, not open it up

filtCell={{1, [8 2]},{3, [8 2]}, {5, [8 2], 'rectangle'}, {7,[8 2], 'rectangle'}};  %med-ave with close then dilation
filtCell={{1, [8 2]},{3, [8 2]}, {5, [8 2], 'rectangle'}, {7,[8 2], 'rectangle'} {5, [8 2], 'rectangle'}};  %try addinga  closing on top of above to terrace out hills
filtCell={{1, [8 2]},{3, [8 2]}, {5, [8 2], 'rectangle'}, {7,[8 2], 'rectangle'} {5, [24 3], 'rectangle'}};  %not much change, try more opening

%try closing before med-aving
filtCell={{5, [8 2], 'rectangle'}, {1, [8 2]},{3, [8 2]}};  %med-ave with close then dilatio
filtCell={{5, [8 2], 'rectangle'}, {1, [8 2]},{3, [8 2]},  {7,[8 2], 'rectangle'}};  %med-ave with close then dilation

%med close ave dilate
filtCell={{1, [8 2]}, {5, [8 2], 'rectangle'}, {3, [8 2]},  {7,[8 2], 'rectangle'}};  %med-ave with close then dilation
filtCell={{1, [8 2]}, {5, [24 3], 'rectangle'}, {3, [8 2]},  {7,[8 2], 'rectangle'}};  %med-ave with close then dilation

filtCell={ {5, [24 3], 'rectangle'}, {7,[8 2], 'rectangle'}};  %med-ave with close then dilation
filtCell={ {1, [8 2]},{5, [24 3], 'rectangle'}, {7,[8 2], 'rectangle'}};  %med-ave with close then dilation
filtCell={ {3, [8 2]},{5, [24 3], 'rectangle'}, {7,[8 2], 'rectangle'}};  %med-ave with close then dilation

filtCell={ {3, [8 2]},{1, [8 2]},{5, [24 3], 'rectangle'}, {7,[8 2], 'rectangle'}};  %med-ave with close then dilation

%try bigger close on original med-ave close
filtCell={{1, [8 2]},{3, [8 2]}, {5, [24 3], 'rectangle'}, {7,[8 2], 'rectangle'}};  %med-ave with close then dilation


figure, processpic_outer(spec399, 399, filtCell);


%%
% ##################################################
% %for segmentation comparison   (made it through)
% ##################################################
filtCell={{1, [8 2]},{3, [8 2]}};  %med-ave  %old standard
filtCell={{1, [8 2]},{3, [8 2]}, {6, [8 2], 'rectangle'}, {7,[8 2], 'rectangle'}};  %med-ave with close then dilation  %add a closing, then dilatation
filtCell={{1, [8 2]},{3, [8 2]}, {6, [8 2], 'rectangle'}, {7,[8 2], 'rectangle'} {6, [24 3], 'rectangle'}};  %see if on same, some opening works?
filtCell={{1, [8 2]}, {5, [24 3], 'rectangle'}, {3, [8 2]},  {7,[8 2], 'rectangle'}};  %med-ave with close then dilation



%add closing on top? to plateo out?




easy= 500;
hard= 399;

% sig, transF, normCase, filt, neigh, spec, segMatRef
