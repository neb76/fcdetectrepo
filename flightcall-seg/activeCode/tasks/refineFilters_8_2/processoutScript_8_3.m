
% for k=[0 1 3]
%     eval(sprintf(';',k));
% end

%process new Filt results, 8/3/2012 morning
%first is from 
J1Out=job109.getAllOutputArguments; %first half output from cluster
run1_labeled= j1out;  %this came from parseOut(j1Out)

%clean out empties
for j= [1 ]
    eval(sprintf('idx= cellfun(@isempty, J%dOut(:,1));',j));
    eval(sprintf('J%dOut=J%dOut(~idx,:);',j,j));
end
%organize runs
%add filtersetting
for j= [ 1 ]
    eval(sprintf('J%d_labeled=parseOut(J%dOut);', j, j))
end


%filtSet is in same column position, 7, as cluster number before, as all are now 3
for j= [1]
    eval(sprintf('filtSets=J%dOut(:,1);',j));
    getFilt=@(x) x{1,6};
    filtSets=cellfun(getFilt, filtSets);
    eval(sprintf('J%d_labeled(:,7)= num2cell(filtSets);;',j));
end
%add icc
for j= [ 1 ]
    eval(sprintf('J%d_labeled(:,8)=J%dOut(:,5);', j, j))
end
%%

%##########################skipped############### 
%########### making sufficient figures ##########
%##########################skipped###############

%%quick skip to making pictures-- more rigorous figures can be added


for j=[ 1 ]
    eval(sprintf('unsorted_j%dSpecMat=getSpecPerfMat(J%dOut,2);',j,j));
end

imagesc(unsorted_k1SpecMat)

for j=[ 1 ]
    eval(sprintf('aveICC=J%dOut(:,3)',j));
    aveICC= cell2mat(aveICC);
    [vec,ix]=sort(aveICC, 'descend');
    pctI= ix';
    eval(sprintf('sorted_j%dSpecMat=unsorted_j%dSpecMat(:,pctI);',j,j));
%     eval(sprintf('sorted_j%dSpecMat=sorted_j%dSpecMat(:,1:7);',j,j)); 
end

% for k=[0 1 3]
%     eval(sprintf(';',k));
% end