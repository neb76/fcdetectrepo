sched= findResource('scheduler', 'configuration', 'atlasscheduler' );
growRegions_dilations= createJob(sched);
set(growRegions_dilations,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})



eval(sprintf('spec%d=specMat{%d, 1};', specID, specID));


specID= 123;
jobSpec={ 6 , 1, 100, 8}; %missing: {spec, stat}



filtCell= 
%add a filtCell job

filtCell{1,1}={{1, [8 2]},{3, [8 2]}, {6, 3, 'square'}, {7,3, 'square'}};  %med-ave with close then dilation
filtCell{end+1,1}={{1, [8 2]},{3, [8 2]}, {6, [8 2], 'rectangle'}, {7,[8 2], 'rectangle'}};  %med-ave with close then dilation
filtCell{end+1,1}={{1, [8 2]},{3, [8 2]}, {6, [8 2], 'rectangle'}, {7,[8 2], 'rectangle'} {6, [8 2], 'rectangle'}};  %try addinga  closing on top of above to terrace out hills
filtCell{end+1,1}={{1, [8 2]},{3, [8 2]}, {6, [8 2], 'rectangle'}, {7,[8 2], 'rectangle'} {6, [24 3], 'rectangle'}};  %not much change, try more opening

filtCell{end+1,1}={{1, [8 2]},{3, [8 2]}, {7,[8 2], 'rectangle'}};  %med-ave dilation
filtCell{end+1,1}={{1, [8 2]},{3, [8 2]}, {7,[8 2], 'rectangle'},{5, [24 3], 'rectangle'}};  %same, now close, not open it up

filtCell{end+1,1}={{1, [8 2]},{3, [8 2]}, {5, [8 2], 'rectangle'}, {7,[8 2], 'rectangle'}};  %med-ave with close then dilation
filtCell{end+1,1}={{1, [8 2]},{3, [8 2]}, {5, [8 2], 'rectangle'}, {7,[8 2], 'rectangle'} {5, [8 2], 'rectangle'}};  %try addinga  closing on top of above to terrace out hills
filtCell{end+1,1}={{1, [8 2]},{3, [8 2]}, {5, [8 2], 'rectangle'}, {7,[8 2], 'rectangle'} {5, [24 3], 'rectangle'}};  %not much change, try more opening

%try closing before med-aving
filtCell{end+1,1}={{5, [8 2], 'rectangle'}, {1, [8 2]},{3, [8 2]}};  %med-ave with close then dilatio
filtCell{end+1,1}={{5, [8 2], 'rectangle'}, {1, [8 2]},{3, [8 2]},  {7,[8 2], 'rectangle'}};  %med-ave with close then dilation

%med close ave dilate
filtCell{end+1,1}={{1, [8 2]}, {5, [8 2], 'rectangle'}, {3, [8 2]},  {7,[8 2], 'rectangle'}};  %med-ave with close then dilation
filtCell{end+1,1}={{1, [8 2]}, {5, [24 3], 'rectangle'}, {3, [8 2]},  {7,[8 2], 'rectangle'}};  %med-ave with close then dilation

filtCell{end+1,1}={ {5, [24 3], 'rectangle'}, {7,[8 2], 'rectangle'}};  %med-ave with close then dilation
filtCell{end+1,1}={ {1, [8 2]},{5, [24 3], 'rectangle'}, {7,[8 2], 'rectangle'}};  %med-ave with close then dilation
filtCell{end+1,1}={ {3, [8 2]},{5, [24 3], 'rectangle'}, {7,[8 2], 'rectangle'}};  %med-ave with close then dilation

filtCell{end+1,1}={ {3, [8 2]},{1, [8 2]},{5, [24 3], 'rectangle'}, {7,[8 2], 'rectangle'}};  %med-ave with close then dilation

%try bigger close on original med-ave close
filtCell{end+1,1}={{1, [8 2]},{3, [8 2]}, {5, [24 3], 'rectangle'}, {7,[8 2], 'rectangle'}};  %med-ave with close then dilation
filtCell{end+1,1}={{1, [8 2]},{3, [8 2]}};  %med-ave  %old standard
filtCell{end+1,1}={{1, [8 2]},{3, [8 2]}, {7,[8 2], 'rectangle'}};  %old standard with a dilation


for sig= [.01:.0075:.035]
    for fc= 1:size(filtCell,1)
        filtCellrow=filtCell{fc, 1};
        eval(sprintf('[ppic, tit]= processpic_outer(spec%d, %d, filtCellrow);', specID, specID));
        stat= segMatRef(specID, :);
        createTask(growRegions_dilations, @computeDistrJob_render, 6, {tit, sig, jobSpec{:}, ppic, stat})
    end
end
%filt cell is nested cells, so pull one out yo




submit(growRegions_dilations)
wait(growRegions_dilations)
jobOut=growRegions_dilations.getAllOutputArguments;

for s= 1:size(jobOut, 1)
    try 
    seed= jobOut{s,5};
%     specID=seed{1,1};
    curSeg= jobOut{s,1};
    if curSeg==1
        continue
    end
    segRow=segMatRef(specID,:);
    boxedSeg= boxTheSpec(curSeg,segRow,2);

    
    spec=specMat{specID,1};
    boxedSpec=boxTheSpec(spec, segRow,1);
    figure;
    set(gca, 'LooseInset', [0,0,0,0]);

    a=subplot(1,3,1); imagesc(boxedSpec);  %subplot, with the left photo being non filtered trim
    
    %box: # to # seconds, frequencies  
    text(1,450,segMatRef{specID,6}); %add species
    belowStr= [num2str(segMatRef{specID,4}) ' to ' num2str(segMatRef{specID,5}) ' hz'];
    text(1, 462, belowStr);
    
    b=subplot(1,3,2); imagesc(boxedSeg);
    
    %title- the %numth spec at %time and boxSeg is %boxInd
    time=floor(segMatRef{specID,2});
    tit=['the ' num2str(specID) 'th seg at time ' num2str(time) ' secs, box index is ' num2str(jobOut{s,2})];
    title(a,tit); 
    
    %score info
    scoreStr=['top %: ' num2str(jobOut{s,3}) '     ICC is: ' num2str(jobOut{s,4})];
    text(1,450,scoreStr);
    
    fcI= mod(s,size(filtCell,1));
    filtCellrow=filtCell{fcI, 1};
    c=subplot(1,3,3); eval(sprintf('processpic_outer(spec%d, %d, filtCellrow );', specID, specID));
    title(seed{1,2})
    sig=seed{1,3};
    text(1,450,num2str(sig));

    catch
        display('strange error');
    end
%     imwrite(gcf, [svStr filesep 'spec_' num2str(time)], 'GIF', 'ScreenSize', [1920 1080]);
%     close all;
end