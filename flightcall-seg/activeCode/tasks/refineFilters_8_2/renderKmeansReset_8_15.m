%wednesday plan
%start re-ordering my open windows

1) take a look at that last cluster job i did

2) finish the K means center initialization

3) render pics with this new 3-d plot labeled by segmentation

4)  maybe initialize all three centers up and down the center line?


tryKmeansReset_fixedBug= createJob(sched);
set(tryKmeansReset_fixedBug,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})


badIs= [600, 333, 99];
badMat= specMat(badIs, :);


for specID= badIs
    eval(sprintf('spec%d=specMat{%d, 1};', specID, specID));

end



for i=1:3
   figure, imagesc(badMat{i,1} );
end


for i= badIs
    for buf= [0 2 10 20]
    spec= specMat{i,1};
    stats= segMatRef(i,:);
    createTask(tryKmeansReset_fixedBug, @computeDistrJob_render, 7, {num2str(buf), buf, spec, stats})
    end
end


submit(tryKmeansReset_fixedBug)
wait(tryKmeansReset_fixedBug)
jobOut=tryKmeansReset_fixedBug.getAllOutputArguments;



for s= 1:size(jobOut, 1)
    try 
    seed= jobOut{s,5};
    specID=seed{1,1};
    curSeg= jobOut{s,1};
    if curSeg==1
        continue
    end
    segRow=segMatRef(specID,:);
    boxedSeg= boxTheSpec(curSeg,segRow,2);

    
    spec=specMat{specID,1};
    boxedSpec=boxTheSpec(spec, segRow,1);
    figure;
    set(gca, 'LooseInset', [0,0,0,0]);

    a=subplot(1,3,1); imagesc(boxedSpec);  %subplot, with the left photo being non filtered trim
    
    %box: # to # seconds, frequencies  
    text(1,450,segMatRef{specID,6}); %add species
    belowStr= [num2str(segMatRef{specID,4}) ' to ' num2str(segMatRef{specID,5}) ' hz'];
    text(1, 462, belowStr);
    
    b=subplot(1,3,2); imagesc(boxedSeg);
    
    %title- the %numth spec at %time and boxSeg is %boxInd
    time=floor(segMatRef{specID,2});
    tit=['the ' num2str(specID) 'th seg at time ' num2str(time) ' secs, box index is ' num2str(jobOut{s,2})];
    title(a,tit); 
    
    %score info
    scoreStr=['top %: ' num2str(jobOut{s,3}) '     ICC is: ' num2str(jobOut{s,4})];
    text(1,450,scoreStr);
    
    c=subplot(1,3,3); eval(sprintf('processpic_outer(spec%d, %d, {1,[8 2]},{3, [8 2]} );', specID, specID));
    title(seed{1,2})
    sig=seed{1,3};
    text(1,450,num2str(sig));

    catch
        display('strange error');
    end
%     imwrite(gcf, [svStr filesep 'spec_' num2str(time)], 'GIF', 'ScreenSize', [1920 1080]);
%     close all;
end