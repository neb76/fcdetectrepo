%eval(sprintf(';'));

choicespecs= [2 66 327 399];

spec327= specMat{327, 1};
imagesc(spec327); title('raw 327');


%showimage
specID=500;
eval(sprintf('spec%d=specMat{%d, 1};', specID, specID));
figure, eval(sprintf('imagesc(spec%d);', specID));
title(['raw ' num2str(specID)]);



%get a little version for fun experiments
eval(sprintf('tp%d=processpic(spec%d,1,0);', specID, specID));
figure, eval(sprintf('imagesc(tp%d);', specID));
title(['trimmed but raw ' num2str(specID)]);

%%
%this is late night on wed, 8/9

sched= findResource('scheduler', 'configuration', 'atlasscheduler' );
testLocalScaling2= createJob(sched);
set(testLocalScaling2,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})

jobSpecs= {.035, 6, 1, 4};

easy= 500;
hard= 399;
% sig, transF, normCase, filt, neigh, spec, segMatRef
for s= [easy ]
    spec= specMat{s,1};
    stats= segMatRef(s,:);
    %     createTask(testLocalScaling,@computeDistrJob_render, 6, {jobSpecs{:}, 8, spec, stats })
    %     createTask(testLocalScaling,@computeDistrJob_render, 6, {jobSpecs{:}, 12, spec, stats })
    %     createTask(testLocalScaling,@computeDistrJob_render, 6, {jobSpecs{:}, 99, spec, stats })
    for n= [1 3 6]
        createTask(testLocalScaling2,@computeDistrJob_render, 6, {.035, n, jobSpecs{3:end}, 101, spec, stats });
        for sig=1:8
            createTask(testLocalScaling2,@computeDistrJob_render, 6, {sig, n, jobSpecs{3:end}, 100, spec, stats });
        end
    end
end


submit(testLocalScaling2)
wait(testLocalScaling2)

jobOut=testLocalScaling2.getAllOutputArguments;
% 
% outerName='troubleSpecs';
% mkdir(outerName)
% for s=iSpecs
%     innerName= sprintf('spec_%d',s);
%     mkdir([outerName filesep innerName]);
% end

for s= 1:size(jobOut, 1)
    seed= jobOut{s, 5};
    specI= seed{1,1};
    curSeg= jobOut{s,1};
    segRow=segMatRef(specI,:);
    boxedSeg= boxTheSpec(curSeg,segRow,2);

    
    spec=specMat{specI,1};
    boxedSpec=boxTheSpec(spec, segRow,1);
    figure; 

    a=subplot(1,3,1); imagesc(boxedSpec);  %subplot, with the left photo being non filtered trim
    
    %box: # to # seconds, frequencies  
    text(1,450,segMatRef{specI,6}); %add species
    belowStr= [num2str(segMatRef{specI,4}) ' to ' num2str(segMatRef{specI,5}) ' hz'];
    text(1, 462, belowStr);
    
    b=subplot(1,3,2); imagesc(boxedSeg);
    
    %title- the %numth spec at %time and boxSeg is %boxInd
    time=floor(segMatRef{specI,2});
    tit=['the ' num2str(specI) 'th seg at time ' num2str(time) ' secs, box index is ' num2str(jobOut{s,2})];
    title(a,tit); 
    
    %score info
    scoreStr=['top %: ' num2str(jobOut{s,3}) '     ICC is: ' num2str(jobOut{s,4})];
    text(1,450,scoreStr);
    
    
    c=subplot(1,3,3); imagesc(processpic(spec, jobSpecs{1,4}, 0));
    title('median[8 2] + average[8 2] filter')
    
    neigh= seed{1,7};
%     saveName= [outerName filesep 'spec_' num2str(specI) filesep 'neigh_' num2str(neigh)];
%     saveas(gcf, saveName, 'jpg');
%     imwrite(gcf, [svStr filesep 'spec_' num2str(time)], 'GIF', 'ScreenSize', [1920 1080]);
%     close all;
end