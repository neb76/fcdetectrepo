%look at some of the 6 cluster with small sig

renderSqOpt_fixed= createJob(sched);
set(renderSqOpt_fixed,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})

%i can do 8, becuase 4 segs
num= 8;
specInds= randperm(size(specMat, 1));
specInds=specInds(1:num); %#

for s= specInds
    spec=specMat{s, 1};
    stats=segMatRef(s,:);
    createTask(renderSqOpt_fixed, @computeDistrJob_render, 7, {num2str(s), .025, 6, spec, stats});
    createTask(renderSqOpt_fixed, @computeDistrJob_render, 7, {num2str(s), .025, 5, spec, stats});    
    createTask(renderSqOpt_fixed, @computeDistrJob_render, 7, {num2str(s), .025, 4, spec, stats});    
    createTask(renderSqOpt_fixed, @computeDistrJob_render, 7, {num2str(s), .035, 3, spec, stats});   
end

submit(renderSqOpt_fixed)
wait(renderSqOpt_fixed)
jobOut=renderSqOpt_fixed.getAllOutputArguments;

%this loop will depend on submission indeces
loopJob=segJob('for output', .025, 6);
loopJob.postDilation= [8 2]
joInd=1;
for s= specInds               
    figure
    boxedSpec=loopJob.boxTheSpec(specMat{s,1}, segMatRef(s,:), 1);
    subplot(3,2,1), imagesc(boxedSpec);
    title(num2str(s));
    subplot(3,2,2), imagesc(processpic(specMat{s,1},4,0));

    for clustPic= 3:6
        if ~isempty(jobOut{joInd,1})
            seed= jobOut{joInd, 5};
            k=seed{1,8};
            loopJob=loopJob.adjustK(k);
            pp_seg= loopJob.postprocess(jobOut{joInd,1});
            boxed_pp_seg= loopJob.boxTheSpec(pp_seg, segMatRef(s,:), 2);
            subplot(3,2,clustPic), imagesc(boxed_pp_seg);
            title([num2str(k) ' clusters']);
        end
        joInd=joInd+1
    end  
end

%%
%################ have not yet done this
renderbadIs= createJob(sched);
set(renderbadIs,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})

badIs= [123 556 174 244];

for s= badIs
    spec=specMat{s, 1};
    stats=segMatRef(s,:);
    createTask(renderSqOpt_fixed, @computeDistrJob_render, 7, {num2str(s), .025, 6, spec, stats});
    createTask(renderSqOpt_fixed, @computeDistrJob_render, 7, {num2str(s), .025, 5, spec, stats});    
    createTask(renderSqOpt_fixed, @computeDistrJob_render, 7, {num2str(s), .025, 4, spec, stats});    
    createTask(renderSqOpt_fixed, @computeDistrJob_render, 7, {num2str(s), .035, 3, spec, stats});   
end


submit(renderSqOpt_fixed)
wait(renderSqOpt_fixed)
jobOut=renderSqOpt_fixed.getAllOutputArguments;

%%  double banded-- how are we doing?

renderDBUP2= createJob(sched);
set(renderDBUP2,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})

dbup=[7
24
42
66
85
98
120
157
325
527
545
557
599
600]


for s= dbup'
    spec=specMat{s, 1};
    stats=segMatRef(s,:);
    createTask(renderDBUP2, @computeDistrJob_render, 7, {num2str(s), .025, 6, spec, stats});
    createTask(renderDBUP2, @computeDistrJob_render, 7, {num2str(s), .025, 5, spec, stats});    
    createTask(renderDBUP2, @computeDistrJob_render, 7, {num2str(s), .025, 4, spec, stats});    
    createTask(renderDBUP2, @computeDistrJob_render, 7, {num2str(s), .035, 3, spec, stats});   
end


submit(renderDBUP2);
jobOut=renderDBUP2.getAllOutputArguments;


loopJob=segJob('for output', .025, 6);
loopJob.postDilation=[16 2];
joInd=1;
for s= dbup'               
    figure
    boxedSpec=loopJob.boxTheSpec(specMat{s,1}, segMatRef(s,:), 1);
    subplot(3,2,1), imagesc(boxedSpec);
    title(num2str(s));
    subplot(3,2,2), imagesc(processpic(specMat{s,1},4,0));

    for clustPic= 3:6
        if ~isempty(jobOut{joInd,1})
            seed= jobOut{joInd, 5};
            k=seed{1,8};
            loopJob=loopJob.adjustK(k);
            pp_seg= loopJob.postprocess(jobOut{joInd,1});
            boxed_pp_seg= loopJob.boxTheSpec(pp_seg, segMatRef(s,:), 2);
            subplot(3,2,clustPic), imagesc(boxed_pp_seg);
            title([num2str(k) ' clusters']);
        end
        joInd=joInd+1
    end  
end


%% odd peep? can we get all the small


renderOdd= createJob(sched);
set(renderOdd,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})

oddPeeps= [608 313:315];


for s= oddPeeps
    spec=specMat{s, 1};
    stats=segMatRef(s,:);
    createTask(renderOdd, @computeDistrJob_render, 7, {num2str(s), .025, 6, spec, stats});
    createTask(renderOdd, @computeDistrJob_render, 7, {num2str(s), .025, 5, spec, stats});    
    createTask(renderOdd, @computeDistrJob_render, 7, {num2str(s), .025, 4, spec, stats});    
    createTask(renderOdd, @computeDistrJob_render, 7, {num2str(s), .035, 3, spec, stats});   
end



submit(renderOdd);
jobOut=renderOdd.getAllOutputArguments;


loopJob=segJob('for output', .025, 6);
loopJob.postDilation=[8 2];
joInd=1;
for s= oddPeeps              
    figure
%     boxedSpec=loopJob.boxTheSpec(specMat{s,1}, segMatRef(s,:), 1);
    subplot(3,2,1), imagesc(specMat{s,1});
    title(num2str(s));
    subplot(3,2,2), imagesc(processpic(specMat{s,1},4,0));

    for clustPic= 3:6
        if ~isempty(jobOut{joInd,1})
            seed= jobOut{joInd, 5};
            k=seed{1,8};
            loopJob=loopJob.adjustK(k);
            pp_seg= loopJob.postprocess(jobOut{joInd,1});
%             boxed_pp_seg= loopJob.boxTheSpec(pp_seg, segMatRef(s,:), 2);
            subplot(3,2,clustPic), imagesc(pp_seg);
            title([num2str(k) ' clusters']);
        end
        joInd=joInd+1
    end  
end


smallRef= segMatRef(oddPeeps,:);

for s=oddPeeps
    row=segMatRef(s,:);
    row{:}
end
