%rendering different post processing
sched= findResource('scheduler', 'configuration', 'atlasscheduler' );
lookatPostProcess_sq= createJob(sched);
set(lookatPostProcess_sq,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})


% jobSpecs= {.035, 6, 1, 4};

num= 32;
smplI= randperm(size(specMat, 1));
smplI=smplI(1:num); %###important for indexing! be sure to keep around!


%randomly sampled 60 specs
smallSpec= specMat(smplI, :);



for i= 1:size(smallSpec,1)
    spec= smallSpec{i,1};
    specI=smplI(i);
    stats=segMatRef(specI,:);
    createTask(lookatPostProcess_sq, @computeDistrJob_render, 7, {'theoslides', 0, spec, stats} );
end



submit(lookatPostProcess_sq)
wait(lookatPostProcess_sq)
jobOut=lookatPostProcess_sq.getAllOutputArguments;

dilAfter= segJob('', 2);
dilBefore=segJob('', 1);;
dilBoth=segJob('', 3);
dilNo=segJob('', 0);

%%small for debug
for po=1:4
    postOp= postOptionCell{po,1};  %set option for post processing, used for all 32
    figure, imagesc(eval(sprintf('dil%s.postprocess(seg413);', postOp )));
    title(postOp)
end

%%on all the data
postOptionCell={'After'; 'Before'; 'Both' ;'No'}
for po= 1:4;
    postOp= postOptionCell{po,1};  %set option for post processing, used for all 32
    
    for s= 1:size(jobOut, 1)
        try
            seed= jobOut{s,5};
            specID=seed{1,1};
            curSeg= jobOut{s,1};
            if curSeg==1
                continue
            end
            eval(sprintf('dilSeg=dil%s.postprocess(curSeg);', postOp ));
            segRow=segMatRef(specID,:);
            boxedSeg= boxTheSpec(dilSeg,segRow,2);
            
            
            spec=specMat{specID,1};
            boxedSpec=boxTheSpec(spec, segRow,1);
            figure;
            set(gca, 'LooseInset', [0,0,0,0]);
            
            a=subplot(1,3,1); imagesc(boxedSpec);  %subplot, with the left photo being non filtered trim
            
            %box: # to # seconds, frequencies
            text(1,450,segMatRef{specID,6}); %add species
            belowStr= [num2str(segMatRef{specID,4}) ' to ' num2str(segMatRef{specID,5}) ' hz'];
            text(1, 462, belowStr);
            
            b=subplot(1,3,2); imagesc(boxedSeg);
            
            %title- the %numth spec at %time and boxSeg is %boxInd
            time=floor(segMatRef{specID,2});
            tit=['the ' num2str(specID) 'th seg at time ' num2str(time) ' secs, box index is ' num2str(jobOut{s,2})];
            title(a,tit);
            
            %score info
            scoreStr=['top %: ' num2str(jobOut{s,3}) '     ICC is: ' num2str(jobOut{s,4})];
            text(1,450,scoreStr);
            
            c=subplot(1,3,3); processpic_outer(spec, specID, {1,[8 2]},{3, [8 2]} );
            title(postOp); %later, could include the filter setting if I'd like, no need me thinks

            
        catch
            display('strange error');
        end
        %     imwrite(gcf, [svStr filesep 'spec_' num2str(time)], 'GIF', 'ScreenSize', [1920 1080]);
        %     close all;
    end
end