lookatPostProcess_six= createJob(sched);
set(lookatPostProcess_six,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})


smallSeg= [123 244 174 545 556];

smallSpec= specMat(smallSeg, :);


for i= 1:size(smallSpec,1)
    spec= smallSpec{i,1};
    specI=smallSeg(i);
    stats=segMatRef(specI,:);
    createTask(lookatPostProcess_six, @computeDistrJob_render, 7, {'theoslides', 0, spec, stats} );
end

submit(lookatPostProcess_six)

wait(lookatPostProcess_six)
jobOut=lookatPostProcess_six.getAllOutputArguments;

for i=17:20
saveas(i, ['figure' num2str(i)], 'jpg');
end



postOp=segJob('test', 1);
postOp.postDilation= [16 3];
postOp.postDilation= 0;


for s= 1:size(jobOut, 1)
        try
            seed= jobOut{s,5};
            specID=seed{1,1};
            curSeg= jobOut{s,1};
            if curSeg==1
                continue
            end
            dilSeg=postOp.postprocess(curSeg);
            segRow=segMatRef(specID,:);
            boxedSeg= boxTheSpec(dilSeg,segRow,2);
            
            
            spec=specMat{specID,1};
            boxedSpec=boxTheSpec(spec, segRow,1);
            figure;
            set(gca, 'LooseInset', [0,0,0,0]);
            
            a=subplot(1,3,1); imagesc(boxedSpec);  %subplot, with the left photo being non filtered trim
            
            %box: # to # seconds, frequencies
            text(1,450,segMatRef{specID,6}); %add species
            belowStr= [num2str(segMatRef{specID,4}) ' to ' num2str(segMatRef{specID,5}) ' hz'];
            text(1, 462, belowStr);
            
            b=subplot(1,3,2); imagesc(boxedSeg);
            
            %title- the %numth spec at %time and boxSeg is %boxInd
            time=floor(segMatRef{specID,2});
            tit=['the ' num2str(specID) 'th seg at time ' num2str(time) ' secs, box index is ' num2str(jobOut{s,2})];
            title(a,tit);
            
            %score info
            scoreStr=['top %: ' num2str(jobOut{s,3}) '     ICC is: ' num2str(jobOut{s,4})];
            text(1,450,scoreStr);
            
            c=subplot(1,3,3); processpic_outer(spec, specID, {1,[8 2]},{3, [8 2]} );
            title(postOp); %later, could include the filter setting if I'd like, no need me thinks

            
        catch
            display('strange error');
        end
        %     imwrite(gcf, [svStr filesep 'spec_' num2str(time)], 'GIF', 'ScreenSize', [1920 1080]);
        %     close all;
end 