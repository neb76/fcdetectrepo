%resampling test
sched= findResource('scheduler', 'configuration', 'atlasscheduler' );
sample_pic_small= createJob(sched);
set(sample_pic_small,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})

jobSpec={ 6 , 1, 100, 8}; %missing: {spec, stat}


specID= 545;
eval(sprintf('spec%d=specMat{%d, 1};', specID, specID));
figure, eval(sprintf('imagesc(spec%d);', specID));
title(['raw ' num2str(specID)]);
eval(sprintf('tp%d=processpic(spec%d,1,0);', specID, specID));
figure, eval(sprintf('imagesc(tp%d);', specID));
title(['trimmed but raw ' num2str(specID)]);

figure, [tp, tit]=processpic_outer(spec123, 123, filtCell);

filtCell{1,1}= {{8, .5}};  %just shrink
filtCell{end+1,1}={{1, [8 2]},{3, [8 2]}, {8,.5}};   %old standard then shrink
filtCell{end+1,1}={{8,.5}, {1, [8 2]},{3, [8 2]}, };  %shrink then old old st
%filt, resize, filt
filtCell{end+1,1}={{1, [8 2]},{3, [8 2]}, {8,.5}, {1, [8 2]},{3, [8 2]}};  %old shrink old
filtCell{end+1,1}={{1, [8 2]},{3, [8 2]}};  %pure old


for sig= [.02:.005:.035]
    for fc= 1:size(filtCell,1)
        filtCellrow=filtCell{fc, 1};
        eval(sprintf('[ppic, tit]= processpic_outer(spec%d, %d, filtCellrow);', specID, specID));
        stat= segMatRef(specID, :);
        createTask(sample_pic_small, @computeDistrJob_render, 6, {tit, sig, jobSpec{:}, ppic, stat})
    end
end

submit(sample_pic_small)
wait(sample_pic_small)
jobOut=sample_pic_small.getAllOutputArguments;

for s= 1:(size(jobOut, 1)/2)
    try
    seed= jobOut{s,5};
%     specID=seed{1,1};
    curSeg= jobOut{s,1};
    if curSeg==1
        continue
    end
    segRow=segMatRef(specID,:);
%     boxedSeg= boxTheSpec(curSeg,segRow,2);
    boxedSeg= curSeg;
    
    spec=specMat{specID,1};
    boxedSpec=boxTheSpec(spec, segRow,1);
    figure;
    set(gca, 'LooseInset', [0,0,0,0]);

    a=subplot(1,3,1); imagesc(boxedSpec);  %subplot, with the left photo being non filtered trim
    
    %box: # to # seconds, frequencies  
    text(1,450,segMatRef{specID,6}); %add species
%     belowStr= [num2str(segMatRef{specID,4}) ' to ' num2str(segMatRef{specID,5}) ' hz'];
%     text(1, 462, belowStr);
    sig=seed{1,3};
    sig=num2str(sig);
    text(1,462,sig);   
    
    b=subplot(1,3,2); imagesc(boxedSeg);
    
    %score info
    scoreStr=['top %: ' num2str(jobOut{s,3}) '     ICC is: ' num2str(jobOut{s,4})];
    text(1,450,scoreStr);   

    
    %title- the %numth spec at %time and boxSeg is %boxInd
    time=floor(segMatRef{specID,2});
    tit=['the ' num2str(specID) 'th seg at time ' num2str(time) ' secs, box index is ' num2str(jobOut{s,2})];
    title(a,tit); 
%     
%     fcI= mod(s,size(filtCell,1))+1;
%     filtCellrow=filtCell{fcI, 1};
%     c=subplot(1,3,3); eval(sprintf('processpic_outer(spec%d, %d, filtCellrow );', specID, specID)); 
% 
%     title(seed{1,2})
    
    catch ME
        display(getReport(ME,'extended'));
    end
%     imwrite(gcf, [svStr filesep 'spec_' num2str(time)], 'GIF', 'ScreenSize', [1920 1080]);
%     close all;
end


for s= (size(jobOut, 1)/2+1):size(jobOut, 1)
    try
    seed= jobOut{s,5};
%     specID=seed{1,1};
    curSeg= jobOut{s,1};
    if curSeg==1
        continue
    end
    segRow=segMatRef(specID,:);
%     boxedSeg= boxTheSpec(curSeg,segRow,2);
    boxedSeg= curSeg;
    
    spec=specMat{specID,1};
    boxedSpec=boxTheSpec(spec, segRow,1);
    figure;
    set(gca, 'LooseInset', [0,0,0,0]);

    a=subplot(1,3,1); imagesc(boxedSpec);  %subplot, with the left photo being non filtered trim
    
    %box: # to # seconds, frequencies  
    text(1,450,segMatRef{specID,6}); %add species
%     belowStr= [num2str(segMatRef{specID,4}) ' to ' num2str(segMatRef{specID,5}) ' hz'];
%     text(1, 462, belowStr);
    sig=seed{1,3};
    sig=num2str(sig);
    text(1,462,sig);   
    
    b=subplot(1,3,2); imagesc(boxedSeg);
    
    %score info
    scoreStr=['top %: ' num2str(jobOut{s,3}) '     ICC is: ' num2str(jobOut{s,4})];
    text(1,450,scoreStr);   

    
    %title- the %numth spec at %time and boxSeg is %boxInd
    time=floor(segMatRef{specID,2});
    tit=['the ' num2str(specID) 'th seg at time ' num2str(time) ' secs, box index is ' num2str(jobOut{s,2})];
    title(a,tit); 
%     
%     fcI= mod(s,size(filtCell,1))+1;
%     filtCellrow=filtCell{fcI, 1};
%     c=subplot(1,3,3); eval(sprintf('processpic_outer(spec%d, %d, filtCellrow );', specID, specID)); 
% 
%     title(seed{1,2})
    
    catch ME
        display(getReport(ME,'extended'));
    end
%     imwrite(gcf, [svStr filesep 'spec_' num2str(time)], 'GIF', 'ScreenSize', [1920 1080]);
%     close all;
end




