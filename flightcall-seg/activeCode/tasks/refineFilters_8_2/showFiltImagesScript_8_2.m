ind=399; spec= specMat{ind,1}; figure; imagesc(processpic(spec,1,0)); %zeep
ind=85; spec= specMat{ind,1}; imagesc(processpic(spec,1,0));


figure; imagesc(processpic(spec,3,1));

newH= size(spec, 1);
newH= newH*.81; %from inspection
smallPic=spec(1:newH, :);%use this for fast things
oldmin= min(min(smallPic));%imshow(trimp, [], 'XData', [1 256], 'YData', [1 175])
if oldmin<0
    smallPic= smallPic-oldmin;
end
figure, imagesc(smallPic);

h=[3 3];
s= .5;

f=1;
imagesc(processpic_debug(spec, f, h, s));

h=3
f=6
figure, processpic_debug(spec, f, h, s);


%test med range
figure;
f=1;
sbI=1;
for h= 2:11
    hMat= [h h]; 
    subplot(3,4, sbI), processpic_debug(spec, f, hMat, sig);
    sbI= sbI+1;
end
subplot(3,4,12), imagesc(smallPic);

%gause binge , all after 9 is gaussian binge
figure
f=4;
sbI=1;
for h= 2:6
    for sig= .1:.2:1.4
        hMat= [h h];
        subplot(5,7, sbI), processpic_debug(spec, f, hMat, sig);
%         title()
        sbI= sbI+1;
    end
end

subplot(5,7,8), imagesc(smallPic);

%%this is for testing rations for neighboorhood setting
%concened about relative resolutions

%
figure
f= 3;
sig= 3;
sbI=1;
for x= [1 1.5 2 3 4]
    for r= [1 2 5 8 10]
        i=ceil(x*r);
        j=ceil(x);
        h= [i j]
        subplot(5, 5, sbI), processpic_debug(spec, f, h, sig);
        sbI=sbI+1;
    end
end
subplot(3,4,5), imagesc(smallPic);


%comparing the good looking [24 3] to disk
subplot(5,5,7), imagesc(processpic(spec,3,1));
processpic_debug(spec, f, h, sig)


%holding time constant at 1
f=1;
sig=.5;
t=3;
figure
sbI=1;
for freq= 1:2:50
    h= [freq t];
    subplot(5, 5, sbI), processpic_debug(spec, f, h, sig);
        sbI=sbI+1;

    
end


%holding neighborrdhood at [8 4]


%5:04 pm experiment: test for Gaussian sigma and nighboorhoods that seem
%reasonable
%
%%this is for testing rations for neighboorhood setting
%concened about relative resolutions
f= 1;
for sig= [.1 .25 .75 1 1.5]
    figure
    sbI=1;
    for x= [1 1.5 2 3 4]
        for r= [1 2 5 8 10]
            i=ceil(x*r);
            j=ceil(x);
            h= [i j]
            subplot(5, 5, sbI), processpic_debug(spec, f, h, sig);
            sbI=sbI+1;
        end
    end
end
%

figure, processpic_debug(spec, 1, [12 2], .75);
figure, processpic_debug(spec, 1, [8 2], .5)
figure, processpic_debug(spec, 1, [24 3], 1)
figure, processpic_debug(spec, 1, [15 3], 1)
figure, processpic_debug(spec, 1, [8 1], .5)


%5:59 experiment-- test the combination set, med and gaussian

figure, processpic_debug(spec, 16, [12 2], .75);


figure, processpic_debug(spec, 3, [8 2], .5)
figure, processpic_debug(spec,3, [10 2], .5)
figure, processpic_debug(spec, 3, [8 1], .5)



%6:48 -- is average and gaussian better??
sig=.5;
for f= [5 6 8 9 ]
    figure
    sbI=1;
    for x= [1 1.5 2 3 4]
        for r= [1 2 5 8 10]
            i=ceil(x*r);
            j=ceil(x);
            h= [i j]
            subplot(5, 5, sbI), processpic_debug(spec, f, h, sig);
            sbI=sbI+1;
        end
    end
end
%


sig= .5;
h= [8 2];
spec1= specMat{399,1};
spec2= specMat{85,1};
figure;
    subplot(3, 4, 1), processpic_debug(spec1, 8, h, sig);

    subplot(3, 4, 2), processpic_debug(spec1, 6, h, sig);

    subplot(3, 4, 5), processpic_debug(spec1, 9, h, sig);

    subplot(3, 4, 6), processpic_debug(spec1, 5, h, sig);
    
    subplot(3, 4, 3), processpic_debug(spec2, 8, h, sig);

    subplot(3, 4, 4), processpic_debug(spec2, 6, h, sig);

    subplot(3, 4, 7), processpic_debug(spec2, 9, h, sig);

    subplot(3, 4, 8), processpic_debug(spec2, 5, h, sig);
    %extra reffs
    subplot(3, 4, 9), imagesc(processpic(spec1, 1, 0));

    subplot(3, 4, 10), processpic_debug(spec1, 6, h, sig); %sharp with gaus to med

    subplot(3, 4, 11), imagesc(processpic(spec2, 1, 0));

    subplot(3, 4, 12), processpic_debug(spec2, 6, h, sig); %sharp with gause to med
    
    
    
%8:33 -- last tests, ensuring neighboorhoods
%test1-- fix med, vary others
sig=.5;
for f= [5 6 8 9 ]
    figure
    sbI=1;
    for x= [1 1.5 2 3 4]
        for r= [1 2 5 8 10]
            i=ceil(x*r);
            j=ceil(x);
            h= [i j]
            subplot(5, 5, sbI), processpic_debug(spec1, f, h, sig);
            sbI=sbI+1;
        end
    end
end

sig=.5;
for f= [5 6 8 9 ]
    figure
    sbI=1;
    for x= [1 1.5 2 3 4]
        for r= [1 2 5 8 10]
            i=ceil(x*r);
            j=ceil(x);
            h= [i j]
            subplot(5, 5, sbI), processpic_debug(spec2, f, h, sig);
            sbI=sbI+1;
        end
    end
end

