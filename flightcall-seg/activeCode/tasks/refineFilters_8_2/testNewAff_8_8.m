%testing 3 new filter settings on all data, testing for apporpriate sigma
%8/2/2012, Thursday evening, beggining at 8:46 pm
%

%
%cluster setup
%testcase
sched= findResource('scheduler', 'configuration', 'atlasscheduler' );
testShot= createJob(sched);
set(testShot,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})

createTask(testShot, @computeDistrJob_val, 8, {.03, 6, 1, 5});
submit(testShot)


debugJ= segJob('debug job', .03, 6, 1);
debugJ.sharp=0;
debugJ.filtSet= 5;


%% the code post debug

testnewNeigh_reboot2= createJob(sched);
set(testnewNeigh_reboot2,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})


highSig= .05;
lowSig= .016;
step= .005;
normC=1;

sig= highSig;

for n= [8 12 99]
    sig=highSig
    while sig>= lowSig
        createTask(testnewNeigh_reboot2, @computeDistrJob_val, 8, {sig, 6, normC, 4, n});
        sig=sig-step;
    end
end


submit(testnewNeigh_reboot2) %submitted at 12:40 thursday evening
wait(testnewNeigh_reboot2)





