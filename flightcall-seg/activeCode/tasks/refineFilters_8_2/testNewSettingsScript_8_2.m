%testing 3 new filter settings on all data, testing for apporpriate sigma
%8/2/2012, Thursday evening, beggining at 8:46 pm
%

%
%cluster setup
%testcase
sched= findResource('scheduler', 'configuration', 'atlasscheduler' );
testShot= createJob(sched);
set(testShot,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})

createTask(testShot, @computeDistrJob_val, 8, {.03, 6, 1, 5});
submit(testShot)


debugJ= segJob('debug job', .03, 6, 1);
debugJ.sharp=0;
debugJ.filtSet= 5;
d

%% the code post debug

sched= findResource('scheduler', 'configuration', 'atlasscheduler' );
testNewFilts_norm2= createJob(sched);
set(testNewFilts_norm2,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})


highSig= .05;
lowSig= .015;
step= .005;
normC=1;

for fs=[4 5 6]
    sig= highSig;
    while sig>= lowSig
        createTask(testNewFilts_norm2, @computeDistrJob_val, 8, {sig, 6, normC, fs});
        sig=sig-step;       
    end
end

fs= 7;
sig= highSig;
lowSig=.0160
while lowSig < sig 
    createTask(testNewFilts_norm2, @computeDistrJob_val, 8, {sig, 6, normC, fs});
    sig=sig-step; 
end

submit(testNewFilts_norm2) %submitted at 12:40 thursday evening

%% prepped for friday morning, 8-3, in case the results look pretty good
%friday morning: results look great
testNewFilts_norm2= createJob(sched);
set(testNewFilts_norm2,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})


highSig= .05;
lowSig= .015;
step= .005;
normC=2;

for fs=[4 5 6]
    sig= highSig;
    while sig>= lowSig
        createTask(testNewFilts_norm2, @computeDistrJob_val, 8, {sig, 6, normC, fs});
        sig=sig-step;       
    end
end

fs= 7;
sig= highSig;
lowSig=.0160
while lowSig < sig 
    createTask(testNewFilts_norm2, @computeDistrJob_val, 8, {sig, 6, normC, fs});
    sig=sig-step; 
end

submit(testNewFilts_norm2) %submitted at 12:40 thursday evening




