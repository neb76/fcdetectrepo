%
sched= findResource('scheduler', 'configuration', 'atlasscheduler' );
validateSampledRegions_p75= createJob(sched);
set(validateSampledRegions_p75,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})

%we have set, in seg job, norm to 1 and transCase to 6,    random walk and
%cubic transformation

filtCell{1,1}= {{8, .75}};  %just shrink
filtCell{end+1,1}={{1, [8 2]},{3, [8 2]}, {8,.75}};   %old standard then shrink
filtCell{end+1,1}={{8,.75}, {1, [8 2]},{3, [8 2]}, };  %shrink then old old st
%filt, resize, filt
filtCell{end+1,1}={{1, [8 2]},{3, [8 2]}, {8,.75}, {1, [8 2]},{3, [8 2]}};  %old shrink old
filtCell{end+1,1}={{1, [8 2]},{3, [8 2]}};  %pure old


for sig= [.02:.005:.035]
    for fc= 1:size(filtCell,1)
        filtCellRow= filtCell{fc, 1};
        createTask(validateSampledRegions_p75, @computeDistrJob_valComboFilt, 8, {sig, filtCellRow} );
    end
end

submit(validateSampledRegions_p75)
wait(validateSampledRegions_p75)
jobOut= validateSampledRegions_p75.getAllOutputArguments

%add one more
validateSampledRegions_single= createJob(sched);
set(validateSampledRegions_single,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})

for sig= [.02:.005:.035]
    createTask(validateSampledRegions_single, @computeDistrJob_valComboFilt, 8, {sig, {0}} );
end
submit(validateSampledRegions_single) %submited at 12:08, sun night 8-13  , intended to be combined with above for processing
jobOut= validateSampledRegions_single.getAllOutputArguments

errmessgs= jobOut(:,8);
errmessgs{:}
mes= errmessgs{1,1}
mes= mes{2,3}
%%
%while its running, we now check how these look

%first, test these on an easy

lookatRunningRegions2= createJob(sched);
set(lookatRunningRegions2,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})


specID=  303;
eval(sprintf('spec%d=specMat{%d, 1};', specID, specID));
figure, eval(sprintf('imagesc(spec%d);', specID));
title(['raw ' num2str(specID)]);
eval(sprintf('tp%d=processpic(spec%d,1,0);', specID, specID));
figure, eval(sprintf('imagesc(tp%d);', specID));
title(['trimmed but raw ' num2str(specID)]);

for sig= [.02:.005:.035]
    for fc= 1:size(filtCell,1)
        filtCellRow= filtCell{fc, 1};
        eval(sprintf('[ppic, tit]= processpic_outer(spec%d, %d, filtCellrow);', specID, specID));
        stats= segMatRef(specID, :);
        createTask(lookatRunningRegions2, @computeDistrJob_render, 6, {tit, sig, 100, ppic, stats} );
    end
end

submit(lookatRunningRegions2)
wait(lookatRunningRegions2)

jobOut= lookatRunningRegions2.getAllOutputArguments

scores= jobOut(:,3)

seeds= jobOut(:,1);
seeds= seeds{:}

getSig=@(x) x{1,2}
sigs= cellfun(getSig, seeds);

getFilt= @(x) x(1,6:end)
filts= cellfun(getFilt, seeds, 'UniformOutput', false);


getFirst= @(x) x{1,1};

showFilts= @(x) cellfun(getFirst, x)
filtfull= cellfun(showFilts,filts, 'UniformOutput', false)



X= sigs;
Y= cell2mat(scores);

g1= cellfun(@num2str,filtfull, 'UniformOutput', false);

figure
gscatter(X,Y,g1)

