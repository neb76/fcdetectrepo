sched= findResource('scheduler', 'configuration', 'atlasscheduler' );
validateDilation_braces_reordered2= createJob(sched);
set(validateDilation_braces_reordered2,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})



filtCell{1,1}={{1, [8 2]},{7,  [16 2], 'rectangle'}, {3, [8 2]  }};  % sneak dilations in between med and ave- big neigh
filtCell{end+1,1}={{1, [8 2]},{7, [8 2], 'rectangle'}, {3, [8 2]  }};  % sneak dilations in between med and ave- small neigh
filtCell{end+1,1}={{1, [8 2]},{3, [8 2]}, {7, [16 2], 'rectangle'} };  % old with big rec dilation
filtCell{end+1,1}={{1, [8 2]},{3, [8 2]}, {7, [8 2], 'rectangle'} };  % old with small rec dilation
filtCell{end+1,1}={{1, [8 2]},{3, [8 2]}, {7, 4, 'diamond'} };  % old with diamond dilation
filtCell{end+1,1}={{1, [8 2]},{3, [8 2]}};  % baseline- old standard

%looks like if using comboFilt, need to grabe who cell, not contents.  USE
%BRACES NOT PARENS  (gives you {-} {-}  and not {{-} {-}}   )
for sig= [.02:.005:.035]
    for fc= 1:size(filtCell,1)
        filtCellRow= filtCell{fc, 1};
        createTask(validateDilation_braces_reordered2, @computeDistrJob_valComboFilt, 8, {sig, filtCellRow} );
    end
end

submit(validateDilation_braces_reordered2)
wait(validateDilation_braces_reordered2)

jobOut= validateDilation_braces_reordered2.getAllOutputArguments;

errs= jobOut(:,8);

oneLog= errs{1,1};
oneLog{2,3}  %show the message for this mysterious error



% Undefined function 'eq' for input arguments of type 'cell'.
% 
% Error in processpic_ingredients (line 48)
% if filt==1
% 
% Error in processpic_outer (line 20)
%         [trimp, tit]= processpic_ingredients(trimp, tit, filtSpec{:});
% 
% Error in segJob/segmentThis (line 70)
%                 pp= processpic_outer( spec, 0, sj.filtSet{:} ) %***perhaps move process pic up to get spec(depends on what I do for create mask)
% 
% Error in computeDistrJob_valComboFilt (line 79)
%         segged= curJob.segmentThis(spec);
% 
% Error in dctEvaluateFunction>iEvaluateWithNoErrors (line 21)
%         [out{1:nOut}] = feval(fcn, args{:});
% 
% Error in dctEvaluateFunction (line 7)
% [textOut, out, errOut] = evalc('iEvaluateWithNoErrors(fcn, nOut, args)');
% 
% Error in dctEvaluateTask>iEvaluateTask/nEvaluateTask (line 273)
%             [output, errOutput, cellTextOutput{end+1}] = dctEvaluateFunction(fcn, nOut, args, capText);
% 
% Error in dctEvaluateTask>iEvaluateTask (line 134)
% nEvaluateTask();
% 
% Error in dctEvaluateTask (line 58)
%         [resultsFcn, taskPostFcn, taskEvaluatedOK] = iEvaluateTask(job, task, runprop);
% 
% Error in distcomp_evaluate_filetask>iDoTask (line 161)
%     dctEvaluateTask(postFcns, finishFcn);
% 
% Error in distcomp_evaluate_filetask (line 64)
%     iDoTask(handlers, postFcns);

