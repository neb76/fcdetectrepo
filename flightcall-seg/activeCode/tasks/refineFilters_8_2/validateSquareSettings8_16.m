%sweep sigmas and K's
sqSweep_full= createJob(sched);
set(sqSweep_full,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})


for k=2:6
    for sig= .015:.005:.04
        createTask(sqSweep_full, @computeDistrJob_valComboFilt, 8, {sig, k} );
    end
end


submit(sqSweep_full)
wait(sqSweep_full)
jobOut= job192.getAllOutputArguments;

J1Out= jobOut;
%i forgot to add clust size to test, so add it on after
jobI=1;
for k=2:6
    for sig= .015:.005:.04
        jobOut{jobI,9}=k;
        jobI=jobI+1;
    end
end



for j= [1]
    eval(sprintf('idx= cellfun(@isempty, J%dOut(:,1));',j));
    eval(sprintf('J%dOut=J%dOut(~idx,:);',j,j));
end



seeds= J1Out(:,1);
seeds_full=cellfun(@(x) x{:}, seeds, 'UniformOutput', false)

sigs= cellfun(@(x) x{1,2}, seeds);
clusts= cell2mat(  J1Out(:,9));
scores= cell2mat( J1Out(:,3));

labeledMat=[clusts sigs scores]

sort_labeldMat=sortrows(labeledMat, -3);

