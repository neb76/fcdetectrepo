
sched=findResource('scheduler', 'configuration', 'atlasscheduler' );
load('Z:\gitRepos\fcdetectrepo\flightcall-seg\dataFiles\more_nights\new_data_names.mat')

test_sym_4_specs=createJob(sched);
set(test_sym_4_specs,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})
%name_file='Z:\gitRepos\fcdetectrepo\flightcall-seg\dataFiles\more_nights\new_data_names'

createTask(test_sym_4_specs, @computeDistrJob_segment_test_sym, 5, {'4 spec test', 1, two_spec});

submit(test_sym_4_specs)
raw_outs=test_sym_4_specs.getAllOutputArguments




%#######################################################
%#######################################################
%#######################################################
%#######################################################
test_compare=createJob(sched);
set(test_compare,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})

createTask(test_compare, @computeDistrJob_segment_test_sym, 5, {'with symmetry check', 1, specMat});
createTask(test_compare, @computeDistrJob_segment_test_sym, 5, {'without symmetry check', 0, specMat});

submit(test_compare)
job1_outputs=test_compare.getAllOutputArguments





