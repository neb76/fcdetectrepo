%template for fast dev--    eval(sprintf('=;',));
%much of this happened, actually, on 7/26
%first, splice back together the processing blocks for each
J2Aout=J_2clustAgainA.getAllOutputArguments;  %job61
J2Bout=J_2clustAgainB.getAllOutputArguments; %job62
J3Aout= job64.getAllOutputArguments;
J3Bout=job53.getAllOutputArguments;
J4Aout=job56.getAllOutputArguments;
J4Bout=job57.getAllOutputArguments;

%j3 is missing the correct values for ICC, so fixing widths
J3Acopy= J3Aout;
J3AoutF= J3Acopy(:,1:3);
J3AoutF(:,4)=cell(24,1); %add missing space
J3AoutF(:,5:8)= J3Acopy(:,4:7);
J3Aout=J3AoutF;

% % incorporate new, fixed J3A_out, recomplie, overwrite the saved file
% get out vecs of pos jobs
% for jn= [47 53 64]
%     eval(sprintf('j%dout=job%d.getAllOutputArguments;',jn, jn))
% end

J3out_old=J3out;
J3out=vertcat(j64out, j53out);

%concantonate all pairs to one set
for k= 2:4
    eval(sprintf('J%dout=vertcat(J%dAout, J%dBout);', k, k, k))
end
%done, save
%%

%build, for each job setting, nicely labeled set
%   K[k]out= [socre num  name sig trans norm
for k= 2:4
    eval(sprintf('k%d_labeled=parseOut(J%dout);', k, k))
end

%scatter plot for each
for k=2:4
    eval(sprintf('figure, plotSingleScat(k%d_labeled);', k));
    tit= sprintf('%d clusters', k);
    eval('title(tit)');
end
%%

%scatter for all at once
%add cluster numer to labeled
for k=2:4
    eval(sprintf('k%d_labeled(:,7)={%d};', k, k));
end
masterlist_labeled= vertcat(k2_labeled, k3_labeled, k4_labeled);
plotAllScat(masterlist_labeled);
%%

%bring in ICC
%clean out bad answers in AV icc readings
for k=2:4
    eval(sprintf('aveICC=cleanICC(J%dout);', k));
    aveICC= num2cell(aveICC);
    eval(sprintf('k%d_labeled(:,8)=aveICC;', k));
end

%first, chec how it compares to my meteric
%   :expCase 1, looks like old metric, only now ICC instead
for k=2:4
    eval(sprintf('figure, compareMetScatter(k%d_labeled,1);', k));
    tit= sprintf('%d clusters', k);
    eval('title(tit)');
end
%I used ylim([.08 .4])

%second, look at using a "per cluser" adjustment on icc
for k=2:4
    eval(sprintf('figure, compareMetScatter(k%d_labeled,2);', k));
    tit= sprintf('%d clusters', k);
    eval('title(tit)');
end

%third, plot x as metric, y as ICC
for k=2:4
    eval(sprintf('figure, compareMetScatter(k%d_labeled,3);', k));
    tit= sprintf('%d clusters', k);
    eval('title(tit)');
end
%use ylim([.01 .4])


%check out which spec 
%case1, is sorted columns
for k=2:4
    eval(sprintf('sorted_k%dSpecMat=getSpecPerfMat(J%dout,1);',k,k));
end

%case 2--unsorted
for k=2:4
    eval(sprintf('unsorted_k%dSpecMat=getSpecPerfMat(J%dout,2);',k,k));
end


%need to clean up ICC's
for k=2:4
    eval(sprintf('cleanICCvec= cleanICC(J%dout);', k));
    cleanICCvec=num2cell(cleanICCvec);
    eval(sprintf('J%dout(:,5)= cleanICCvec;',k));
end

specHeatMaps(J2out);
specHeatMaps(J3out);
specHeatMaps(J4out);


%looking at one good job, and what the mistakes look like in terms of what
%I have
bestRes=J3out{35,2};
bestRes= bestRes'
bestRes_p= bestRes;
bestRes_i=J3out{35,4};
bestRes_i= bestRes_i';


cond= @(x) (x<1);
clean= @(x) x*cond(x);
bestRes_i=arrayfun(clean, bestRes_i);

scatter(bestRes_i, bestRes_p);
figure, scatter(bestRes_p, bestRes_i);



errLog= J3out{35,8};

nonmatch=zeros(633,1);
for i= 2:size(errLog,1)
    if strcmp(errLog{i,2}, 'nonMatch')
        nonI= errLog{i,1};
        nonmatch(nonI)= 1;    
    end
end

figure, gscatter(bestRes_i, bestRes_p, nonmatch, 'gr');
nonmatchCell= num2cell(nonmatch);

strMat= repmat('fine', 633,1);
strMat=strMat(~nm);
@(x) sprintf()

%   %non match: .43% are non matching
    %  
nm=logical(nonmatch);    

ones= 
ones= bestRes_p(bestRes_p(nm)==1);   %479 ones

nonMatching= bestRes_p(nm);
nonMatching_one= nonMatching(nonMatching==1);

%a couple summary notes
    %479 ones
    %278 non matching
    %175 on & nonmatching
    %p(nonmatch)= .43
    %p(nonmatch|one)= .3653

%%
%back to looking at specs

aves= J3out(:,3);
aves= cell2mat(aves);
[vec,ix]=sort(aves, 'descend');
pctI= ix';

colorBlock2= unsorted_k3SpecMat(:,pctI);

figure, imagesc(unsorted_k3SpecMat);

%plot the top scores
good= colorBlock2(:,1:8);
figure, imagesc(good);
set(gca, 'XTickLabel',{ixGood})


medianScore=median(good,2);

%get position of bad segments
badIs=find(medianScore<.2);


