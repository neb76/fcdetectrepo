%run validate
% for k=[0 1 3]
%     eval(sprintf(';',k));
% end

sched= findResource('scheduler', 'configuration', 'atlasscheduler' );
% ClusterInfo.setUserDefinedOptions('-l nodes=3:ppn=1')
ClusterInfo.setProcsPerNode(1);
clustJ= createJob(sched);
set(clustJ,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})
%%

highSig= .05;
lowSig= .01;
step= .005;
sigInfo= {highSig, lowSig, step};
specN= 633;

%in order to test both the filter settings, some irritating reordering
s=3;
for s= [0 1]
    argCell= makeArgCell(sigInfo{:}, 6, 1, s);
    argCell=reshape(argCell, size(argCell,2),1);
    for e=1:size(argCell,1)
        curArg=argCell{e,1};
        curArg{1,6}=s;
        argCell{e,1}=curArg;
    end
        createTask(clustJ, @computeDistrJob_val, 8, argCell)  ;
end

submit(clustJ);

%now called job89
jobOut=job89.getAllOutputArguments;

J0out=jobOut(1:7,:);
J1out=jobOut(10:16,:);
%now take a look at the less filtered version here, especially looking to
%see

%build, for each job setting, nicely labeled set
%   k[s]out= [socre num  name sig trans norm
%this labeling is used for easy comparrison to the old results, with strong
%filter
for s= [0 1 3]
    eval(sprintf('k%d_labeled=parseOut(J%dout);', s, s))
end


%scatter plot for each
for k=[0 1]
    eval(sprintf('figure, plotSingleScat(k%d_labeled);', k));
    tit= sprintf('%d clusters', k);
    eval('title(tit)');
end
%%

%%scatter all together
    for k=[0 1 3]
        eval(sprintf('k%d_labeled(:,7)={%d};', k, k));
    end
%bring in ICC
for k=[0 1 3]
    eval(sprintf('aveICC=cleanICC(J%dout);', k));
    aveICC= num2cell(aveICC);
    eval(sprintf('k%d_labeled(:,8)=aveICC;', k));
end

masterlist_labeled= vertcat(k0_labeled, k1_labeled, k3_labeled);

    %control for normalization and trans
    normCases= masterlist_labeled(:,6);
    indeces= cell2mat(normCases);
    indeces=indeces==2;
    masterlist_labeled= masterlist_labeled(indeces,:);

    transCases= masterlist_labeled(:,5);
    indeces= cell2mat(transCases);
    indeces=indeces==6;
    masterlist_labeled= masterlist_labeled(indeces,:);

    X= cell2mat(masterlist_labeled(:,4));
    pctY= cell2mat(masterlist_labeled(:,1));
    clustSize= cell2mat(masterlist_labeled(:,7));

    gscatter(X,pctY,{clustSize }, 'gbr', 'osh');

%%


  iccY= cell2mat(masterlist_labeled(:,8));
  gscatter(X,iccY,{clustSize }, 'gbr', 'osh');
  
  gscatter(pctY,iccY,{clustSize }, 'gbr', 'osh');
  figure, gscatter(iccY,pctY,{clustSize }, 'gbr', 'osh');

%build specmats  
for k=[0 1 3]
    eval(sprintf('unsorted_k%dSpecMat=getSpecPerfMat(J%dout,2);',k,k));
end
  
%take a look at specmats
for k=[0 1 3]
    eval(sprintf('figure, imagesc(unsorted_k%dSpecMat);',k));
end


% sort specmats


for k=[0 1 3]

    eval(sprintf('aveICC=J%dout(:,3)',k));
    aveICC= cell2mat(aveICC);
    [vec,ix]=sort(aveICC, 'descend');
    pctI= ix';
    eval(sprintf('sorted_k%dSpecMat=unsorted_k%dSpecMat(:,pctI);',k,k));
    eval(sprintf('sorted_k%dSpecMat=sorted_k%dSpecMat(:,1:7);',k,k)); 
end

figure
spI=1;
for k=[0 1 3]
    eval(sprintf('subplot(1,3,%d), imagesc(sorted_k%dSpecMat);',spI, k));
    tit= ['filt:  ' num2str(k)];
    title(tit);
    spI=spI+1;
end

% for k=[0 1 3]
%     eval(sprintf(';',k));
% end