%
% %overnight script
% %run validate
% k=2;
% %%
% sched= findResource('scheduler', 'configuration', 'atlasscheduler' );
% J1_7_25= createJob(sched);
% J2_7_25= createJob(sched);
% set(J1_7_25,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})
% set(J2_7_25,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})
% 
% highSig= .05;
% lowSig= .015;
% step= .005;
% sigInfo= {highSig, lowSig, step};
% specN= 633;
% 
% jobI= 1;
% for trans= [1 3 6]
%     for norm= [1 2]
%         argCell= makeArgCell(sigInfo{:}, trans, norm, k);
%         if (jobI<3)
%             createTask(J1_7_25, @computeDistrJob_val, 7, argCell )  ;
%         else
%             createTask(J2_7_25, @computeDistrJob_val, 7, argCell )  ;            
%         end       
%         jobI= jobI + 1;
%     end
% end
%     
% submit(J1_7_25);
% wait(J1_7_25);
% submit(J2_7_25); %submitted at 12:28
% %both succeeded

%%
%test first
% testNewICC2= createJob(sched);
% set(testNewICC2,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})
% createTask(testNewICC2, @computeDistrJob_val, 8, {'testing new ICC', .03, 6, 2, 3});
% submit(testNewICC2);
%###succsess!

clear all
k=3;
%%
sched= findResource('scheduler', 'configuration', 'atlasscheduler' );
% J3_7_25= createJob(sched);
J4b_7_25= createJob(sched);
% set(J3_7_25,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})
set(J4b_7_25,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})

highSig= .05;
lowSig= .015;
step= .005;
sigInfo= {highSig, lowSig, step};
specN= 633;

jobI= 1;
for trans= [1 3 6]
    for norm= [1 2]
        argCell= makeArgCell(sigInfo{:}, trans, norm, k);
        if (jobI<4)
        else
            createTask(J4b_7_25, @computeDistrJob_val, 8, argCell )  ;            
        end       
        jobI= jobI + 1;
    end
end
%     
% submit(J3_7_25);
% wait(J3_7_25);
submit(J4b_7_25); %submitted at 12:28
%both succeeded

%%
k= 4;
J_3clustAgainA= createJob(sched);
J_3clustAgainB= createJob(sched);

set(J_3clustAgainA,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})
set(J_3clustAgainB,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})


highSig= .05;
lowSig= .020;
step= .005;
sigInfo= {highSig, lowSig, step};
specN= 633;

jobI= 1;
for trans= [1 3 6]
    for norm= [1 2]
        argCell= makeArgCell(sigInfo{:}, trans, norm, k);
        if (jobI<4)
            createTask(J_3clustAgainA, @computeDistrJob_val, 8, argCell )  ;
        else
            createTask(J_3clustAgainB, @computeDistrJob_val, 8, argCell )  ;            
        end       
        jobI= jobI + 1;
    end
end

highSig= .015;
lowSig= .014;
step= .005;
sigInfo2= {highSig, lowSig, step};
specN= 633;


for trans= [3 6]
    for norm= [1 2]
        argCell= makeArgCell(sigInfo2{:}, trans, norm, k);        
        createTask(J_3clustAgainB, @computeDistrJob_val, 8, argCell );
    end
end

submit(J_3clustAgainA)  %submitted at 7 pm on 7/25

submit(J_3clustAgainB)

%%

%%
k= 2;
J_3clustAgainA= createJob(sched);
J_3clustAgainB= createJob(sched);

set(J_3clustAgainA,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})
set(J_3clustAgainB,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})


highSig= .05;
lowSig= .020;
step= .005;
sigInfo= {highSig, lowSig, step};
specN= 633;

jobI= 1;
for trans= [1 3 6]
    for norm= [1 2]
        argCell= makeArgCell(sigInfo{:}, trans, norm, k);
        if (jobI<4)
            createTask(J_3clustAgainA, @computeDistrJob_val, 8, argCell )  ;
        else
            createTask(J_3clustAgainB, @computeDistrJob_val, 8, argCell )  ;            
        end       
        jobI= jobI + 1;
    end
end

highSig= .015;
lowSig= .014;
step= .005;
sigInfo2= {highSig, lowSig, step};
specN= 633;


for trans= [3 6]
    for norm= [1 2]
        argCell= makeArgCell(sigInfo2{:}, trans, norm, k);        
        createTask(J_3clustAgainB, @computeDistrJob_val, 8, argCell );
    end
end

submit(J_3clustAgainA);  %submitted at 10:27 am on 7/26

submit(J_3clustAgainB);

%%
%resubmission of k=3, pt.1 due to bad icc metric

%%
k= 3;
J_3clustAgainA= createJob(sched);
J_3clustAgainB= createJob(sched);

set(J_3clustAgainA,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})
set(J_3clustAgainB,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})


highSig= .05;
lowSig= .020;
step= .005;
sigInfo= {highSig, lowSig, step};
specN= 633;

jobI= 1;
for trans= [1 3 6]
    for norm= [1 2]
        argCell= makeArgCell(sigInfo{:}, trans, norm, k);
        if (jobI<4)
            createTask(J_3clustAgainA, @computeDistrJob_val, 8, argCell )  ;
        else
            createTask(J_3clustAgainB, @computeDistrJob_val, 8, argCell )  ;            
        end       
        jobI= jobI + 1;
    end
end

highSig= .015;
lowSig= .014;
step= .005;
sigInfo2= {highSig, lowSig, step};
specN= 633;


for trans= [3 6]
    for norm= [1 2]
        argCell= makeArgCell(sigInfo2{:}, trans, norm, k);        
        createTask(J_3clustAgainB, @computeDistrJob_val, 8, argCell );
    end
end

submit(J_3clustAgainA);          %resubmitted at 12:02 pm on 7/27


submit(J_3clustAgainB);



