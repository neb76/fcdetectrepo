%case 1 means on original spectrogram, others, usually 2, mean box a
%segmented

function[boxedSpec]=boxTheSpec_hard(spec, segMatRef, specCase)
%hard coded values that need customization if settings shift:
k=3; 
specTime=.4; %this value is set in generating spectrograms actually
specSize= 53; %right now hardCoded, due to size of specs generated

if specCase== 1
    boxedSpec=processpic(spec,1,0);
    boxColor= max(max(boxedSpec));
    maxHeight= floor(size(spec,1)*.81);
else
    boxedSpec=spec;
    boxColor=k+1;
    maxHeight=size(spec,1);
end

%time dimensions
start=segMatRef{1,2};
stop=segMatRef{1,3};
segRange=stop-start;
shift= (.5*segRange)*specSize/specTime; %amount of time 'blocks' in segment that equal the shift from center to start and stop
shift= ceil(shift);
midI= floor(specSize/2)
startI= midI-shift
finI= midI + shift

%freqDimensions
blockSize= 12000/((1024+1)/2);
flowI= 512-round(segMatRef{1,4}/blockSize);
fhighI= 512-round(segMatRef{1,5}/blockSize);

if (flowI>maxHeight);
    flowI=maxHeight;
end


%make stripes
boxedSpec(fhighI:flowI, startI)=boxColor; %left
boxedSpec(fhighI:flowI, finI)=boxColor; %right
boxedSpec(fhighI, startI:finI)=boxColor; %top
boxedSpec(flowI, startI:finI)=boxColor; %bottom


end