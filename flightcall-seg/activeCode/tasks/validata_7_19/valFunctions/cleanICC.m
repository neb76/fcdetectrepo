function [ cleanICCvec ] = cleanICC( jobOut )

jobN= size(jobOut, 1);
cleanICCvec=ones(jobN,1);
for j=1:jobN
%     col= jobOut{j,4};
%     col=col';
%     iccMat(:,j)=col;
    col= jobOut{j,4};
    colI= col>1;
    cleanCol= col(~colI);
    aveICC= mean(cleanCol);
    cleanICCvec(j,1)=aveICC;
end

end

