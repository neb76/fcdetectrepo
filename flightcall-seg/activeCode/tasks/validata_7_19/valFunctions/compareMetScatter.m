function [  ] = compareMetScatter( out_labeled, expCase )

    X= cell2mat(out_labeled(:,4));
    Y= cell2mat(out_labeled(:,8));
    normCases= out_labeled(:,6);
    transCases= out_labeled(:,5);

    if expCase==2
        K= cell2mat(out_labeled(:,7));
        Y= bsxfun(@rdivide, Y, K);
    end
    
    if expCase==3
        sigLabs= arrayfun(@num2str, X, 'UniformOutput', false);
        X= cell2mat(out_labeled(:,1));
    end
    
    
    for i= 1:size(out_labeled,1)
        curN=normCases{i,1};
        curT=transCases{i,1};
        if curN==1
            normCases{i,1}= 'rw'
        else
            normCases{i,1}= 'sym'
        end
        
        if curT==1
            transCases{i,1}= 'noTrans';
        elseif curT==3
            transCases{i,1}= 'sq';
        else
            transCases{i,1}= 'cub';
        end
    end
    gscatter(X,Y,{normCases transCases}, 'bgrbgr', 'ooohhh'); %for use without labeling
    
    if expCase==3
        dx= .01;
        text(X+dx, Y, sigLabs);
    end
    
end

