%returns, for each full jobOut cell, a 633 x number-jobs of results
%cases-- caseN==
%(both of first use % in box metric)
% 1:attempt to sort here, where I found to not work actually...
% 2: no sorting

%now ICC metric
% 3: ICC metric, no sorting

function[specPMat]=getSpecPerfMat(jobOut, caseN)
jobN=size(jobOut,1);
specPMat= ones(633,jobN);

if caseN==1
    aves=jobOut(:,3);
    aves=cell2mat(aves);
    [~,IX]= sort(aves,'descend');
    IX=IX';
    for j= IX;
        col=jobOut{j,2};
        col=col';
        specPMat(:,j)= col;
    end
elseif caseN==3
    cond= @(x) (x<1);
    clean= @(x) x*cond(x);
    for j=1:jobN;
        col=jobOut{j,4};
        %splice point
        class(col)
        
        col= arrayfun(clean, col);
        %cellfun
        
        %splice point
        col=col';
        specPMat(:,j)= col;
    end
else
    for j=1:jobN;
        col=jobOut{j,2};
        col=col';
        specPMat(:,j)= col;
    end
end

end