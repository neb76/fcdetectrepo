%used for setting up Tasks for cluster testing
function [argCell]= makeArgCell(highSig, lowSig, step, trans, norm, k)
jobN= floor((highSig-lowSig)/step) +1 ;
argCell= cell(1, jobN);

sg=highSig;
jobTemp= {'', 0, trans, norm, k};

for c= 1:jobN
    jobTemp{1,2}= sg;
    argCell{1, c}= jobTemp;
    sg= sg-step;
end


end
