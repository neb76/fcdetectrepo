sched= findResource('scheduler', 'configuration', 'atlasscheduler' );
renderJ3= createJob(sched);
set(renderJ3,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})

num= 1;
smplI= randperm(size(specMat, 1));
smplI=smplI(1:num);


%randomly sampled 60 specs
smallSpec= specMat(smplI, :);

for i= 1:size(smallSpec,1)
    spec= smallSpec{i,1};
    createTask(renderJ3, @computeDistrJob_render, 3, {'', .025, 6, 2, spec,3} );
end


submit(renderJ3)
wait(renderJ3)


for s= 1:size(jobOut, 1)
    curSeg= jobOut{s,1};
%     if curSeg ~=1
     figure, imagesc(curSeg);
%         seed= jobOut{s, 2};
%        
%         tit= ['seg_num ' num2str(segMatRef{s,1})];
%         title(figTit);
% %     end
end