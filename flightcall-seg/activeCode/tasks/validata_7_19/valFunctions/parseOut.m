function [scores_labeled]= parseOut(jobOut)

scores= jobOut(:, 3);
names= cell(size(jobOut, 1),5);  %format:   jobNumb, name, sig, trans, norm
badI=[];
for i= 1:size(jobOut, 1)
    curSeed= jobOut{i,1};
    if iscell(curSeed)
        jobN= ['sig_' num2str(curSeed{1,2}) ' transF_' num2str(curSeed{1,3}) ' norm_ ' num2str(curSeed{1,4}) ' jobnum_' num2str(i) ];
        curName= {i, jobN, curSeed{1,2:4} };
        names(i,:)= curName;
    else
        badI(end+1)= i;
    end
end

names(badI,:)=[];
scores(badI,:)=[];

scores_labeled= horzcat(scores, names);
end
