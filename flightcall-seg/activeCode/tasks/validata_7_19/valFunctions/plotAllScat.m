function[]= plotAllScat(out_labeled)
%to reudce points, use jut one normalization case (they seem mighty close
%at this scale)
% % this is code for that:
normCases= out_labeled(:,6);
% indeces= cell2mat(normCases);
% indeces=indeces==1;
% out_labeled= out_labeled(indeces,:);

X= cell2mat(out_labeled(:,4));
Y= cell2mat(out_labeled(:,1));
transCases= out_labeled(:,5);
clustSize= cell2mat(out_labeled(:,7));

for i= 1:size(out_labeled,1)
    curN=normCases{i,1};
    curT=transCases{i,1};
    if curN==1
        normCases{i,1}= 'rw';
    else
        normCases{i,1}= 'sym';
    end
    
    if curT==1
        transCases{i,1}= 'noTrans';
    elseif curT==3
        transCases{i,1}= 'sq';
    else
        transCases{i,1}= 'cub';
    end
end
% gscatter(X,Y,{normCases transCases clustSize}, 'bgrbgr', 'ooohhh'); %for use without labeling
sizeVec= [5 10 15];
sizeVec=repmat(sizeVec, 1,3);
% gscatter(X,Y,{transCases clustSize }, 'bgrbgrbgr', 'ooossshhh', sizeVec);
gscatter(X,Y,{transCases clustSize }, 'gggbbbrrr', 'ooossshhh', sizeVec);

end