function[]= plotSingleScat(out_labeled)

X= cell2mat(out_labeled(:,4));
Y= cell2mat(out_labeled(:,1));
normCases= out_labeled(:,6);
transCases= out_labeled(:,5);

for i= 1:size(out_labeled,1)
    curN=normCases{i,1};
    curT=transCases{i,1};
    if curN==1
        normCases{i,1}= 'rw'
    else
        normCases{i,1}= 'sym'
    end
    
    if curT==1
        transCases{i,1}= 'noTrans';
    elseif curT==3
        transCases{i,1}= 'sq';
    else
        transCases{i,1}= 'cub';
    end
end
gscatter(X,Y,{normCases transCases}, 'bgrbgr', 'ooohhh'); %for use without labeling

end