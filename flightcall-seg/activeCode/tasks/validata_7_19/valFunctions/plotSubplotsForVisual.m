
%with outputs, plot a subplot with some nice stats
for s= 1:size(jobOut, 1)
    curSeg= jobOut{s,1};
    segRow=segMatRef(specI,:);
    boxedSeg= boxTheSpec(curSeg,segRow,2);
    specI=smplI(s);
    
    spec=specMat{specI,1};
    boxedSpec=boxTheSpec(spec, segRow,1);
    figure;

    a=subplot(1,2,1), imagesc(boxedSpec);  %subplot, with the left photo being non filtered trim
    colorbar;
    %box: # to # seconds, frequencies  
    boxStr= [segMatRef{specI,6} '  freq range: ' num2str(segMatRef{specI,4}) ' hz to ' num2str(segMatRef{specI,5}) ' hz'];
    text(1,450,boxStr);
    
    b=subplot(1,2,2), imagesc(boxedSeg);
    colorbar;
    %title- the %numth spec at %time and boxSeg is %boxInd
    time=floor(segMatRef{specI,2});
    tit=['the ' num2str(specI) 'th seg at time ' num2str(time) ' secs, box index is ' num2str(jobOut{s,2})];
    title(a,tit); 
    
    %score info
    scoreStr=['top %: ' num2str(jobOut{s,3}) '     ICC is: ' num2str(jobOut{s,4})];
    text(1,450,scoreStr);    
end