%takes spec, trims off the part filtered by low pass filter,
%also makes min value 0, so all positive amplitude values
function [ trimp ] = processpic_debug( p, filt, h, sig)
sharpBool= false;

% trimp=p(1:208, :); %for original, 512 DFT
newH= size(p, 1);
newH= newH*.81; %from inspection
trimp=p(1:newH, :);
tit='processedpic';
%lh is size
%median

if sharpBool
    HAve= fspecial('average', [5 5]);
    blur=imfilter(trimp, HAve, 'replicate');
    detail= bsxfun(@minus, trimp, blur);
    trimp= trimp - detail/2;
end

if filt==1
    trimp=medfilt2(trimp, h, 'symmetric');
    tit=['median filter with neigh size: ' num2str(h)];
end

%laplacian
%alpha in range from 0 to 1, default is .2
if filt==2
    trimp=wiener2(trimp, h);
    tit=['winer filter with neigh size: ' num2str(h)];
    
end

if filt==3
    HAve= fspecial('average', h);
    trimp=imfilter(trimp, HAve, 'replicate');
    tit=['avg, neigh: '  num2str(h)];
end



if filt==4
    HAve= fspecial('gaussian', h, sig)
    trimp=imfilter(trimp, HAve, 'replicate');
    tit=['gaus, neigh: ' num2str(h) ' sig: ' num2str(sig)];
end


if filt==5
    trimp=medfilt2(trimp, [8 2], 'symmetric');
    HAve= fspecial('gaussian', h, .5)
    trimp=imfilter(trimp, HAve, 'replicate');
    %     tit=['med+gaus, neigh: ' num2str(h) ' sig: ' num2str(sig)];
    tit=['med+gaus, neigh: ' num2str(h) ' sig: ' num2str(.5)];
    
end

if filt==6
    HAve= fspecial('gaussian', h, .5)
    trimp=imfilter(trimp, HAve, 'replicate');
    trimp=medfilt2(trimp, [8 2], 'symmetric');
    %     tit=['gause+med, neigh: ' num2str(h) ' sig: ' num2str(sig)];
    tit=['gause+med, neigh: ' num2str(h) ' sig: ' num2str(.5)];
    
end


if filt==7
    HAve= fspecial('gaussian', [10 2], .05)
    trimp=imfilter(trimp, HAve, 'replicate');
    trimp=medfilt2(trimp, h, 'symmetric');
    trimp=imfilter(trimp, HAve, 'replicate');
    tit=['GMG, neigh: ' num2str(h(1)) ' sig: ' num2str(sig)];
end

if filt==8
    HAve= fspecial('average', h)
    trimp=imfilter(trimp, HAve, 'replicate');
    trimp=medfilt2(trimp, [8 2], 'symmetric');
    tit=['ave+med, aveneigh: [8 2],  med-neigh: ' num2str(h)];
    
end


if filt==9
    trimp=medfilt2(trimp, [8 2], 'symmetric');
    HAve= fspecial('average', h);
    trimp=imfilter(trimp, HAve, 'replicate');
    tit=['med+ave, med-neigh: ' num2str(h) ',  aveneigh: [8 2]'  ];
    
end

%square closing
if filt==10
    seSq3=strel('square',h);
    trimp=imclose(trimp, seSq3);
    tit= ['closing, square neigh:' num2str(h)];
end

%square opening
if filt==11
    seSq3=strel('square',h);
    trimp=imopen(trimp, seSq3);
    tit= ['opening, square neigh:' num2str(h)];
    
end


if filt==10
    seSq3=strel('square',h);
end

%both are very poor
% if filt==5
%     HAve= fspecial('prewitt')
%     trimp=imfilter(trimp, HAve, 'replicate')
% end
%
% if filt==6
%     HAve= fspecial('sobel')
%     trimp=imfilter(trimp, HAve, 'replicate')
% end


oldmin= min(min(trimp));%imshow(trimp, [], 'XData', [1 256], 'YData', [1 175])
if oldmin<0
    trimp= trimp-oldmin;
end

figure
imagesc(trimp);
if sharpBool
    tit=[tit '-shrp'];
end
title(tit)
end

