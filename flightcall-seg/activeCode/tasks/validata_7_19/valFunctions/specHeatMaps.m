function[]= specHeatMaps(jobOut)

pcnt_specPerf= getSpecPerfMat(jobOut, 2);
icc_specPerf= getSpecPerfMat(jobOut, 3);



%success with sorting
aves= jobOut(:,3);
aves= cell2mat(aves);
[vec,ix]=sort(aves, 'descend');
pctI= ix';

iccaves= jobOut(:,5);
iccaves= cell2mat(iccaves);
[vec,ix]=sort(iccaves, 'descend');
iccI= ix';

%colorBlocks:
%                    display metric
%                  pcnt  icc
% sort      pcnt   1      2
% metric    icc    3      4

colorBlock1= pcnt_specPerf(:,pctI);
colorBlock2=icc_specPerf(:,pctI);
colorBlock3= pcnt_specPerf(:,iccI);
colorBlock4=icc_specPerf(:,iccI);

% for c=1:4
%     figure
%     tit= ['color block ' num2str(c)];
%     eval(sprintf('imagesc(colorBlock%d), title(tit);',c));
% end
% figure
for c=1:4
    subplot(2,2,c);
    eval(sprintf('imagesc(colorBlock%d)',c));
end



end
