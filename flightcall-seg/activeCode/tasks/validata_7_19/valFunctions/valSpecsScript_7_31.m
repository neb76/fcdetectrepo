%switching to visual evaluation
csv_name='C:\Users\neb76\Desktop\nick_FCDetect_beforGit\DANBY_20090912_194900_NFC_637.csv'
[num,txt,raw]= xlsread(csv_name);


sched= findResource('scheduler', 'configuration', 'atlasscheduler' );
baseCase3= createJob(sched);
set(baseCase3,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript\'})


jobSpecs= {'',.025, 6, 2, 3};

num= 60;
smplI= randperm(size(specMat, 1));
smplI=smplI(1:num); %###important for indexing! be sure to keep around!


%randomly sampled 60 specs
smallSpec= specMat(smplI, :);

for i= 1:size(smallSpec,1)
    spec= smallSpec{i,1};
    specI=smplI(i);
    stats=segMatRef(specI,:);
    createTask(baseCase3, @computeDistrJob_render, 6, {jobSpecs{:}, spec, stats} );
end


submit(baseCase3)
wait(baseCase3)



%with outputs, plot a subplot with some nice stats
for s= 1:size(jobOut, 1)
    curSeg= jobOut{s,1};
    segRow=segMatRef(specI,:);
    boxedSeg= boxTheSpec(curSeg,segRow,2);
    specI=smplI(s);
    
    spec=specMat{specI,1};
    boxedSpec=boxTheSpec(spec, segRow,1);
    figure;

    a=subplot(1,2,1), imagesc(boxedSpec);  %subplot, with the left photo being non filtered trim
    colorbar;
    %box: # to # seconds, frequencies  
    boxStr= [segMatRef{specI,6} '  freq range: ' num2str(segMatRef{specI,4}) ' hz to ' num2str(segMatRef{specI,5}) ' hz'];
    text(1,450,boxStr);
    
    b=subplot(1,2,2), imagesc(boxedSeg);
    colorbar;
    %title- the %numth spec at %time and boxSeg is %boxInd
    time=floor(segMatRef{specI,2});
    tit=['the ' num2str(specI) 'th seg at time ' num2str(time) ' secs, box index is ' num2str(jobOut{s,2})];
    title(a,tit); 
    
    %score info
    scoreStr=['top %: ' num2str(jobOut{s,3}) '     ICC is: ' num2str(jobOut{s,4})];
    text(1,450,scoreStr);    
end