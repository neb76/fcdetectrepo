highSig= .10;
lowSig= .02;
step= .005;
len= 3

tasksSent= 192;
settingNum=tasksSent/len;
resultMat= ones(len, settingNum);

outI=1;
for s= 1:settingNum
    for spN= 1:len
        curJob= jobOut(outI,:);
        resultMat(spN,settingNum)= curJob{1, 2};
        outI= outI +1;
    end
end

settingAv= mean(resultMat,2);
specAv= mean(resultMat,1);

resultMat_av= vertcat(resultMat, specAv);
settingAv(end+1)=0;
resultMat_av= horzcat(resultMat_av, settingAv);

specName= segMat(:,2);
settingName= cell(1,settingNum);


snI=1;

sg=1;
while sg>=lowSig
    settingName{1,snI}= ['rwSQ with sig_' sg];
    snI=snI +1;
    sg= sg-step;
end

sg=1;
while sg>=lowSig
    settingName{1,snI}= ['symSQ with sig_' sg];
    snI=snI +1;
    sg= sg-step;
end

sg=1;
while sg>=lowSig
    settingName{1,snI}= ['rwCU with sig_' sg];
    snI=snI +1;
    sg= sg-step;
end

sg=1;
while sg>=lowSig
    settingName{1,snI}= ['symCU with sig_' sg];
    snI=snI +1;
    sg= sg-step;
end
 
%paste names into one grand cell
resultCell= cell(size(resultMat, 1)+2, size(resultMat, 2) +2);
specName= vertcat([],specName);
specName{end+1}= 'settingAve';
resultCell(:,1)=specName;

settingName=horzcat([], settingName);
settingName{end+1}= 'specAve';
resultCell(end,:)=settingName;


