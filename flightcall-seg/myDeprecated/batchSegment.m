% Ng, A., Jordan, M., and Weiss, Y. (2002). On spectral clustering: analysis and an algorithm. In T. Dietterich,
% S. Becker, and Z. Ghahramani (Eds.), Advances in Neural Information Processing Systems 14 
% (pp. 849 � 856). MIT Press.

% CONCEPT: Introduced the normalization process of affinity matrix(D-1/2 A D-1/2), 
% eigenvectors orthonormal conversion and clustering by kmeans 

%affTech:
%     -3, global sigma
%     -4, self tuned, local sigma.  Sigma param is now irrelevent
%     

%filtset
%         right now this is very wonky, set up to assume all will be disk filtered
            %otherwise, specify the number corresponding to "processpic"
                %setting
            %no way to add the "sharpening" for now
            

function [lab_pic]= batchSegment(spec, sigma, filtSet, affTech, evNum, clustNum)
data= processpic( spec, filtSet, 1 );
% calculate affinity and use to build normalized Laplacian matrix
affinity = newAff_vec(data, affTech, sigma, 1);
dimF= size(data, 1)-2;
dimT= size(data, 2)-2;
NL1= getLap(affinity);

% perform kmeans clustering on the matrix U, norm-1 matrix of the k
% eigenvectors corresponding to the k smallest eigenvalues
[U,~] = eigs(NL1, evNum, 'SM');
U= normr(U);
U= abs(U);
[IDX,C] = kmeans(U,clustNum, 'replicates', 50); 

lab_pic= reshape(IDX, dimF, dimT);
figure

imagesc(lab_pic)
end

