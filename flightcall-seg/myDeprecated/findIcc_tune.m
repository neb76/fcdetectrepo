function [ icc ] = findIcc_tune( idx, orig, k )
dimF= size(orig, 1);
dimT= size(orig, 2);
orig= orig(2:(dimF-1), 2: (dimT-1) );  
% orig= orig(safe_spots);

allvec= reshape(orig, (dimT-2)*(dimF-2), 1);
totVar= var(allvec);

clustVTot= 0;
for i= 1:k
    clustI= idx== k;
    clustVal= orig(clustI);
    clustVar= var(clustVal);
    clustVtot= clustVTot + clustVar;
end
pr = totVar
pr = clustVTot
% icc= totVar^2/((clustVtot)^2 + totVar^2);
icc= clustVTot^2 ;

end

