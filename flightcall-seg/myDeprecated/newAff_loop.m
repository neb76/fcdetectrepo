function [ aff ] = newAff_loop( spec )
%# big notes
    %- have not yet implemented how to to normalize with 2 ?-sqd
    % - for now, just doing amplitude diff
        %have droped encoding time/frequency explicitly
        %might later recquire weighting

    
%UNTITLED new implmentation of affinity calculation, now with sparse array
%   Detailed explanation goes here
% "(we use Euclidean distance within an 8-neighborhood, scaled by 2?2, where
% ? is the standard deviation of the neighborhood affinities)."

%sub2ind(size(p), 2, 3)

dimF= size(spec, 1);
dimT= size(spec, 2);
dimAff= dimF*dimT;

aff= sparse(dimAff, dimAff);

% use sub2ind(size(spec), #index#, #index#)  

for f= 1: dimF
    for t= 1:dimt                
        %all interior, standard cases. 4 edges (R, D, D-R, D-L)
          %on bottom, 1 edge(R)
          %on left side of spec, 3 edges (R, D, D-R)
          %on right side of spec, 2 edges (D, D-L)
          %bottom right point is skipped
          
          curI= sub2ind(size(p), f, t);  
          %right edge
                %skip if on right of spec
            if (t~=dimT) 
                ampDifR= abs(spec(f,t)-spec(f, t+1));                
                rI= sub2ind(size(p), f, t+1);
                aff(curI, rI)= ampDifR;
                aff(rI, curI)= ampDifR;
            end
           
            %down edge
                %skip if on bottom of spec
            if (f~= dimF) 
                ampDifD= abs(spec(f,t)-spec(f+1, t));
                dI= sub2ind(size(p), f+1, t);                
                aff(curI, dI)= ampDifD;
                aff(
                %down-right edge    
                    % ##skip if bottom or right
                if (t~=dimT) 
                  ampDifDR= abs(spec(f,t)-spec(f+1, t+1));
                  drI= sub2ind(size(p), f+1, t+1);                
                                    
                end
                
                %down-left edge
                    % ##skip if bottom or left
                if (t~=1) 
                    ampDifDL= abs(spec(f,t)-spec(f+1, t-1));
                    dlI= sub2ind(size(p), f+1, t-1);
                end              
            end                                
    end
    

end

aff= sparse(fV, tV, difV, dimAff, dimAff);

end

