 
%has nicholas mod

startT = 1259.79;
endT = 1259.89;

[p,f,t] = getspec('DANBY','20090912','194900',startT,endT);

n = 3;
n1 = 1;
% apply smoothening filters to image
tp = processpic(p,n,n1);
dimF= size(tp, 1);
dimT= size(tp, 2);

%compute exponential of euclidean distances for sparse affinity matrix
processCase = 2;
A= newAff_vec(tp, 3, .3);
L= getLap(A);
[eigV, eigVals] = eigs(L);

%%%%%%%%%%%%%%%%%%%%case 1: shi malik normalized cuts:

%plot second largest eigenvector:

figure,plot(eigV(:,2),'r*'),title('2nd Largest Eigenvector');

%Choose 2nd largest eigenvector
L=eigV(:,2);

%threshold by observation
thresh = .01;
L(L>thresh)=1;
L(L<=thresh)=0;

%segmented image
Im=reshape(L,dimF,dimT);

%show image
imagesc(Im);

%%%%%%%%%%%%%%%%%%%case 2: Jordan weiss -- normalized kmeans
k = 4;
for i =1:k
    tit= strcat(num2str(i), 'th Largest Eigenvector');
    figure,plot(eigV(:,i),'r*'),title(tit);
end

% select k largest eigen vectors

nEigVec = eigV(:,(size(eigV,2)-(k-1)): size(eigV,2));

% construct the normalized matrix U from the obtained eigen vectors
for i=1:size(nEigVec,2)
    n = sqrt(sum(nEigVec(:,i).^2)); 
    U(:,i) = nEigVec(:,i) ./ n;
end

% perform kmeans clustering on the matrix U
[IDX,C] = kmeans(U,2); 

% figure,
% hold on;

m = reshape(IDX,dimF,dimT);
% imagesc(m)


imshow(m,[], 'XData', [1 256], 'YData', [1 175]), title('image labeled by cluster index');
