
%% BEGIN SPECTRAL IMAGE SEGMENTATION:
% given a p_new.

%% SET PARAMETERS:
% .4<= sigma <= 1.8
% .04<= thresh <= .8
% Try sigma at 1, <1, and >1
% generally let 10*thresh ~= sigma in the interval, 
% as this has shown to return a manageable number 
% of nnzr values in the sparse A.

sig1 = 1; thresh1 = .1;

%Randomized within interval offsets
negDelt = .6*rand(1);
posDelt = .8*rand(1);

sig2 = 1-negDelt;
sig3 = 1-posDelt;

thresh2 = (sig2)/10;
thresh3 = (sig3)/10;

sigma = [sig1,sig2,sig3];
thresh = [thresh1,thresh2,thresh3];

%% SPARSE Lsym:

[L1,dimF(1),dimT(1)] = getAff(p_new,sigma(1),thresh(1),0);
[L2,dimF(2),dimT(2)] = getAff(p_new,sigma(2),thresh(2),0);
[L3,dimF(3),dimT(3)] = getAff(p_new,sigma(3),thresh(3),0);


%% FAST(SPARSE) EIGENDECOMPOSITION
[eigV1, eigVals1] = eigs(L1);
[eigV2, eigVals2] = eigs(L2);
[eigV3, eigVals3] = eigs(L3);

%% SM and JW SC Algorithms diverge here:

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% CASE 1: Shi - Malik

% KMEANS CLUSTERING on the 2nd smallest generalized eigenvector:
% Always set clusters = 2 in function call. Here, repetitions set to 10. 
clusters =2;
reps = 10;

[vsegSM1,Csm1] = kmeans(eigV1(:,2),clusters,'Replicates',reps); 
[vsegSM2,Csm2] = kmeans(eigV2(:,2),clusters,'Replicates',reps); 
[vsegSM3,Csm3] = kmeans(eigV3(:,2),clusters,'Replicates',reps); 

msegSM1 = reshape(vsegSM1,dimF(1),dimT(1));
msegSM2 = reshape(vsegSM2,dimF(2),dimT(2));
msegSM3 = reshape(vsegSM3,dimF(3),dimT(3));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% CASE 2: Jordan - Weiss

% CHOOSE number of grouped eigenvectors to study. 
% This determines the number of outerIterations.
%outI = ... -------------------------------- ... = Ituo%

% CHOOSE number of generalized eigenvectors to study:
n = 3;

% SELECT n LARGEST generalized eigenvectors:
nEigVec1 = eigV1(:,(size(eigV1,2)-(k-1)): size(eigV1,2));
nEigVec2 = eigV2(:,(size(eigV2,2)-(k-1)): size(eigV2,2));
nEigVec3 = eigV3(:,(size(eigV3,2)-(k-1)): size(eigV3,2));

for i=1:size(nEigVec1,2)
    s1 = sqrt(sum(nEigVec1(:,i).^2));
    s2 = sqrt(sum(nEigVec2(:,i).^2)); 
    s3 = sqrt(sum(nEigVec3(:,i).^2)); 
    
    U1(:,i) = nEigVec1(:,i) ./ s1;
    U2(:,i) = nEigVec2(:,i) ./ s2;
    U3(:,i) = nEigVec3(:,i) ./ s3;
end

%%KMEANS CLUSTERING ON EACH Ui:
%Again, use clusters = 2. reps = 10 (?)
[vsegJW1,Cjw1] = kmeans(U1,clusters,'Replicates',reps); 
[vsegJW2,Cjw2] = kmeans(U2,clusters,'Replicates',reps); 
[vsegJW3,Cjw3] = kmeans(U3,clusters,'Replicates',reps); 

msegJW1 = reshape(vsegJW1,dimF(1),dimT(1));
msegJW2 = reshape(vsegJW2,dimF(2),dimT(2));
msegJW3 = reshape(vsegJW3,dimF(3),dimT(3));

