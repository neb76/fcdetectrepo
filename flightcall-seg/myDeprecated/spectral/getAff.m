function [ A ,points,dimF,dimT] = getAff(p,sigma, thresh, casen)
%RETURN LAPLACIAN A

% Begin by gettting the affinity matrix

dimF= size(p, 1);
dimT= size(p, 2);
dbvec= reshape(p, dimT*dimF, 1); %vec of all points
fvectemp=1:dimF;
fvec= repmat(fvectemp',dimT,1);
tvecttemp= 1:dimT;
tmat= repmat(tvecttemp, dimF, 1);
tvec= reshape(tmat, dimT*dimF, 1);

% (dimF x dimT) X 3 matrix of points.
points= horzcat(dbvec, fvec, tvec);


%pixel by pixel affinity calculation
if casen==1
    distvec= exp(-pdist(points, 'euclidean')./(2*sigma^2));
elseif casen == 2
    distvec= -pdist(points, 'seuclidean');
else
    distvec= -pdist(points, 'euclidean');
end

max(distvec)

%threshold for sparse matrix
distV = find(distvec<thresh);
distvec(distV)= 0;

pr = 'done with pdist'
size(distvec)
%distvec
A= squareform(distvec); 


%sparse matrix -- using threshold.

A = sparse(A);
nnZS = nnz(A)

% Degree matrixx sumvec. 
sumvec = sum(A,2);
for i = 1: size(sumvec) 
    A(i,i) = -sumvec(i);
end


pr = 'got laplacian'

% compute the normalized laplacian

sqsumvec = sqrt(sumvec);
bsxfun(@rdivide,A, sqsumvec);
bsxfun(@rdivide,A, sqsumvec');

pr = 'normalized..'



end

