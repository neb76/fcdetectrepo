 function [l] = getComponents(lap)
% 
% % affinity = CalculateAffinity(data);
% %figure,imshow(lap,[]), title('Affinity Matrix');
% 
% %[eigVectors,~] = eig(lap);
 [l,~] = eigs(lap);
% % plot the eigen vector corresponding to the 2nd largest eigen value
  figure,plot(l(:,2),'r*'),title('2nd Largest Eigenvector');
% %secLgst= eigVectors(:,2);
% 
L=l(:,2);

thresh = .01;
L(L>thresh)=1;
L(L<=thresh)=0;

Im=reshape(L,df,dt);

imagesc(Im);

% 
% 
% 
 end
% 
% 
% % decreasing the window size can assisst for a more manageable 
%     % affinity computation/ and others following it. Furthermore, 
%     % the size of the desired cluster increases relative to the background
%     % this has a benefit. in windows containing a call,
%     % the call-cluster will recieve more weight. disadvantage: more
%     % windows to examine, possibly of false positives goes up(?) 
%     % as time resolution increases (gabor limit).  
%     % cluster size disparity is an issue for spectral clustering,
%    % the extent to which it is, i dont now. 57 times the size of the
%    % smallest cluster from xiang gong spectral clustering
%     %
% 
% % threshold the eigen vectors
% % thresh = .0;
% % [xx1,yy1,val1] = find(l(:,2) > thresh);
% % [xx3,yy3,val3] = find(l(:,2) < -thresh);    
% % 
% % [xx2,yy2,val2] = find(l(:,2) ==0);
% %     
% % 
% % L=l(:,2);
% % 
% % thresh = .08;
% % L(L>thresh)=1;
% % L(L<=thresh)=0;
% % 
% % Im=reshape(L,208,37);
% % 
% % imagesc(Im);
% % 
% % 
% % l(l(:,2)>thresh) = 
% % 
% % xval1 = mod(xx1,size(trimp,1));
% % xval2 = mod(xx2,size(trimp,1));
% % xval3 = mod(xx3,size(trimp,1));
% % 
% % yval1 = floor(xx1/size(trimp,1))+1;
% % yval2 = floor(xx2/size(trimp,1))+1;
% % yval3 = floor(xx3/size(trimp,1))+1;
% % 
% % figure,
% % hold on;
% % plot(yval1,xval1,'m*');
% % plot(yval2,xval2,'b*');
% % plot(yval3,xval3,'g*');
% % hold off;
% % title('Clustering Results using 2nd Generalized Eigen Vector');
% % grid on;shg
% % 
% % 
% % 
% flen= 208;
% tlen = 39;
% 
% 
% % select k largest eigen vectors
% k = 3;
% nEigVec = m(:,(size(m,1)-(k-1)): size(m,1));
% 
% % construct the normalized matrix U from the obtained eigen vectors
% for i=1:size(nEigVec,1)
%     n = sqrt(sum(nEigVec(i,:).^2));    
%     U(i,:) = nEigVec(i,:) ./ n; 
% end
% 
% % perform kmeans clustering on the matrix U
% [IDX,C] = kmeans(U,3); 
% 
% % plot the eigen vector corresponding to the largest eigen value
% %figure,plot(IDX)
% figure,
% hold on;
% 
% m = reshape(IDX,flen,tlen);
% imagesc(Im);
% 
% % for i=1:size(IDX,1)
% %     if IDX(i,1) == 1
% %         plot(data(i,1),data(i,2),'m+');
% %     elseif IDX(i,1) == 2
% %         plot(data(i,1),data(i,2),'g+');
% %     else
% %         plot(data(i,1),data(i,2),'b+');        
% %     end
% % end
% % hold off;
% % title('Clustering Results using K-means');
% % grid on;shg
% 
% 
flen= 208;
tlen = 39;

% select k largest eigen vectors
k = 3;
nEigVec = m(:,(size(m,2)-(k-1)): size(m,2));

% construct the normalized matrix U from the obtained eigen vectors
for i=1:size(nEigVec,2)
    n = sqrt(sum(nEigVec(:,i).^2)); 
    U(:,i) = nEigVec(:,i) ./ n;
end
% perform kmeans clustering on the matrix U
[IDX,C] = kmeans(U,3); 
% plot the eigen vector corresponding to the largest eigen value
%figure,plot(IDX)
C
figure,
hold on;

m = reshape(IDX,flen,tlen);
size(m)
imagesc(m)
