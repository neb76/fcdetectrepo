function [ icc ] = thetaTune( theta, spec )
evNum = 4;
clustNum=3;
dimF= size(spec, 1)-2;
dimT= size(spec, 2)-2;

% calculate affinity matrix
affinity = newAff_vec(spec, 3, theta);
NL1= getLap(affinity);

[U,~] = eigs(NL1, evNum, 'sr' );
U= normr(U);

% perform kmeans clustering on the matrix U
[IDX,~] = kmeans(U,clustNum, 'replicates', 50); 

icc= findIcc_tune(IDX, spec, clustNum);
lab_pic= reshape(IDX, dimF, dimT);
figure
imagesc(lab_pic)

tit= strcat('seg when theta= ', num2str(theta), '        eig-Vectors = ', num2str(evNum), '         cluster numer = ', num2str(clustNum) );
title(tit)

end

