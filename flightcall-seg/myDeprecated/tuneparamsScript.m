clear all;
close all;
start= 7020.449;
stop= 7020.580;
[p_new,f,t]= getspec('DANBY','20090912','194900',start,stop);
data= processpic( p_new, 3, 1 );

 
dimF= size(data, 1)-2;
dimT= size(data, 2)-2;
% calculate affinity matrix
%theta params
high=.5;
low=.045;
iterations= 20;

%pre allocate
stepsize= (high-low)/iterations;
iccVals= zeros(2,iterations);

curTheta= low
for i = 1:iterations
    iccVals(1, i) = curTheta;
    iccVals(2,i)= thetaTune(curTheta, data);
    curTheta= curTheta + stepsize;
end