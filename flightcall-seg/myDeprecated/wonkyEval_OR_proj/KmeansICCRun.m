%ICC kmeans RUN

statmat =  batchKmeansICC( 4664.826, 4664.941)
statmat =  vertcat(statmat, batchKmeansICC(8474.073, 8474.239));
statmat =  vertcat(statmat,  batchKmeansICC(16023.112, 16023.287));
statmat =  vertcat(statmat, batchKmeansICC(21867.284, 21867.392));
statmat =  vertcat(statmat, batchKmeansICC(28926.312,28926.522));

statmat2 =  batchKmeansICC(4408.993,  4409.090)
statmat2 =  vertcat(statmat2,  batchKmeansICC(7648.275, 7648.405));
statmat2 =  vertcat(statmat2, batchKmeansICC(17526.419, 17526.534));
statmat2 =  vertcat(statmat2,  batchKmeansICC(22727.291 , 22727.370));
statmat2 =  vertcat(statmat2,  batchKmeansICC(25044.712,  25044.799));

% [sC5, mC1]=makeGold(7020.449,  7020.580)
% [sC6, mC2]=makeGold(7490.403,7490.473)
% [sC8, mC3]=makeGold(10907.648,10907.746)
% [sC9, mC4]=makeGold(24744.692,24744.815)
% [sC10, mC5]=makeGold(26099.925, 26100.003)
% 

statmat3 = batchKmeansICC(7020.449,  7020.580)
statmat3 =  vertcat(statmat3, batchKmeansICC(7490.403,7490.473));
statmat3 =  vertcat(statmat3, batchKmeansICC(10907.648,10907.746));
statmat3 =  vertcat(statmat3, batchKmeansICC(24744.692,24744.815));
statmat3 =  vertcat(statmat3, batchKmeansICC(26099.925, 26100.003));