function [ iccrow ] = batchKmeansICC( start, finish )
spec= iccMakeGold( start, finish);

ufilt = processpic(spec, 1, 0);
ufil_shp = processpic(spec, 1, 1);
blur = processpic(spec, 2, 0);
blur_shp = processpic(spec, 2, 1);
disk = processpic(spec, 3, 0);
disk_shp = processpic(spec, 3, 1);

spec= processpic(spec, 1, 0); %for debugging ICC metric

ufilt_seg  = kmeansseg( ufilt, 2, 100, 4);
ufil_shp_seg= kmeansseg( ufil_shp, 2, 100, 4);
blur_seg= kmeansseg( blur, 2, 100, 4);
blur_shp_seg= kmeansseg( blur_shp, 2, 100, 4);
disk_seg= kmeansseg( disk, 2, 100, 4);
disk_shp_seg= kmeansseg( disk_shp, 2, 100, 4);
figure
imshow(disk_shp_seg,[], 'XData', [1 256], 'YData', [1 175]), title('image labeled by cluster index');


iccrow  = [findICC(spec, ufilt_seg)];
iccrow(2)= findICC(spec, ufil_shp_seg);
iccrow(3)= findICC(spec, blur_seg);
iccrow(4)= findICC(spec, blur_shp_seg);
iccrow(5)= findICC(spec, disk_seg);
iccrow(6)= findICC(spec, disk_shp_seg);

end

