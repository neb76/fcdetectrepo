function [ icc ] = findICC_OR(orig, seg)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here


%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

dimt= size(seg, 2) ;
dimf= size(seg, 1);

twoIs= bsxfun(@minus, seg, 1);
twomat= immultiply(orig, twoIs);

oneIs= -bsxfun(@minus, seg, 2);
onemat= immultiply(orig, oneIs);

allvec= reshape(orig, dimt*dimf, 1);
onevec= reshape(onemat, dimt*dimf, 1);
twovec= reshape(twomat, dimt*dimf, 1);

onevec = onevec(onevec ~= 0);
twovec= twovec(twovec~=0);


cv1=var(onevec);
cv2= var(twovec);
btv= var(allvec);


icc= btv^2/((cv1 + cv2)^2 + btv^2);



end

