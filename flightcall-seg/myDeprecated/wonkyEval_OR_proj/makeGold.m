function [ tpr, mask] = makeGold( start, finish)
[p_new,f,t]= getspec('DANBY','20090912','194900',start, finish);
tpr= processpic(p_new, 1, 0);
tpm= processpic(p_new, 3, 1);
h= imfreehand
mask= createMask(h)

end

