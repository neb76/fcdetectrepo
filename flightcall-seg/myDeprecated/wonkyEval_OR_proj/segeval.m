function [ rcl,precision, tpr, fpr, theta, MCC ] = segeval( seg, goldmask )
%UNTITLED Summary of this function goes here


goldpixels= sum(sum(goldmask==1))
allpixels= size(seg,1)*size(seg, 2)
allpreds=0;

wrongpreds=0; %maps to fp
corpreds=0; %maps to tp
fn= 0
tn= 0
onelabes= sum(sum(seg==1))
twolabes= sum(sum(seg==2))
noiseis1= (twolabes<onelabes)

noGold= immultiply(seg, ~goldmask)

if noiseis1
    allpreds= twolabes
    wrongpreds= sum(sum(noGold==2))%in a perfect segmentation, noGold extracts all "call" guesses
    fn= onelabs- sum(sum(noGold==1))
else
    allpreds= onelabes
    wrongpreds= sum(sum(noGold==1))
    fn= twolabes-sum(sum(noGold==2))
end


corpreds= allpreds-wrongpreds
tn= allpixels- fn- corpreds-wrongpreds
rcl= corpreds/goldpixels
precision= corpreds/allpreds
fpr= wrongpreds/allpreds    
tpr= corpreds/allpreds
theta= fpr/tpr
%calculating Mathews Correlation Coeficient
MCC= (corpreds*tn - wrongpreds*fn)/...
    sqrt( (corpreds+wrongpreds)*(corpreds+fn)*(tn+ wrongpreds)*(tn+ fn) )



end

