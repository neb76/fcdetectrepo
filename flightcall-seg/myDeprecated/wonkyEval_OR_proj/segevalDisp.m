function [ evalstats  ] = segevalDisp( seg, goldmask )
%SEGEVALDISP generates evaluation statistics and displays in table form


goldpixels= sum(sum(goldmask==1));
allpixels= size(seg,1)*size(seg, 2);
allpreds=0;

wrongpreds=0; %maps to fp
corpreds=0; %maps to tp
fn= 0;
tn= 0;
onelabes= sum(sum(seg==1));
twolabes= allpixels-onelabes;
noGold= immultiply(seg, ~goldmask);

%1 is noise case
    %all 2's should be gone in noGold
    %amount of 1's should not change
    allpreds= twolabes;
    wrongpreds= sum(sum(noGold==2));%in a perfect segmentation, noGold extracts all "call" guesses
    fn= onelabes- sum(sum(noGold==1));
    corpreds= allpreds-wrongpreds;
    tn= allpixels- fn- corpreds-wrongpreds;
    
    rcl= corpreds/goldpixels;
    precision= corpreds/allpreds;
    
    fpr= wrongpreds/allpreds;    
    tpr= corpreds/(corpreds+fn);
    theta= fpr/tpr;
    %calculating Mathews Correlation Coeficient
    MCC= (corpreds*tn - wrongpreds*fn)/...
        sqrt( (corpreds+wrongpreds)*(corpreds+fn)*(tn+ wrongpreds)*(tn+ fn) );


%2 is noise case    
    %all 1's should be gone in nogold
    %amount of 2's should not change between seg and nogold
    allpreds= onelabes;
    wrongpreds= sum(sum(noGold==1));%in a perfect segmentation, noGold extracts all "call" guesses
    fn= twolabes- sum(sum(noGold==2));
    corpreds= allpreds-wrongpreds;
    tn= allpixels- fn- corpreds-wrongpreds;
    
    rcl2= corpreds/goldpixels;
    precision2= corpreds/allpreds;
    
    fpr2= wrongpreds/allpreds;    
    tpr2= corpreds/(corpreds+fn);
    theta2= fpr2/tpr2;
    %calculating Mathews Correlation Coeficient
    MCC2= (corpreds*tn - wrongpreds*fn)/...
        sqrt( (corpreds+wrongpreds)*(corpreds+fn)*(tn+ wrongpreds)*(tn+ fn) );
    
    
    evalstats= {' ' 'case 1' 'case 2';...
        'recall'  num2str(rcl) num2str(rcl2);...
        'precision' num2str(precision) num2str(precision2);...
        'true positive rate' num2str(tpr) num2str(tpr2);...
        'false positive rate' num2str(fpr) num2str(fpr2);...
        'fpr/tpr' num2str(theta) num2str(theta2);...
        'MCC' num2str(MCC) num2str(MCC2)}
    
    
end

