valV= rand(1, 7);
indM= randi(7, 4, 7);
valMat= valV(indM)
%works! above builds a matrix from values using indeces\

sVM= sort(valMat)
sVM(2,:)

sig_map= (safe_spots, sig_AVec);

%fix by copying sigma from adjacent columns

TV= 1:7
TV= TV'
TM= randi(20, 7,5)
horzcat(TV, TM)


clear all;
close all;
start= 4664.826;
stop=  4664.941;
sigma= .05;

%get spec, then filter and trim
[p_new,f,t]= getspec('DANBY','20090912','194900',start,stop);
data= processpic( p_new, 3, 1 );
dimF= size(data, 1);
dimT= size(data, 2);
dimAff= dimF*dimT;

data_v= reshape(data, dimAff, 1);
gen_nn_distance(data_v, 7, 10000, 0)

[cluster_labels evd_time kmeans_time total_time]= sc(A, 0, 4);

lab_pic= reshape(cluster_labels, dimF, dimT);
figure

imagesc(lab_pic)