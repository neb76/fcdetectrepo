lab_pic= reshape(IDX, dimF, dimT);
HAve= fspecial('average');
filt_pic=imfilter(lab_pic, HAve, 'replicate');
figure
imagesc(filt_pic)


% median filter
filt_pic=medfilt2(lab_pic, [5 5]);
figure
imagesc(filt_pic)


%weinger
filt_pic=wiener2(lab_pic, [5 5]);
figure
imagesc(filt_pic)
