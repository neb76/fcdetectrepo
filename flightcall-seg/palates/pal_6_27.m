clear all
close all
start= 4664.826;
stop=  4664.941;
affTech= 3;
filter= 3;
ampPower=2;
% evNum = 4;
% clustNum=4 ;
thresh= .00000001;

%get spec, then filter and trim
[p_new,f,t]= getspec('DANBY','20090912','194900',start,stop);
data= processpic( p_new, filter, 1 );

% data= data.^2;
% maxV= max(max(data));
% data= data./maxV;

dimF= size(data, 1);
dimT= size(data, 2);
dimAff= dimF*dimT;

B= ones(dimAff, 8); 
d= [-(dimF+1), -dimF, -(dimF-1), -1, 1, (dimF-1), dimF, (dimF+1)];

iMat= spdiags(B, d, dimAff, dimAff);
iMat(1, dimF) = 0;
iMat(dimF, 1) = 0;
iMat(end-(dimF-1), end) = 0;
iMat( end, end-(dimF-1))  = 0;
iMat(find(iMat))= find(iMat);

%list fold-- write custom map function
%        @apFunc= MakeMapFunc(spec)
difFunc= @(x) findDifH(data, x);
aff= spfun(difFunc, iMat);

%try gaus with kernal as above
sigma= .07;
sigTran= 2*sigma^2;
gaussify= @(x) exp(-(x/sigTran));
aff_gaus= spfun(gaussify, aff);
affToEnd(aff_gaus,dimF, dimT);
gTit= strcat('sigma = ', num2str(sigma));
title(gTit);

% valVec= aff(find(aff));

negexp= @(x) exp(-x);
aff_exp= spfun(negexp, aff);
affToEnd(aff_exp, dimF, dimT);  %could turn this into a params struct...

 
%trying simple inverse
invert= @(x) (1/x);
aff_inv= spfun(invert, aff);
affToEnd(aff_inv,dimF, dimT);


afftoEnd(aff, dimF, dimT);
