[p_new,f,t]= getspec('DANBY','20090912','194900',1259.79,1259.89);
tp= processpic( p_new, 3, 1 );
%put image into a 0-1 range
oldmin= min(min(tp));
oldmax= max(max(tp));
oldrange= oldmax-oldmin;


imshow(tp,[], 'XData', [1 256], 'YData', [1 175]), title('image labeled by cluster index');

dimF= size(tp, 1);
dimT= size(tp, 2);
dbvec= reshape(tp, dimT*dimF, 1); %vec of all points
dbvec= (dbvec-oldmin)/oldrange;
fvectemp=1:dimF;
fvec= repmat(fvectemp',dimT,1);
fvec= fvec.^(.2);
tvecttemp= 1:dimT;
tmat= repmat(tvecttemp, dimF, 1);
tvec= reshape(tmat, dimT*dimF, 1);
tvec= tvec.^.5;

% (dimF x dimT) X 3 matrix of points.
points= horzcat(dbvec, fvec, tvec);
W= SimGraph( points',4, 15,1);
%W= SimGraph( dbvec', 4, 8, 9  );


 %attempt two, will only use points this time
 %3 clusters
 C = SpectralClustering( W, 3, 3);
 C=  bsxfun(@times, C, [1 2 3]);
 C= C*[1 1 1]';
 F = full(C);
 pic= reshape(F, dimF, dimT);
 imshow(pic,[], 'XData', [1 256], 'YData', [1 175]), title('image labeled by cluster index');

 %2 clusters
 C = SpectralClustering( W, 2, 3 );
 C=  bsxfun(@times, C, [1 2 ]);
 C= C*[1 1 ]';
 F = full(C);
 pic= reshape(F, dimF, dimT);
 imshow(pic,[], 'XData', [1 256], 'YData', [1 175]), title('image labeled by cluster index');

 
 %try with freq but not time
 points= horzcat(dbvec, fvec);

  
 
 imshow(p_new,[], 'XData', [1 256], 'YData', [1 175]), title('image labeled by cluster index');

 %##################################################################
%         sunday testing     #######################################
 %##################################################################

[p_new,f,t]= getspec('DANBY','20090912','194900',1259.79,1259.89);
tp= processpic( p_new, 1, 1 );
W= newAff_vec(tp, 3, .01);   %try 1, some weird stuff
%try two, will try to leverage off the shelf- similarity on my own aff
W= SimGraph( W,4, 15,1);
dimF= size(tp, 1);
dimT= size(tp, 2);

type= 3;
 %attempt two, will only use points this time
 %3 clusters
 C = SpectralClustering( W, 3, type);
 C=  bsxfun(@times, C, [1 2 3]);
 C= C*[1 1 1]';
 F = full(C);
 pic= reshape(F, dimF, dimT);
 imshow(pic,[], 'XData', [1 256], 'YData', [1 175]), title('image labeled by cluster index');

 %2 clusters
 C = SpectralClustering( W, 2, type );
 C=  bsxfun(@times, C, [1 2 ]);
 C= C*[1 1 ]';
 F = full(C);
 pic= reshape(F, dimF, dimT);
 figure
 imshow(pic,[], 'XData', [1 256], 'YData', [1 175]), title('image labeled by cluster index');

 
 
 %now trying karthiks methods
 clustsnum= 2;
 k=2;
 
[p_new,f,t]= getspec('DANBY','20090912','194900',1259.79,1259.89);
tp= processpic( p_new, 3, 1 );
dimF= size(tp, 1);
dimT= size(tp, 2);
W= newAff_vec(tp, 3);   %try 1, some weird stuff
lap= getLap(W);
[eigV, eigVals] = eigs(lap);
L=eigV(:,2);

L_nzI= find(L);
L_nz= L(L_nzI);
figure,plot(L_nz),title('2nd Largest Eigenvector');

[cluster_idx, C] = kmeans(L_nz, clustsnum, 'start', 'cluster', ...
                 'EmptyAction', 'singleton');
thresh= mean(C);
L(L>thresh)=1;
L(L<=thresh)=0;
thresh_pic = reshape(L,dimF,dimT);
imagesc(thresh_pic);

%attempt at the multpile eig-vec normalization

nEigVec = eigV(:,2:(k));
% construct the normalized matrix U from the obtained eigen vectors
for i=1:size(nEigVec,2)
    n = sqrt(sum(nEigVec(:,i).^2)); 
    U(:,i) = nEigVec(:,i) ./ n;
end

% perform kmeans clustering on the matrix U
[IDX,C] = kmeans(U,2); 
pixel_labels = reshape(IDX,dimF,dimT);
imagesc(pixel_labels)


lap_show= full(W);
 imshow(lap_show,[], 'XData', [1 256], 'YData', [1 175]), title('image labeled by cluster index');
 imshow(W,[], 'XData', [1 256], 'YData', [1 175]), title('image labeled by cluster index');

 
 


 