[p_new,f,t]= getspec('DANBY','20090912','194900',1259.79,1259.89);
data= processpic( p_new, 3, 1 );
dimF= size(data, 1);
dimT= size(data, 2);
figure,plot(data(:,1), data(:,2),'r+'),title('Original Data Points'); grid on;shg

affinity = newAff_vec(data, 3, .1);
%
% compute the degree matrix
for i=1:size(affinity,1)
    D(i,i) = sum(affinity(i,:));
end

D= sparse(D);

% compute the unnormalized graph laplacian matrix
L = D - affinity; 

[eigVectors,eigValues] =  eigs(L, 2, 'sr' );

% plot the eigen vector corresponding to the 2nd largest eigen value
figure,plot(eigVectors(:,1),'r*'),title('2nd smallest e-vals Eigenvector');

low= -.013;
high= -.005;

secEV= eigVectors(:,1);
s_secEV= sort(secEV);
% plot(s_secEV)
% imagesc(s_secEV)

secEV(secEV > high)= 2;
secEV(secEV > low & secEV < high )= 1;
secEV(secEV < low)= 0;

lab_im= reshape(secEV, dimF, dimT);
imagesc(lab_im)

