stest= segJob('for use in testing imagesc', .08, 6, 2);
clear
stest= segJob('for use in testing imagesc', .08, 6, 3);
start= 4664.826;
stop=  4664.941;
[p_new,f,t]= getspec('DANBY','20090912','194900',start,stop);
spec= p_new;

segged= stest.segmentThis(spec)
imagesc(segged)

%try same on cluster
sched= findResource('scheduler', 'configuration', 'atlasscheduler' );
clustJ= createJob(sched);
set(clustJ,'FileDependencies', {'Z:\gitRepos\fcdetectrepo\flightcall-seg\activeCode\runCode\notScript'})
createTask(clustJ, @computeDistrJob_clientSv, 1, { 'for use in testing imagesc', .08, 6, 3, spec});
submit(clustJ)
wait(clustJ)
jobOut= clustJ.getAllOutputArguments;






imwrite( segged, 'directWrite3.jpg')

%attempts to replicate imagesc() without showing images
iaSeg=imadjust(segged);
isSeg= imscale(segged);

imshow(segged)
imshow(segged,[], 'XData', [1 256], 'YData', [1 175]), title('testin internals');

imshow(iaSeg)
imshow(iaSeg,[], 'XData', [1 256], 'YData', [1 175]), title('testin internals');

isc= imagesc(segged);

close all
display(isc)


h= giveIm(segged);