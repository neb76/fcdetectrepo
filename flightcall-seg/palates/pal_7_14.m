%debugging spectral

start= 4664.826;
stop=  4664.941;
spec= getspecBatch(1024, 'DANBY', '20090912', '194900', start, stop);
figure, imagesc(processpic(spec, 3, 1));



eJune= segJob('matching early spring', .05, 6, 3);
segged= eJune.segmentThis(spec);
juneFig= figure;
imagesc(segged)
title(eJune.ID)
%above looks fine, but does have strange regions?

eJune.absY= false;
eJune.ID= 'attempting no abs value 2';
segged= eJune.segmentThis(spec);
juneFig= figure;
imagesc(segged)
title(eJune.ID)


%attempt to see what happens if you bring the number of clusters up to the
%apparant number of segments
eJune.adjustK(7)
eJune.ID= 'matching k with apparant clusters';
segged= eJune.segmentThis(spec);
juneFig= figure;
imagesc(segged)
title(eJune.ID)