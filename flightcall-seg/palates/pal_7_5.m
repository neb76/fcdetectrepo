%7/5/2012
%attempt to match early june success with new, batch tests

% lapcase== 3
% evstyle= 'SR'
% 
% ev/k= 4
% 
% range then
% ampower= 3
% 


clear all
close all
len= 1;  %if 0, use all data from csv
csv_name='C:\Users\neb76\Desktop\nick_FCDetect_beforGit\DANBY_20090912_194900_NFC_637.csv'

segMat= parseCSV(csv_name, len);
eJune= segJob('matching early spring', .05, 6, 3);
[spec, ~]= eJune.getJobSpecG(segMat{1, 2:3});
segged= eJune.segmentThis(spec);   

juneFig= figure;
imagesc(segged)
title(eJune.ID)




eJune.evType= 'reg';
eJune.ID= 'switching evType from default SR to normal eigs, no arg';
segged= eJune.segmentThis(spec);   

juneFig2= figure;
imagesc(segged)
title(eJune.ID)


eJune.evType= 'SM';
eJune.ID= 'evType now SM';
segged= eJune.segmentThis(spec);   

juneFig3= figure;
imagesc(segged)
title(eJune.ID)


eJune.evType= 'LR';
eJune.ID= 'evType now LR';
segged= eJune.segmentThis(spec);   

juneFig4= figure;
imagesc(segged)
title(eJune.ID)



eJune.evType= 'reg';
eJune.filtSet= 2;
eJune.ID= 'reg EV, less filt?';
segged= eJune.segmentThis(spec);  

juneFig5= figure;
imagesc(segged)
title(eJune.ID)
%did no converge, trying a higher sigma


eJune.sig= .08;
segged= eJune.segmentThis(spec);  

juneFig5= figure;
imagesc(segged)
title(eJune.ID)



eJune.evType= 'reg';


