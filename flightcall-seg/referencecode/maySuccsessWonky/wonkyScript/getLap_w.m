function [lap] = getLap( A)
lap= -A;
% Degree matrixx sumvec. 
sumvec = sum(A,2);
for i = 1: size(sumvec) 
    lap(i,i) = -sumvec(i);
end

pr = 'got laplacian'

% compute the normalized laplacian
sumvec(sumvec == 0) = eps;
sqsumvec = sqrt(sumvec);
lap= bsxfun(@rdivide,lap, sqsumvec);
lap= bsxfun(@rdivide,lap, sqsumvec');

pr = 'normalized..'



% TODO- 
end


