function [ aff ] = newAff_vec( spec, distProcCase, sigma )
neigh= 5;
%newAff_vec new get Aff, returns a sparse matrix and fully vectorized
%   -does not examine edges
%   distProcCase cases:
        %1(or not defined), gaussian, with sigma as median value of whole
        %spec * 1.5
        %2- gaussian, with alpha as neighborhood median value * 1.5

%TODO:
    %as of now, not overwriting anything and have multiple copies floating
    %around, if memory issues, fix this

%put image into 0-1 range
oldmin= min(min(spec));
oldmax= max(max(spec));
oldrange= oldmax-oldmin;
spec= (spec-oldmin)/oldrange;
spec= spec.^3;
%prep referencing indeces
dimF= size(spec, 1);
dimT= size(spec, 2);
dimAff= dimF*dimT;
dimSafe= (dimF-2)*(dimT-2);

%get vector of non-edge points
safe_spots= reshape(1:dimAff, dimF, dimT);
safe_spots= safe_spots(2:(dimF-1), 2: (dimT-1) );  
safe_spots= reshape(safe_spots,  1, dimSafe);

%create matrices of neighboring positions, which we use to find neighbor
%amps and then differences
    %[E, SE, S, SW, W, NW, N, NE]
M= dimF;
neigh_offsets = [M, (M + 1), 1, -M + 1, -M, -M - 1,-1,  M - 1]';
neighIMat= bsxfun(@plus,safe_spots, neigh_offsets);
neighAmpMat= spec(neighIMat);
ampDMat= abs(bsxfun(@minus, spec(safe_spots), neighAmpMat));
ampDVec= reshape(ampDMat, 1, 8*dimSafe);

%operate on the raw distances
%according to "spectrally clustered"
 if (distProcCase==2)
     neighMedVec= median(ampDMat);
     ampAffMat= bsxfun(@rdivide, ampDMat, 2*((neighMedVec*1.5).^2));
     ampAffMat=exp(-ampAffMat); 
     ampAffVec= reshape(ampAffMat, 1, 8*dimSafe);
     figure
     hist(ampAffVec);
 elseif (distProcCase== 3)
%     sigma= .1;
    ampAffVec= exp(-(ampDVec./(2*sigma^2) ));
    figure
    hist(ampAffVec);
    
 %attempt to create local scaling
 elseif (distProcCase== 4)
%      ampAffMat= -(ampDMat.^2);
     ampAffMat= -(ampDMat);

     
     s_neighAmpMat= sort(neighAmpMat);
     sig_AVec= s_neighAmpMat(neigh,:);  %***this one of potential "local statistics", here using "7-th neighbor"
     %now must fix so we have the "unsafe" values too,
     %the shortcut? hacky? just copy what we found in next row in...
        %1) return to matrix form
        sig_fullMat= reshape(sig_AVec, dimF-2, dimT-2);
        %2) paste copies of  bottom then top row to self
        sig_fullMat= vertcat(sig_fullMat, sig_fullMat(end, :));
        sig_fullMat= vertcat(sig_fullMat(1,:), sig_fullMat);
        %3) past copies of right the left onto self
        sig_fullMat= horzcat( sig_fullMat, sig_fullMat(:,end));
        sig_fullMat= horzcat(sig_fullMat(:,1), sig_fullMat);
        %4.) build map that has all values
%         sig_fullMat= reshape(sig_fullMat, 1, dimAff);
        
        %using a map, but now that all values are back, i don't think
        %necessary
%       sig_map= containers.Map(safe_spots, sig_AVec);
%       sig_BMat= values(sig_map, num2cell(neighIMat));
%       sig_BMat= cell2mat(sig_BMat);
     sig_BMat= sig_fullMat(neighIMat);

     ampAffMat= bsxfun(@rdivide, ampAffMat, sig_AVec);
     ampAffMat= bsxfun(@rdivide, ampAffMat, sig_BMat);
     ampAffMat= exp(ampAffMat);
     ampAffVec= reshape(ampAffMat, 1, 8*dimSafe);
     figure
     hist(ampAffVec);

     
     
     

    
%here, using variance of DISTANCES from this point. 
% if (caseN==3)
%     neighSDVec= std(ampDMat);
%     ampAffMat= bsxfun(@rdivide, ampDMat, 2*(neighSDVec.^2));
%     ampAffMat=exp(-ampAffMat); 
%     ampAffVec= reshape(ampAffMat, 1, 8*dimSafe);
%     figure
%     hist(ampAffVec);
    
%variances of point neighborhood
%elseif (caseN==4)
%     neighSDVec= std(neighAmpMat);
%     ampAffMat= bsxfun(@rdivide, ampDMat, 2*(neighSDVec.^2));
%     ampAffMat=exp(-ampAffMat); 
%     ampAffVec= reshape(ampAffMat, 1, 8*dimSafe);
%     figure
%     hist(ampAffVec);    
else
    sigma= 1.5* median(ampDVec);
    ampAffVec= exp(-(ampDVec./(2*sigma^2) ));
    hist(ampAffVec);
 end

%build column and row index vectors in order to build a sparse matrix
colIVec= reshape(neighIMat, 1, 8*dimSafe);    
    %for row, create 8-time repeat vector of linear indeces
        %eg. 1111111122222233333333...
rowIMat= safe_spots;
rowIMat= repmat(rowIMat, 8, 1);
rowIVec= reshape(rowIMat, 1, 8*dimSafe);
aff= sparse(rowIVec, colIVec, ampAffVec,dimAff, dimAff);
aff= aff(safe_spots, safe_spots);

end

