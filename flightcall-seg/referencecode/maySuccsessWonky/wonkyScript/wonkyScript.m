clear all;
% close all;
start= 4664.826;
stop=  4664.941;
sigma= .05;
evNum = 4;
clustNum=4 ;

%get spec, then filter and trim
[p_new,f,t]= getspec_w('DANBY','20090912','194900',start,stop);
data= processpic_w( p_new, 3, 1 );
figure

affinity = newAff_vec_w(data, 3, sigma); 
dimF= size(data, 1)-2;
dimT= size(data, 2)-2;
NL1= getLap_w(affinity);
[U,~] = eigs(NL1, evNum, 'SR');
U= normr(U);
[IDX,C] = kmeans(U,clustNum, 'replicates', 50); 
lab_pic= reshape(IDX, dimF, dimT);
figure
imagesc(lab_pic)
% end